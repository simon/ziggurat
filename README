Introduction
------------

This repository contains 'Ziggurat', a suite of loosely connected
programs that do 2D computational geometry.

I wrote them all with the general aim of starting from some kind of
map data and producing 3D-printable models of terrain in STL format.
But because they're essentially 2D, the only kind of 3D model they can
output is one that consists of a sequence of layers stacked on top of
each other, each of which is an extrusion of a 2D outline to a certain
thickness. (Like a stepped pyramid, hence the name.)

However, the Ziggurat tools contain some reasonably general geometric
primitives and may be useful for other purposes too.

Currently, there isn't much documentation, and the tools do quite
low-level things. So if you want to use them for anything, you
probably need to chain multiple tools together. Sorry. This is all
quite unfinished and doesn't have much user interface yet. If it
breaks, you can keep all the pieces.

Building
--------

Ziggurat currently comes with an autotools build system. It depends on
GMP and libpng.

It's not yet been tested on any operating system except Linux.

(The code _should_ be portable, though. If you can get GMP and libpng
to build on Windows, it _ought_ to be possible to build Ziggurat
against them. But I've never tried.)

Outline file format
-------------------

Many of the Ziggurat tools work on a thing called an 'outline file'.
This is a simple format I made up myself, which describes a shape in
2D space by giving the outline of one or more polygons that bound it.

An outline file consists of a sequence of closed loops, each defined
by some number of points. For example, here's a valid outline file
that describes two squares joined at a corner:

loop 4
point 0 0
point 0.5 0
point 0.5 0.5
point 0 0.5
endloop
loop 4
point 0 0
point -1/2 0
point -1/2 -1/2
point 0 -1/2
endloop

As shown in this example, points' coordinates don't have to be
integers, and they can be specified in decimal form or fraction form.

The tools
---------

The tools built by the Ziggurat makefile are as follows. Each one has
some command-line options you can access via '--help'.

'munge' is a tool for converting outline files into other outline
files. Given multiple input outlines, it can compute their union, the
intersection, or the set difference. Given one input outline, it can
compute its convex hull, or simplify a very complicated outline to
make it easier to compute with. Also, it can apply an affine
transformation to the input outlines (scale, rotate, shear, translate,
reflect, etc).

'outlineview' is a diagnostic display tool. Given one or more
outlines, it will output a Postscript file that displays each outline
on one page. I don't normally try to _print_ the output; I just feed
it to 'gv' to show it on the screen.

'minkowski' computes the Minkowski sum of two outlines. (For example,
given one outline file describing a large square and one approximating
a small circle, you'd get back a square with rounded corners.) This
one is particularly poorly tested, I'm afraid.

'voronoi' computes the Voronoi diagram of a set of input points,
producing an outline file for each input point showing the boundary of
its Voronoi cell. But it can't do it using _only_ a set of points,
because in any Voronoi diagram some of the cells must extend to
infinity, and outline files can't describe an unbounded region of the
plane. So actually this tool requires you to specify a boundary
outline (or just an orthogonal rectangle to make things easier), and
it will return the intersection of each Voronoi cell with that
boundary. The expected input file format consists of a sequence of
lines each containing an x and y coordinate followed by a name for the
point; the output files are called by those names with ".outline"
appended. For example, a file consisting of

0 0 foo
0 1 bar
1 0 baz
1 1 quux

will output foo.outline, bar.outline, baz.outline and quux.outline.

'makestl' takes multiple outline files and outputs a 3D model in STL
format. You can specify the thickness you want for each layer of the
model.

'tracemap' generates an outline starting from a digital elevation map
file. The input DEM file is expected to be in the format used by the
USGS GTOPO30 data set, and also the higher-resolution SRTM data: a raw
binary file formatted as a rectangular array of 16-bit big-endian
signed integers, each giving the height at one point of a rectangular
grid. (Usually the heights are in metres above sea level, with some
special value like -9999 representing sea itself.) So you have to
specify the width and height of the grid in the DEM file, because it
doesn't contain that information within it.

'prepmap' is intended to be run before 'tracemap': it reads one DEM
file and writes another, computing an approximate Minkowski sum
between the input landscape and a horizontal circle. This allows
landscapes to be very squashed and still printed, without the
mountains becoming such flimsy vertical needles that they won't print
sensibly. (This tool was written a long time before the outline-based
'minkowski' tool; it's possible that now the same job would be better
done by that, although, on the other hand, this one might run faster.)
