/*
 * bigrat.hh: Implementation of the 'bigrat' class, which is a C++
 * operator-overloaded class wrapping GMP's mpq_t.
 *
 * I tried 'mpq_class' from gmpxx.h, but had some trouble with it
 * trying to realloc things on the stack. I suspect it of trying to be
 * too clever in its pursuit of efficiency, so this is a deliberately
 * simpler approach.
 */

#ifndef BIGRAT_HH
#define BIGRAT_HH

#include <stdlib.h>
#include <string.h>

#include <string>
#include <iostream>

#include <gmp.h>

namespace Ziggurat {
namespace ZigguratInternal {
#if 0
} // mess with editors' indenting
} // mess with editors' indenting
#endif

using std::string;
using std::ostream;

class bigrat {
  public:
    inline bigrat() { mpq_init(q); }
    inline bigrat(int x) { mpq_init(q); mpq_set_si(q, x, 1); }
    inline bigrat(int x, int y) { mpq_init(q); mpq_set_si(q, x, y); }
    inline bigrat(mpq_t x) { mpq_init(q); mpq_set(q, x); }
    inline bigrat(const bigrat &x) { mpq_init(q); mpq_set(q, x.q); }
    inline ~bigrat() { mpq_clear(q); }

    inline int getint(bool ceil) const {
        mpz_t z;
        mpz_init(z);
        mpz_set(z, mpq_numref(q));
        if (ceil) {
            mpz_add(z, z, mpq_denref(q));
            mpz_sub_ui(z, z, 1);
        }
        mpz_div(z, z, mpq_denref(q));
        int ret = mpz_get_si(z);
        mpz_clear(z);
        return ret;
    }
    inline operator int() const { return getint(false); }
    inline int floor() const { return getint(false); }
    inline int ceil() const { return getint(true); }
    inline operator bool() const { return mpq_cmp_si(q, 0, 1) != 0; }
    inline operator double() const { return mpq_get_d(q); }
    inline bigrat &operator=(const bigrat &a) { mpq_set(q,a.q); return *this; }
    inline bigrat &operator+=(const bigrat &a) { mpq_add(q,q,a.q); return *this; }
    inline bigrat &operator-=(const bigrat &a) { mpq_sub(q,q,a.q); return *this; }
    inline bigrat &operator*=(const bigrat &a) { mpq_mul(q,q,a.q); return *this; }
    inline bigrat &operator/=(const bigrat &a) { mpq_div(q,q,a.q); return *this; }

    inline char *stringify() const {
        mpq_t qq;
        mpq_init(qq);
        mpq_set(qq, q);
        mpq_canonicalize(qq);

        char *numstr = mpz_get_str(NULL, 10, mpq_numref(qq));
        char *ret;
        if (!mpz_cmp_ui(mpq_denref(qq), 1)) {
            ret = new char[1 + strlen(numstr)];
            sprintf(ret, "%s", numstr);
        } else {
            char *denstr = mpz_get_str(NULL, 10, mpq_denref(qq));
            ret = new char[2 + strlen(numstr) + strlen(denstr)];
            sprintf(ret, "%s/%s", numstr, denstr);
            free(denstr);
        }

        free(numstr);
        mpq_clear(qq);
        return ret;
    }

    inline operator string() const {
        char *text = stringify();
        string ret = text;
        delete[] text;
        return ret;
    }

  private:
    mpq_t q;
    friend bigrat operator+(const bigrat &a, const bigrat &b);
    friend bigrat operator+(const bigrat &a, int b);
    friend bigrat operator+(int a, const bigrat &b);
    friend bigrat operator-(const bigrat &a, const bigrat &b);
    friend bigrat operator-(const bigrat &a, int b);
    friend bigrat operator-(int a, const bigrat &b);
    friend bigrat operator*(const bigrat &a, const bigrat &b);
    friend bigrat operator*(const bigrat &a, int b);
    friend bigrat operator*(const bigrat &a, unsigned b);
    friend bigrat operator*(int a, const bigrat &b);
    friend bigrat operator*(unsigned a, const bigrat &b);
    friend bigrat operator/(const bigrat &a, const bigrat &b);
    friend bigrat operator/(const bigrat &a, unsigned b);
    friend bigrat operator-(const bigrat &a);
    friend bool operator==(const bigrat &a, const bigrat &b);
    friend bool operator!=(const bigrat &a, const bigrat &b);
    friend bool operator<(const bigrat &a, const bigrat &b);
    friend bool operator>(const bigrat &a, const bigrat &b);
    friend bool operator<=(const bigrat &a, const bigrat &b);
    friend bool operator>=(const bigrat &a, const bigrat &b);
    friend bool operator==(const bigrat &a, int b);
    friend bool operator!=(const bigrat &a, int b);
    friend bool operator<(const bigrat &a, int b);
    friend bool operator>(const bigrat &a, int b);
    friend bool operator<=(const bigrat &a, int b);
    friend bool operator>=(const bigrat &a, int b);
    friend bool operator==(int a, const bigrat &b);
    friend bool operator!=(int a, const bigrat &b);
    friend bool operator<(int a, const bigrat &b);
    friend bool operator>(int a, const bigrat &b);
    friend bool operator<=(int a, const bigrat &b);
    friend bool operator>=(int a, const bigrat &b);
    friend bigrat bigrat_power(unsigned x, unsigned y);
};

inline ostream &operator<<(ostream &os, const bigrat &q) { return os << string(q); }

inline bigrat operator+(const bigrat &a, const bigrat &b) { bigrat ret = a; return ret += b; }
inline bigrat operator+(const bigrat &a, int b) { bigrat ret = a; return ret += b; }
inline bigrat operator+(int a, const bigrat &b) { bigrat ret = b; return ret += a; }
inline bigrat operator-(const bigrat &a, const bigrat &b) { bigrat ret = a; return ret -= b; }
inline bigrat operator-(const bigrat &a, int b) { bigrat ret = a; return ret -= b; }
inline bigrat operator-(int a, const bigrat &b) { bigrat ret; mpq_neg(ret.q, b.q); return ret += a; }
inline bigrat operator*(const bigrat &a, const bigrat &b) { bigrat ret = a; return ret *= b; }
inline bigrat operator*(const bigrat &a, int b) { bigrat ret = a; return ret *= b; }
inline bigrat operator*(const bigrat &a, unsigned b) { bigrat ret = a; return ret *= b; }
inline bigrat operator*(int a, const bigrat &b) { bigrat ret = b; return ret *= a; }
inline bigrat operator*(unsigned a, const bigrat &b) { bigrat ret = b; return ret *= a; }
inline bigrat operator/(const bigrat &a, const bigrat &b) { bigrat ret = a; return ret /= b; }
inline bigrat operator/(const bigrat &a, unsigned b) { bigrat ret = a; return ret /= b; }
inline bigrat operator-(const bigrat &a) { bigrat ret; mpq_neg(ret.q,a.q); return ret; }
inline bool operator==(const bigrat &a, const bigrat &b) { return 0 == mpq_cmp(a.q,b.q); }
inline bool operator!=(const bigrat &a, const bigrat &b) { return 0 != mpq_cmp(a.q,b.q); }
inline bool operator<(const bigrat &a, const bigrat &b) { return 0 > mpq_cmp(a.q,b.q); }
inline bool operator>(const bigrat &a, const bigrat &b) { return 0 < mpq_cmp(a.q,b.q); }
inline bool operator<=(const bigrat &a, const bigrat &b) { return 0 >= mpq_cmp(a.q,b.q); }
inline bool operator>=(const bigrat &a, const bigrat &b) { return 0 <= mpq_cmp(a.q,b.q); }
inline bool operator==(const bigrat &a, int b) { return 0 == mpq_cmp_si(a.q,b,1); }
inline bool operator!=(const bigrat &a, int b) { return 0 != mpq_cmp_si(a.q,b,1); }
inline bool operator<(const bigrat &a, int b) { return 0 > mpq_cmp_si(a.q,b,1); }
inline bool operator>(const bigrat &a, int b) { return 0 < mpq_cmp_si(a.q,b,1); }
inline bool operator<=(const bigrat &a, int b) { return 0 >= mpq_cmp_si(a.q,b,1); }
inline bool operator>=(const bigrat &a, int b) { return 0 <= mpq_cmp_si(a.q,b,1); }
inline bool operator==(int a, const bigrat &b) { return 0 == mpq_cmp_si(b.q,a,1); }
inline bool operator!=(int a, const bigrat &b) { return 0 != mpq_cmp_si(b.q,a,1); }
inline bool operator<(int a, const bigrat &b) { return 0 < mpq_cmp_si(b.q,a,1); }
inline bool operator>(int a, const bigrat &b) { return 0 > mpq_cmp_si(b.q,a,1); }
inline bool operator<=(int a, const bigrat &b) { return 0 <= mpq_cmp_si(b.q,a,1); }
inline bool operator>=(int a, const bigrat &b) { return 0 >= mpq_cmp_si(b.q,a,1); }

inline bigrat bigrat_power(unsigned x, int y) {
    mpz_t z;
    mpz_init(z);
    mpz_ui_pow_ui(z, x, (y > 0 ? y : -y));
    mpq_t q;
    mpq_init(q);
    mpq_set_z(q, z);
    mpz_clear(z);
    bigrat ret(q);
    mpq_clear(q);
    if (y < 0)
        ret = bigrat(1)/ret;
    return ret;
}

#if 0
{ // mess with editors' indenting
{ // mess with editors' indenting
#endif
} // close namespace ZigguratInternal
using ZigguratInternal::bigrat;
using ZigguratInternal::bigrat_power;
} // close namespace Ziggurat

#endif /* BIGRAT_HH */
