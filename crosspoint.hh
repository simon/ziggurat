#ifndef CROSSPOINT_HH
#define CROSSPOINT_HH

namespace Ziggurat {
namespace ZigguratInternal {
#if 0
} // mess with editors' indenting
} // mess with editors' indenting
#endif

/*
 * Utility function to find the crossing point of two line segments.
 */
template<class Number> bool crosspoint(Number xa1, Number ya1,
                                       Number xa2, Number ya2,
                                       Number xb1, Number yb1,
                                       Number xb2, Number yb2,
                                       Number *xout, Number *yout) {
    /*
     * Give the intersection point of the (possibly extrapolated)
     * line segments (xa1,ya1)-(xa2,ya2) and (xb1,yb1)-(xb2,yb2).
     */
    Number dxa = xa2-xa1;
    Number dya = ya2-ya1;
    Number dxb = xb2-xb1;
    Number dyb = yb2-yb1;
    // Special case: if gradients are equal, there's no answer.
    if (dya * dxb == dxa * dyb)
        return false;
    // Second special case: if either gradient is horizontal or vertical.
    if (dxa == 0) {
        // Because we've already eliminated the parallel case, dxb
        // is now known to be nonzero. So we can simply
        // extrapolate along the b line until it hits the common
        // value xa1==xa2.
        *xout = xa1;
        *yout = (xa1 - xb1) * dyb / dxb + yb1;
        return true;
    }
    // Similar cases for dya == 0, dxb == 0 and dyb == 0.
    if (dxb == 0) {
        *xout = xb1;
        *yout = (xb1 - xa1) * dya / dxa + ya1;
        return true;
    }
    if (dya == 0) {
        *xout = (ya1 - yb1) * dxb / dyb + xb1;
        *yout = ya1;
        return true;
    }
    if (dyb == 0) {
        *xout = (yb1 - ya1) * dxa / dya + xa1;
        *yout = yb1;
        return true;
    }

    // General case: all four gradient components are nonzero. In
    // this case, we have
    //
    //     y - ya1   dya           y - yb1   dyb
    //     ------- = ---    and    ------- = ---
    //     x - xa1   dxa           x - xb1   dxb
    //
    // We rewrite these equations as
    //
    //     y = ya1 + dya (x - xa1) / dxa
    //     y = yb1 + dyb (x - xb1) / dxb
    //
    // and equate the RHSes of each
    //
    //     ya1 + dya (x - xa1) / dxa = yb1 + dyb (x - xb1) / dxb
    //  => ya1 dxa dxb + dya dxb (x-xa1) = yb1 dxb dxa + dyb dxa (x-xb1)
    //  => (dya dxb - dyb dxa) x =
    //          dxb dxa (yb1 - ya1) + dya dxb xa1 - dyb dxa xb1
    //
    // So we have a formula for x
    //
    //         dxb dxa (yb1 - ya1) + dya dxb xa1 - dyb dxa xb1
    //     x = -----------------------------------------------
    //                        dya dxb - dyb dxa
    //
    // and by a similar derivation we also obtain a formula for y
    //
    //         dya dyb (xa1 - xb1) + dxb dya yb1 - dxa dyb ya1
    //     y = -----------------------------------------------
    //                        dya dxb - dyb dxa

    Number d = dya * dxb - dyb * dxa;
    Number xn = dxb * dxa * (yb1-ya1) + dya * dxb * xa1 - dyb * dxa * xb1;
    Number yn = dya * dyb * (xa1-xb1) + dxb * dya * yb1 - dxa * dyb * ya1;
    *xout = xn / d;
    *yout = yn / d;
    return true;
}

#if 0
{ // mess with editors' indenting
{ // mess with editors' indenting
#endif
} // close namespace ZigguratInternal
using ZigguratInternal::crosspoint;
} // close namespace Ziggurat

#endif /* CROSSPOINT_HH */
