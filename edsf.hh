/*
 * edsf.hh: 'extended disjoint set forest', i.e. a dsf augmented with
 * information about how each element differs from its parent.
 */

#ifndef EDSF_HH
#define EDSF_HH

#include <assert.h>

namespace Ziggurat {
namespace ZigguratInternal {
#if 0
} // mess with editors' indenting
} // mess with editors' indenting
#endif

template<class Aux>
class EDSF {
    struct element {
        element *parent;
        Aux aux;
        element() : parent(NULL) {}
    };
    int n;
    element *a;

    int findroot(int k, Aux *aux) {
        assert(0 <= k && k < n);
        element *e = &a[k];
        Aux auxret;
        while (e->parent) {
            auxret = auxret + e->aux;
            e = e->parent;
        }
        *aux = auxret;
        return e - a;
    }

    void squash(int k, int root, Aux auxtoroot) {
        assert(0 <= k && k < n);
        assert(0 <= root && root < n);
        element *e = &a[k], *r = &a[root];
        assert(!r->parent);
        while (e->parent) {
            element *tmp = e->parent;
            Aux auxtmp = auxtoroot - e->aux;
            e->aux = auxtoroot;
            e->parent = r;
            auxtoroot = auxtmp;
            e = tmp;
        }
        assert(auxtoroot == Aux());
    }

  public:
    EDSF(int size) : n(size), a(new element[size]) {}
    ~EDSF() { delete[] a; }

    int canonify(int index) {
        Aux aux;
        int ret = findroot(index, &aux);
        squash(index, ret, aux);
        return ret;
    }

    void merge(int p, int q, Aux aux_p_to_q = Aux()) {
        Aux aux_p_to_root, aux_q_to_root;
        int p_root = findroot(p, &aux_p_to_root);
        int q_root = findroot(q, &aux_q_to_root);
        int root;

        if (p_root == q_root) {
            assert(aux_p_to_root == aux_p_to_q + aux_q_to_root);
            root = p_root;
            squash(p, p_root, aux_p_to_root);
        } else {
            a[q_root].parent = &a[p_root];
            a[q_root].aux = aux_p_to_root - (aux_p_to_q + aux_q_to_root);
            squash(q, p_root, aux_p_to_root - aux_p_to_q);
            squash(p, p_root, aux_p_to_root);
        }
    }

    bool get_difference(int p, int q, Aux *aux = NULL) {
        Aux aux_p_to_root, aux_q_to_root;
        int p_root = findroot(p, &aux_p_to_root);
        int q_root = findroot(q, &aux_q_to_root);
        squash(q, q_root, aux_q_to_root);
        squash(p, p_root, aux_p_to_root);
        if (p_root != q_root)
            return false;
        if (aux)
            *aux = aux_p_to_root - aux_q_to_root;
        return true;
    }
};

class NullAux {
  public:
    bool operator==(const NullAux &) const { return true; }
    NullAux operator-(const NullAux &) const { return NullAux(); }
    NullAux operator+(const NullAux &) const { return NullAux(); }
};

typedef EDSF<NullAux> DSF;

#if 0
{ // mess with editors' indenting
{ // mess with editors' indenting
#endif
} // close namespace ZigguratInternal
using ZigguratInternal::EDSF;
using ZigguratInternal::DSF;
} // close namespace Ziggurat

#endif /* EDSF_HH */
