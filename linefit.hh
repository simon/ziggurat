namespace Ziggurat {
namespace ZigguratInternal {
#if 0
} // mess with editors' indenting
} // mess with editors' indenting
#endif

struct linefit {
    // This 'struct' is really just a funny kind of namespace. Prevent
    // anyone from accidentally _making_ one.
    linefit() = delete;

    struct point {
        double x, y;
    };
    static int greedy(const point *pts, int npts, double epsilon, int *ret);
    static int optimal(const point *pts, int npts, double epsilon, int *ret);
};

#if 0
{ // mess with editors' indenting
{ // mess with editors' indenting
#endif
} // close namespace ZigguratInternal
using ZigguratInternal::linefit;
} // close namespace Ziggurat
