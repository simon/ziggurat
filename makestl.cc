/*
 * Program to take a collection of outline files with associated
 * heights, and construct an STL file consisting of a series of layers
 * which are extrusions of those outlines.
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include <limits.h>
#include <assert.h>
#include <math.h>

#include <string>
#include <vector>

#include <err.h>

#include "merge.hh"
#include "numparse.hh"
#include "linefit.hh"

using std::string;
using std::map;
using std::vector;
using std::pair;

using Ziggurat::bigrat;
using Ziggurat::OutlineMerge;
using Ziggurat::linefit;

string fgetstring(FILE *fp)
{
    string ret;
    char buf[1024];
    while (fgets(buf, sizeof(buf), fp)) {
	ret += buf;
	if (ret[ret.length()-1] == '\n')
	    break;		       /* got a newline, we're done */
    }
    return ret;
}

// I don't normally enjoy implicit reference parameters but I can't
// resist the temptation to make this look syntactically like the Perl
// version
void chomp(string &s)
{
    size_t len = s.length();
    if (len > 0 && s[len-1] == '\n') {
        len--;
        if (len > 0 && s[len-1] == '\r')
            len--;
    }
    s.resize(len);
}

typedef pair<bigrat, bigrat> Point;
typedef vector<Point> Loop;
typedef vector<Loop> Layer;

struct Aux {
    int a, b;                          // containment above and below
    Aux() : a(0), b(0) {}
    Aux(int aa, int ab) : a(aa), b(ab) {}
    Aux(const Aux &aux) : a(aux.a), b(aux.b) {}
    Aux operator+(const Aux &x) const { return Aux(a + x.a, b + x.b); }
    Aux operator-(const Aux &x) const { return Aux(a - x.a, b - x.b); }
    bool operator==(const Aux &x) const { return a == x.a && b == x.b; }
    Aux operator-() const { return Aux(-a, -b); }
};

typedef OutlineMerge<bigrat, Aux> OurOutlineMerge;

void build_horizontal_surface(OurOutlineMerge &om,
                              const Layer *belowp, const Layer *abovep)
{
    if (belowp) {
        const Layer &below = *belowp;
        for (int i = 0; below.size() > i; i++) { // arrgh, emacs
            const Loop &loop = below[i];
            const Point *prev = &loop[loop.size() - 1];
            for (int j = 0; j < loop.size(); j++) {
                om.add_edge(prev->first, prev->second,
                            loop[j].first, loop[j].second, Aux(0, 1));
                prev = &loop[j];
            }
        }
    }

    if (abovep) {
        const Layer &above = *abovep;
        for (int i = 0; above.size() > i; i++) { // arrgh, emacs
            const Loop &loop = above[i];
            const Point *prev = &loop[loop.size() - 1];
            for (int j = 0; j < loop.size(); j++) {
                om.add_edge(prev->first, prev->second,
                            loop[j].first, loop[j].second, Aux(1, 0));
                prev = &loop[j];
            }
        }
    }

    om.compute();
}

void check_layer(OurOutlineMerge &below, string &filename)
{
    /*
     * Here we check each layer to see if its postprocessed outline
     * visits any vertex more than once, i.e. has multiple filled
     * regions touching at a corner. This is not illegal in our
     * outline files in general, but will cause trouble if it gets
     * into an STL file. We don't actually refuse to generate the file
     * if so; we just print a warning.
     *
     * The easiest way is to check each vertex in one of the
     * OutlineMerges for the horizontal surfaces adjoining the layer.
     * It doesn't matter which; our parameter 'below' is the surface
     * _below_ the layer being tested, so we'll only care about aux.a
     * of the triangles.
     */
    for (int i = 0; i < below.points.size(); i++) {
        OurOutlineMerge::Point &p = below.points[i];

        int nstartlines = 0;

        /*
         * Go round each edge leaving this vertex, and see if it has a
         * filled region on its right. We expect there should be
         * either 0 or 1 such edge.
         */
        for (int j = 0; j < p.ei_out.size(); j++) {
            OurOutlineMerge::Edge &e = below.edges[p.ei_out[j]];
            if (below.triangles[e.til].aux.a == 0 &&
                below.triangles[e.tir].aux.a != 0) {
                nstartlines++;
            }
        }

        if (nstartlines > 1) {
            char *xs = p.x.stringify(), *ys = p.y.stringify();
            fprintf(stderr, "warning: %s: vertex at (%s,%s) has multiple "
                    "filled regions meeting at it\n",
                    filename.c_str(), xs, ys);
            delete[] xs;
            delete[] ys;
        }
    }
}

class Output {
  public:
    virtual ~Output() {}

    virtual void filename(const char *fname) = 0;
    virtual void title(const string &solidtitle_) {}
    virtual void mtllib(const string &mtllib_) {}
    virtual void set_material(const string &material_) {}

    // a,b,c are specified in clockwise order by the caller
    virtual void triangle(
        const bigrat &ax, const bigrat &ay, const bigrat &az,
        const bigrat &bx, const bigrat &by, const bigrat &bz,
        const bigrat &cx, const bigrat &cy, const bigrat &cz) = 0;
};

class STLOutput : public Output {
    FILE *outfp;
    bool need_close;
    string solidtitle;
  public:
    STLOutput() : outfp(NULL), need_close(false) {}

    void filename(const char *fname) {
        if (!fname) {
            outfp = stdout;
        } else {
            outfp = fopen(fname, "w");
            if (!outfp)
                err(1, "%s: open", fname);
            need_close = true;
        }
    }

    ~STLOutput() {
        if (outfp)
            fprintf(outfp, "endsolid %s\n", solidtitle.c_str());
        if (need_close)
            fclose(outfp);
    }

    void title(const string &solidtitle_) {
        solidtitle = solidtitle_;
        fprintf(outfp, "solid %s\n", solidtitle.c_str());
    }

    // Parameters are declared in the order 0,2,1 to accomplish the
    // clockwise -> anticlockwise reversal that STL expects
    void triangle(
        const bigrat &x0, const bigrat &y0, const bigrat &z0,
        const bigrat &x2, const bigrat &y2, const bigrat &z2,
        const bigrat &x1, const bigrat &y1, const bigrat &z1) {
        double xn = (y1-y0)*(z2-z1) - (y2-y1)*(z1-z0);
        double yn = (z1-z0)*(x2-x1) - (z2-z1)*(x1-x0);
        double zn = (x1-x0)*(y2-y1) - (x2-x1)*(y1-y0);
        double nreciplen = 1 / sqrt(xn*xn + yn*yn + zn*zn);

        fprintf(outfp,
                "facet normal %.17g %.17g %.17g\n"
                "outer loop\n"
                "vertex %.17g %.17g %.17g\n"
                "vertex %.17g %.17g %.17g\n"
                "vertex %.17g %.17g %.17g\n"
                "endloop\n"
                "endfacet\n",
                xn * nreciplen, yn * nreciplen, zn * nreciplen,
                (double)x0, (double)y0, (double)z0,
                (double)x1, (double)y1, (double)z1,
                (double)x2, (double)y2, (double)z1);
    }
};

class BinarySTLOutput : public Output {
    /*
     * Source: https://en.wikipedia.org/wiki/STL_(file_format)#Binary_STL
     *
     * This claims that the format consists of:
     *  - an initial 80-byte header, completely ignored except that it
     *    shouldn't start with the word "solid" to avoid confusion
     *    with text STL. I've set this to all zero bytes.
     *  - a 4-byte little-endian integer counting the triangles in the
     *    file, which I go back and fill in at the end (hence the
     *    inability to output binary STL on stdout).
     *  - that many 50-byte triangle records. Each record contains 12
     *    little-endian IEEE 754 single-precision floats, in four
     *    x,y,z triples, giving respectively the normal vector and the
     *    three points of the triangle; those floats occupy 48 bytes
     *    of the 50, and the remaining two are a little-endian uint16
     *    field labelled 'attribute byte count' which Wikipedia says
     *    you should set to zero.
     */

    FILE *outfp;
    size_t ntriangles;
    string solidtitle;
  public:
    BinarySTLOutput() : outfp(NULL), ntriangles(0) {}

    void filename(const char *fname) {
        if (!fname)
            errx(1, "cannot write binary STL to standard output; use -o");

        outfp = fopen(fname, "w");
        if (!outfp)
            err(1, "%s: open", fname);

        // 80-byte ignored header + leave 4 bytes for the triangle
        // count. We'll go back and fill in the latter properly when
        // we close the file.
        char header[84];
        memset(header, 0, 84);
        fwrite(header, 1, 84, outfp);
    }

    ~BinarySTLOutput() {
        if (outfp) {
            fseek(outfp, 80, SEEK_SET);
            write_le_word(ntriangles, 4);
            fclose(outfp);
        }
    }

    void write_le_word(unsigned long w, int n) {
        assert(outfp);
        while (n-- > 0) {
            fputc((w & 0xFF), outfp);
            w >>= 8;
        }
    }

    void write_le_float(float f) {
        /*
         * I make the shameful assumption here that I can memcpy a
         * float into a uint32_t to get its IEEE 754 single precision
         * bit pattern. If that were untrue on some platform then I
         * suppose I'd have to do something more fiddly here involving
         * manually building an IEEE bit pattern from a binary
         * exponent and fraction obtained from some other source, such
         * as frexp() or sprintf("%a") or some such.
         */
        uint32_t u;
        enum {
            static_assertion_f = 1 / (sizeof(f) == 4),
            static_assertion_u = 1 / (sizeof(u) == 4),
        };
        memcpy(&u, &f, 4);
        write_le_word(u, 4);
    }

    // Parameters are declared in the order 0,2,1 to accomplish the
    // clockwise -> anticlockwise reversal that STL expects
    void triangle(
        const bigrat &x0, const bigrat &y0, const bigrat &z0,
        const bigrat &x2, const bigrat &y2, const bigrat &z2,
        const bigrat &x1, const bigrat &y1, const bigrat &z1) {
        double xn = (y1-y0)*(z2-z1) - (y2-y1)*(z1-z0);
        double yn = (z1-z0)*(x2-x1) - (z2-z1)*(x1-x0);
        double zn = (x1-x0)*(y2-y1) - (x2-x1)*(y1-y0);
        double nreciplen = 1 / sqrt(xn*xn + yn*yn + zn*zn);

        write_le_float(xn * nreciplen);
        write_le_float(yn * nreciplen);
        write_le_float(zn * nreciplen);
        // GMP doesn't provide a rational-to-float conversion, only
        // rational-to-double. I think for this application I can
        // tolerate the resulting risk of rounding twice in the same
        // direction, since after all it only adds 2^{-30} ULP to the
        // maximum rounding error.
        write_le_float((double)x0);
        write_le_float((double)y0);
        write_le_float((double)z0);
        write_le_float((double)x1);
        write_le_float((double)y1);
        write_le_float((double)z1);
        write_le_float((double)x2);
        write_le_float((double)y2);
        write_le_float((double)z2);
        write_le_word(0, 2);

        ntriangles++;
    }
};

class OBJOutput : public Output {
    FILE *outfp;
    bool need_close;

    struct XYZ {
        bigrat x, y, z;
        bool operator<(const XYZ &rhs) const {
            return (x < rhs.x ? true : x > rhs.x ? false :
                    y < rhs.y ? true : y > rhs.y ? false :
                    z < rhs.z);
        }
    };
    map<XYZ, unsigned> vertex_ids;
    unsigned next_vertex_id;
    unsigned vertex_id(const bigrat &x, const bigrat &y, const bigrat &z) {
        XYZ xyz = { x, y, z };
        map<XYZ, unsigned>::iterator it = vertex_ids.find(xyz);
        if (it != vertex_ids.end())
            return it->second;
        fprintf(outfp, "v %.17g %.17g %.17g\n",
                (double)x, (double)y, (double)z);
        vertex_ids[xyz] = next_vertex_id;
        return next_vertex_id++;
    }

    string material;

  public:
    OBJOutput() : outfp(NULL), need_close(false), next_vertex_id(1) {}

    void filename(const char *fname) {
        if (!fname) {
            outfp = stdout;
        } else {
            outfp = fopen(fname, "w");
            if (!outfp)
                err(1, "%s: open", fname);
            need_close = true;
        }
    }

    void mtllib(const string &filename) {
        fprintf(outfp, "mtllib %s\n", filename.c_str());
    }
    void set_material(const string &material_) {
        if (material != material_) {
            material = material_;
            fprintf(outfp, "usemtl %s\n", material.c_str());
        }
    }

    ~OBJOutput() {
        if (need_close)
            fclose(outfp);
    }

    // Parameters are declared in the order 0,2,1 to accomplish the
    // clockwise -> anticlockwise reversal that OBJ expects
    void triangle(
        const bigrat &x0, const bigrat &y0, const bigrat &z0,
        const bigrat &x2, const bigrat &y2, const bigrat &z2,
        const bigrat &x1, const bigrat &y1, const bigrat &z1) {
        unsigned v0 = vertex_id(x0, y0, z0);
        unsigned v1 = vertex_id(x1, y1, z1);
        unsigned v2 = vertex_id(x2, y2, z2);
        fprintf(outfp, "f %u %u %u\n", v0, v1, v2);
    }
};

Output *output;

void output_triangle(OurOutlineMerge::Point &p0, bigrat &z0,
                     OurOutlineMerge::Point &p1, bigrat &z1,
                     OurOutlineMerge::Point &p2, bigrat &z2,
                     const bigrat &hscale, const string &mtl)
{
    output->set_material(mtl);
    output->triangle(p0.x * hscale, p0.y * hscale, z0,
                     p1.x * hscale, p1.y * hscale, z1,
                     p2.x * hscale, p2.y * hscale, z2);
}

void output_horizontal_surface(
    OurOutlineMerge &om, bigrat &z, const bigrat &hscale,
    const string &mtlup, const string &mtldown)
{
    for (int i = 0; i < om.triangles.size(); i++) {
        OurOutlineMerge::Triangle &t = om.triangles[i];
        if (t.aux.b != 0 && t.aux.a == 0) {
            /*
             * Triangle pointing upwards.
             */
            output_triangle(om.points[t.pi[0]], z,
                            om.points[t.pi[1]], z,
                            om.points[t.pi[2]], z,
                            hscale, mtlup);
        } else if (t.aux.a != 0 && t.aux.b == 0) {
            /*
             * Triangle pointing downwards.
             */
            output_triangle(om.points[t.pi[0]], z,
                            om.points[t.pi[2]], z,
                            om.points[t.pi[1]], z,
                            hscale, mtldown);
        }
    }
}

inline int sign(bigrat x) { return x < 0 ? -1 : x > 0 ? +1 : 0; }

bool in_right_direction(OurOutlineMerge::Point a,
                        OurOutlineMerge::Point b,
                        OurOutlineMerge::Point c)
{
    /*
     * Return true iff vectors ab and ac are positive scalar multiples
     * of one another.
     */
    bigrat abx = b.x - a.x, aby = b.y - a.y, acx = c.x - a.x, acy = c.y - a.y;
    return (sign(abx)==sign(acx) &&
            sign(aby)==sign(acy) &&
            abx * acy == aby * acx);
}

void output_layer(Layer &layer,
                  OurOutlineMerge &below, bigrat &zbelow,
                  OurOutlineMerge &above, bigrat &zabove,
                  const bigrat &hscale, const string &mtl)
{
    for (int i = 0; layer.size() > i; i++) { // arrgh, emacs
        const Loop &loop = layer[i];
        const Point *prev = &loop[loop.size() - 1];
        for (int j = 0; j < loop.size(); j++) {
            /*
             * Find the indices of the start and end points of this
             * layer edge in the merged outline graphs below and above
             * the layer.
             */
            int bstart = below.pointfinder[*prev];
            int astart = above.pointfinder[*prev];
            int bend = below.pointfinder[loop[j]];
            int aend = above.pointfinder[loop[j]];

            /*
             * Proceed through the 'below' outline graph from the
             * start point towards the end point, finding any
             * intermediate points along the way introduced by merging
             * with the outline of the next layer down. Connect all
             * points we find to the start point at the top of this
             * layer.
             */
            while (bstart != bend) {
                OurOutlineMerge::Point &p = below.points[bstart];
                int k, nextpt;
                for (k = 0; k < p.ei_out.size(); k++) {
                    nextpt = below.edges[p.ei_out[k]].pi1;
                    if (in_right_direction(below.points[bstart],
                                           below.points[nextpt],
                                           below.points[bend]))
                        break;
                }
                assert(k < p.ei_out.size());

                /*
                 * Check the aux for the triangles on either side of
                 * this edge in the 'below' surface, in case the user
                 * specified this outline anticlockwise rather than
                 * clockwise.
                 */
                int flip;
                OurOutlineMerge::Edge &e = below.edges[p.ei_out[k]];
                if (below.triangles[e.til].aux.a == 0)
                    flip = 0;          // right way round already
                else
                    flip = bstart ^ nextpt; // backwards - reverse points

                output_triangle(above.points[astart], zabove,
                                below.points[bstart ^ flip], zbelow,
                                below.points[nextpt ^ flip], zbelow,
                                hscale, mtl);
                bstart = nextpt;
            }

            /*
             * Now proceed through 'above' in the same way, connect
             * all points to the _end_ point at the bottom.
             */
            while (astart != aend) {
                OurOutlineMerge::Point &p = above.points[astart];
                int k, nextpt;
                for (k = 0; k < p.ei_out.size(); k++) {
                    nextpt = above.edges[p.ei_out[k]].pi1;
                    if (in_right_direction(above.points[astart],
                                           above.points[nextpt],
                                           above.points[aend]))
                        break;
                }
                assert(k < p.ei_out.size());

                /*
                 * As above, check the sense of this edge, but this
                 * time we do it in the 'above' surface because that's
                 * where this is guaranteed to be exactly one edge.
                 */
                int flip;
                OurOutlineMerge::Edge &e = above.edges[p.ei_out[k]];
                if (above.triangles[e.til].aux.b == 0)
                    flip = 0;          // right way round already
                else
                    flip = astart ^ nextpt; // backwards - reverse points

                output_triangle(below.points[bend], zbelow,
                                above.points[nextpt ^ flip], zabove,
                                above.points[astart ^ flip], zabove,
                                hscale, mtl);
                astart = nextpt;
            }

            prev = &loop[j];
        }
    }
}

bool reduce_loop(Loop &loop, double epsilon)
{
    int n = loop.size();
    linefit::point *pts = new linefit::point[n + 1];
    for (int i = 0; i < n; i++) {
        pts[i].x = loop[i].first;
        pts[i].y = loop[i].second;
    }
    pts[n] = pts[0];
    int *indices = new int[n + 1];
    int nindices = linefit::optimal(pts, n + 1, epsilon, indices);
    Loop newloop;
    // Ignore final entry in indices[] because it's the same as entry
    // 0. FIXME: it would be nicer here if we could optimise without
    // bias towards a particular starting point.
    for (int i = 0; i < nindices-1; i++)
        newloop.push_back(loop[indices[i]]);
    loop = newloop;
    delete[] indices;
    delete[] pts;

    // Return failure if we ended up with only one point.
    return loop.size() > 1;
}

int main(int argc, char **argv)
{
    char *outfile = NULL;
    vector<string> layerfiles;
    vector<bigrat> layerheights;
    vector<string> layermtls;
    vector<Layer> layers;
    string solidtitle = "ziggurat";
    bigrat currheight(2);
    bigrat hscale(1);
    double reduce_eps = 0;
    string mtllib, currmtl;

    output = new STLOutput;

    bool doing_opts = true;
    while (--argc > 0) {
        char *p = *++argv;
        if (doing_opts && p[0] == '-') {
            if (p[1] == '-') {
                if (!strcmp(p, "--")) {
                    doing_opts = false;
                } else if (!strcmp(p, "--help")) {
                    printf("usage:   makestl <options> <outline> [[options] <outline>...]\n"
                           "options: -o OUTFILE      output STL file name\n"
                           "         -h HEIGHT       height of layer files following this option\n"
                           "         -t TITLE        textual title to write in the STL file\n"
                           "         -s HSCALE       scale factor to apply to horizontal dimensions\n"
                           "         -r EPSILON      reduce outline with tolerance EPSILON\n"
                           "         -O FORMAT       specify output file format: 'stl' or 'obj'\n"
                           "         -M MTLLIB       specify materials library filename (.obj only)\n"
                           "         -m MATERIAL     specify material name for following layers (.obj)\n");
                    return 0;
                } else {
                    errx(1, "unrecognised option '%s'", p);
                }
            } else {
                char c;
                while (*p && (c = *++p) != '\0') {
                    switch (c) {
                      case 'h':
                      case 's':
                      case 't':
                      case 'o':
                      case 'O':
                      case 'r':
                      case 'M':
                      case 'm':
                        /*
                         * Options requiring an argument.
                         */
                        char *arg = p+1;
                        if (*arg)
                            p += strlen(p); // eat remainder of argv word
                        else if (--argc > 0)
                            arg = *++argv;
                        else
                            errx(1, "expected an argument to option '-%c'", c);

                        switch (c) {
                          case 'h':
                            if (!numparse(arg, &currheight) || currheight <= 0)
                                errx(1, "unable to parse '%s' as a height",
                                    arg);
                            break;
                          case 's':
                            if (!numparse(arg, &hscale) || hscale <= 0)
                                errx(1, "unable to parse '%s' as a scale factor",
                                    arg);
                            break;
                          case 't':
                            solidtitle = arg;
                            break;
                          case 'o':
                            outfile = arg;
                            break;
                          case 'M':
                            mtllib = arg;
                            break;
                          case 'm':
                            currmtl = arg;
                            break;
                          case 'O':
                            delete output;
                            if (!strcmp(arg, "stl"))
                                output = new STLOutput;
                            else if (!strcmp(arg, "stlbin"))
                                output = new BinarySTLOutput;
                            else if (!strcmp(arg, "obj"))
                                output = new OBJOutput;
                            else
                                errx(1, "unrecognised output format '%s'",
                                     arg);
                          case 'r':
                            reduce_eps = atof(arg);
                            break;
                        }
                    }
                }
            }
        } else {
            layerfiles.push_back(p);
            layerheights.push_back(currheight);
            layermtls.push_back(currmtl);
        }
    }

    if (layerfiles.size() == 0)
        errx(1, "expected at least one input file name");

    for (int i = 0; i < layerfiles.size(); i++) {
        const char *fname = layerfiles[i].c_str();
        FILE *fp = fopen(fname, "r");
        if (!fp)
            err(1, "'%s': open", fname);

        layers.push_back(Layer());
        Layer &layer = layers[layers.size()-1];

        string line;
        int lineno = 0;
        while ((lineno++, line = fgetstring(fp)) != "") {
            chomp(line);
            int npoints;
            if (sscanf(line.c_str(), "loop %d", &npoints) != 1)
                errx(1, "%s:%d: expected a 'loop' line", fname, lineno);

            Loop loop;

            while (npoints-- > 0) {
                if ((lineno++, line = fgetstring(fp)) == "")
                    errx(1, "%s:%d: expected a 'point' line", fname, lineno);
                chomp(line);
                if (line.substr(0, 6) != "point ")
                    errx(1, "%s:%d: expected a 'point' line", fname, lineno);
                size_t nextspace = line.find(' ', 6);
                if (nextspace == string::npos)
                    errx(1, "%s:%d: unable to parse point specification '%s'",
                         fname, lineno, line.c_str());
                string xs = line.substr(6, nextspace-6);
                string ys = line.substr(nextspace+1);
                bigrat x, y;
                if (!numparse(xs.c_str(), &x) || !numparse(ys.c_str(), &y))
                    errx(1, "%s:%d: unable to parse point specification '%s'",
                         fname, lineno, line.c_str());
                loop.push_back(Point(x, y));
            }

            if ((lineno++, line = fgetstring(fp)) == "")
                errx(1, "%s:%d: expected an 'endloop' line", fname, lineno);
            chomp(line);
            if (line != "endloop")
                errx(1, "%s:%d: expected an 'endloop' line", fname, lineno);

            if (reduce_eps) {
                if (!reduce_loop(loop, reduce_eps))
                    continue;
            }
            layer.push_back(loop);
        }

        fclose(fp);
    }

    vector<OurOutlineMerge> surfaces(layers.size() + 1);
    for (int i = 0; i < surfaces.size(); i++)
        build_horizontal_surface(surfaces[i],
                                 i == 0 ? NULL : &layers[i-1],
                                 i == layers.size() ? NULL : &layers[i]);

    for (int i = 0; i < layers.size(); i++)
        check_layer(surfaces[i], layerfiles[i]);

    output->filename(outfile);
    output->title(solidtitle);
    if (!mtllib.empty())
        output->mtllib(mtllib);

    bigrat z = 0;
    for (int i = 0; i < surfaces.size(); i++) {
        output_horizontal_surface(
            surfaces[i], z, hscale,
            i == 0 ? "" : layermtls[i-1],
            i == layermtls.size() ? "" : layermtls[i]);
        if (i < layers.size()) {
            bigrat znew = z + layerheights[i];
            output_layer(layers[i], surfaces[i], z, surfaces[i+1], znew,
                         hscale, layermtls[i]);
            z = znew;
        }
    }

    delete output;

    return 0;
}
