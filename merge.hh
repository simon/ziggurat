/*
 * merge.hh: merge together collections of cycles and yield analysis
 * data including which edges bound the overall interior and which
 * parts of the plane are in one collection but not another.
 */

#ifndef MERGE_HH
#define MERGE_HH

#include "triangulate.hh"
#include "planarise.hh"
#include "edsf.hh"

namespace Ziggurat {
namespace ZigguratInternal {
#if 0
} // mess with editors' indenting
} // mess with editors' indenting
#endif

using std::set;
using std::map;
using std::vector;
using std::pair;

template<class NumberTemplateParam, class AuxTemplateParam>
class OutlineMerge {
  public:
    typedef NumberTemplateParam Number;
    typedef AuxTemplateParam Aux;
  private:
    struct IntermediateGraph {
        typedef NumberTemplateParam Number;
        typedef AuxTemplateParam Aux;
        struct Vertex {
            Number xc, yc;

            Vertex(const Number &ax, const Number &ay)
                : xc(ax), yc(ay) {}

            Number x() const { return xc; }
            Number y() const { return yc; }

            Vertex(const Vertex &v) : xc(v.xc), yc(v.yc) {}

            bool operator< (const Vertex &b) const {
                if (yc != b.yc)
                    return yc < b.yc;
                return xc < b.xc;
            }
        };

        set<Vertex> vertices;
        map<Vertex, map<Vertex, Aux> > edges;

        void add_vertex(Number x, Number y) {
            Vertex v(x, y);
            vertices.insert(v);
        }

        void add_edge(Number x0, Number y0, Number x1, Number y1, Aux aux) {
            Vertex v0(x0, y0), v1(x1, y1);
            vertices.insert(v0);
            vertices.insert(v1);
            edges[v0][v1] = edges[v0][v1] + aux;
            edges[v1][v0] = edges[v1][v0] - aux;
        }

        void enum_vertices(vector<const Vertex *> &out) const {
            for (const Vertex &v: vertices)
                out.push_back(&v);
        }
        void enum_neighbours(const Vertex *v,
                             vector<const Vertex *> &out) const {
            auto ei = edges.find(*v);
            if (ei != edges.end()) {
                for (const auto &kv: ei->second)
                    out.push_back(&(*vertices.find(kv.first)));
            }
        }
        Aux get_aux(const Vertex *start, const Vertex *end) const {
            return edges.find(*start)->second.find(*end)->second;
        }
    };

    IntermediateGraph ig;
    typedef Planarisation<IntermediateGraph> OurPlanarisation;
    typedef typename OurPlanarisation::Point OurPoint;
    typedef Triangulation<OurPlanarisation> OurTriangulation;
    typedef pair<OurPoint, OurPoint> OurPointPair;

    OurPlanarisation *P;
    OurTriangulation *T;

    map<OurPointPair, int> edgemap;

    bool compute_face_aux; // inhibits EDSF computation for special purposes

  public:
    // Publicly visible data structures describing the output layout.
    struct Point {
        Number x, y;
        // Edges are indexed clockwise around the point
        vector<int> ei_out, ei_in; // indices into the 'edges' array
      private:
        friend class OutlineMerge;
        Point(Number ax, Number ay) : x(ax), y(ay) {}
    };
    struct Edge {
        // Edges are stored in _both_ directions separately
        int pi0, pi1;                  // indices into the 'points' array
        int til, tir;   // indices into the 'triangles' array, or 'exterior'
        int reverse; // index of the reverse of this edge in the 'edges' array
        Aux aux;     // has the default-constructed value if not an input edge
      private:
        friend class OutlineMerge;
        Edge() {}
    };
    struct Triangle {
        // Both points and edges are indexed clockwise around the
        // triangle. Edge ei[i] runs from pi[i] to pi[(i+1) % 3].
        int pi[3];                     // indices into the 'points' array
        int ei[3];                     // indices into the 'edges' array
        Aux aux;
      private:
        friend class OutlineMerge;
        Triangle(Aux aa) : aux(aa) {}
    };
    vector<Point> points;
    vector<Edge> edges;
    vector<Triangle> triangles;
    int exterior;       // index beyond the end of 'triangles' array
    map<pair<Number, Number>, int> pointfinder;

    OutlineMerge() : P(NULL), T(NULL), compute_face_aux(true) {}
    ~OutlineMerge() { if (P) delete P; if (T) delete T; }

    void no_face_aux() { compute_face_aux = false; }

    void add_vertex(Number x, Number y) {
#ifdef MERGE_DIAGNOSTICS
        fprintf(stderr,
                "merge: add graph vertex (%g,%g)\n", (double)x0, (double)y0);
#endif
        ig.add_vertex(x, y);
    }

    void add_edge(Number x0, Number y0, Number x1, Number y1, Aux aux) {
#ifdef MERGE_DIAGNOSTICS
        fprintf(stderr,
                "merge: add graph edge (%g,%g) -> (%g,%g)\n",
                (double)x0, (double)y0, (double)x1, (double)y1);
#endif
        ig.add_edge(x0, y0, x1, y1, aux);
    }

    void compute() {
        P = new OurPlanarisation(ig);
        T = new OurTriangulation(*P);

        exterior = T->triangles.size();
#ifdef MERGE_DIAGNOSTICS
        fprintf(stderr,
                "merge: %d triangles, so %d faces; exterior is #%d\n",
                T->triangles.size(), exterior+1, exterior);
#endif

        for (int ti = 0; ti < T->triangles.size(); ti++) {
            const auto &t = T->triangles[ti];

#ifdef MERGE_DIAGNOSTICS
            fprintf(stderr,
                    "merge: triangle #%d: (%g,%g)-(%g,%g)-(%g,%g)\n",
                    ti,
                    (double)t.v[0]->x(), (double)t.v[0]->y(),
                    (double)t.v[1]->x(), (double)t.v[1]->y(),
                    (double)t.v[2]->x(), (double)t.v[2]->y());
#endif
            /*
             * The triangles coming out of Triangulation are always
             * oriented clockwise, which is how we want them.
             */
            for (int i = 0; i < 3; i++) {
                const OurPoint *v0 = t.v[i];
                const OurPoint *v1 = t.v[(i+1) % 3];

                edgemap[OurPointPair(*v0, *v1)] = ti;
#ifdef MERGE_DIAGNOSTICS
                fprintf(stderr,
                        "merge: map edge (%g,%g) -> (%g,%g) to triangle #%d\n",
                        (double)v0->x(), (double)v0->y(),
                        (double)v1->x(), (double)v1->y(),
                        ti);
#endif
            }
        }

        /*
         * Find all the one-sided edges in the above, which must also
         * border the exterior face.
         */
        vector<OurPointPair> extedges;
        for (auto &kv: edgemap) {
            OurPointPair flip(kv.first.second, kv.first.first);
            if (edgemap.find(flip) == edgemap.end()) {
#ifdef MERGE_DIAGNOSTICS
                fprintf(stderr,
                        "merge: edge (%g,%g) -> (%g,%g) (#%d) is one-sided\n",
                        (double)ei->first.first.x(), (double)ei->first.first.y(),
                        (double)ei->first.second.x(), (double)ei->first.second.y(),
                        ei->second);
#endif
                extedges.push_back(flip);
            }
        }
        for (OurPointPair &ee: extedges)
            edgemap[ee] = exterior;

        EDSF<Aux> edsf(exterior + 1);
        if (compute_face_aux) {
            /*
             * Now go over all the edges doing DSF unifications. This
             * will fail an assertion if we messed up the cyclic
             * properties.
             */
            for (auto &kv: edgemap) {
                OurPointPair flip(kv.first.second, kv.first.first);
                typename IntermediateGraph::Aux aux;
#ifdef MERGE_DIAGNOSTICS
                bool adj = false;
#endif
                if (P->adjacent(kv.first.first, kv.first.second)) {
#ifdef MERGE_DIAGNOSTICS
                    adj = true;
#endif
                    aux = P->get_aux(kv.first.first, kv.first.second);
                }
                int left = edgemap[flip], right = kv.second;
#ifdef MERGE_DIAGNOSTICS
                fprintf(stderr,
                        "merge: edge (%g,%g) -> (%g,%g): "
                        "left #%d, right #%d, aux %d (%s)\n",
                        (double)kv.first.first.x(),
                        (double)kv.first.first.y(),
                        (double)kv.first.second.x(),
                        (double)kv.first.second.y(),
                        left, right, aux.i, adj ? "true" : "false");
#endif
                edsf.merge(left, right, aux);
            }
        }

        /*
         * Now construct simple integer-indexed arrays of points,
         * edges and triangles to use as the output.
         */
        int i;
        map<OurPoint, int> point_indices;

        for (i = 0; i < exterior; i++) {
            Aux aux;
            bool ret;
            if (compute_face_aux) {
                ret = edsf.get_difference(exterior, i, &aux);
                assert(ret);
            }
            triangles.push_back(Triangle(aux));
            Triangle &t = triangles[triangles.size()-1];
            for (int j = 0; j < 3; j++) {
                const OurPoint &p = *T->triangles[i].v[j];
                int pi;
                if (point_indices.find(p) != point_indices.end())
                    pi = point_indices[p];
                else {
                    pi = points.size();
                    points.push_back(Point(p.x(), p.y()));
                    point_indices[p] = pi;
                }
                t.pi[j] = pi;
            }
        }

        /*
         * Add a dummy triangle entry for the exterior face, which
         * won't have valid point or edge indices but will have a
         * valid null Aux.
         */
        triangles.push_back(Triangle(Aux()));
        for (i = 0; i < 3; i++)
            triangles[triangles.size()-1].pi[i] =
                triangles[triangles.size()-1].ei[i] = -1;

        i = 0;
        map<pair<int, int>, int> edge_indices;
        for (auto &kv: edgemap) {
            OurPointPair flip(kv.first.second, kv.first.first);
            int thisindex = edges.size();
            edges.push_back(Edge());
            Edge &e = edges[thisindex];
            e.til = edgemap[flip];
            e.tir = kv.second;
            e.pi0 = point_indices[kv.first.first];
            e.pi1 = point_indices[kv.first.second];
            if (P->adjacent(kv.first.first, kv.first.second)) {
                e.aux = P->get_aux(kv.first.first, kv.first.second);
            }
            edge_indices[pair<int, int>(e.pi0, e.pi1)] = thisindex;
            if (e.tir != exterior) {
                int j;
                for (j = 0; j < 3; j++)
                    if (triangles[e.tir].pi[j] == e.pi0)
                        break;
                assert(j < 3);
                assert(triangles[e.tir].pi[(j+1) % 3] == e.pi1);
                triangles[e.tir].ei[j] = thisindex;
            }
        }

        for (i = 0; i < edges.size(); i++) {
            Edge &e = edges[i];
            e.reverse = edge_indices[pair<int, int>(e.pi1, e.pi0)];
        }

        for (i = 0; i < points.size(); i++) {
            Point &p = points[i];
            OurPoint op(p.x, p.y);
            vector<const OurPoint *> pneighbours = T->neighbours(&op);
            for (int j = 0; j < pneighbours.size(); j++) {
                int pi = point_indices[*pneighbours[j]];
                p.ei_out.push_back(edge_indices[pair<int, int>(i, pi)]);
                p.ei_in.push_back(edge_indices[pair<int, int>(pi, i)]);
            }

            pointfinder[pair<Number,Number>(p.x, p.y)] = i;
        }
    }
};

#if 0
{ // mess with editors' indenting
{ // mess with editors' indenting
#endif
} // close namespace ZigguratInternal
using ZigguratInternal::OutlineMerge;
} // close namespace Ziggurat

#endif /* MERGE_HH */
