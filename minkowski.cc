/*
 * Command-line tool to compute Minkowski sums.
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <limits.h>
#include <assert.h>
#include <math.h>

#include <string>
#include <vector>

#include <err.h>

#include "numparse.hh"
#include "munge.hh"
#include "minkowski.hh"

using std::string;
using std::vector;

using Ziggurat::numparse;
using Ziggurat::Minkowski;
using namespace Ziggurat::Munge;

struct IdentityMatrix {
    Point operator*(const Point &p) const { return p; }
};

class TrivialInsideTest {
  public:
    bool inside(const OurOutlineMerge &om, int ti) const {
        const Aux &aux = om.triangles[ti].aux;
        return aux[0];
    }
};

class NeverLoopReduce {
  public:
    bool reduce_loop(Loop &loop) const {
        return true;
    }
};

int main(int argc, char **argv)
{
    string outfile = "-";
    vector<string> files;

    bool doing_opts = true;
    while (--argc > 0) {
        char *p = *++argv;
        if (doing_opts && p[0] == '-' && p[1]) {
            if (p[1] == '-') {
                if (!strcmp(p, "--")) {
                    doing_opts = false;
                } else if (!strcmp(p, "--help")) {
                    printf("usage:   minkowski [options <outline> [<outline>]\n"
                           "options: -o OUTFILE      output outline file name\n"
                           "note:    if only one input file given, standard input will be used as the\n"
                           "         other one\n");
                    return 0;
                } else {
                    errx(1, "unrecognised option '%s'", p);
                }
            } else {
                char c;
                while (*p && (c = *++p) != '\0') {
                    switch (c) {
                      case 'o':
                        /*
                         * Options requiring an argument.
                         */
                        char *arg = p+1;
                        if (*arg)
                            p += strlen(p); // eat remainder of argv word
                        else if (--argc > 0)
                            arg = *++argv;
                        else
                            errx(1, "expected an argument to option '-%c'", c);

                        switch (c) {
                          case 'o':
                            outfile = arg;
                            break;
                        }
                        break;
                    }
                }
            }
        } else {
            files.push_back(p);
        }
    }

    if (files.size() == 1)
        files.push_back("-");
 
    if (files.size() != 2)
        errx(1, "expected exactly two input files");

    OurOutlineMerge inputs[2], output;

    for (int i = 0; i < 2; i++) {
        Outline outline;
        read_outline(outline, files[i], IdentityMatrix(), NeverLoopReduce());
        add_to_om(inputs[i], outline, 0);
        inputs[i].compute();
    }

    /*
     * Now run the Minkowski sum algorithm.
     */
    Minkowski<OurOutlineMerge, Point, TrivialInsideTest>
        (TrivialInsideTest(), Aux(0, 1)).compute(output, inputs[0], inputs[1]);
    output.compute();

    /*
     * And output.
     */
    Outline normalised_result;
    om_result(normalised_result, output, TrivialInsideTest());
    write_outline(outfile, normalised_result);

    return 0;
}
