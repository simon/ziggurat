/*
 * Algorithm to compute the Minkowski sum of two arbitrary collections
 * of polygons.
 */

#include <list>
#include <map>
#include <set>
#include <algorithm>
#include <iostream>

namespace Ziggurat {
namespace ZigguratInternal {
#if 0
} // mess with editors' indenting
} // mess with editors' indenting
#endif

using std::string;
using std::list;
using std::map;
using std::multiset;
using std::multimap;
using std::pair;
using std::clog;
using std::endl;

/*
 * The basic idea:
 *
 * First, imagine we just have two convex polygons. Imagine tracing
 * round the boundary of one polygon A, with the other one B glued on
 * the end of your pencil, and see what edges you get in the output
 * polygon.
 *
 * Clearly for each edge of A, there will be some edge with the same
 * length and direction in the output, traced out as you move the
 * pencil by whatever point on B is furthest away in the perpendicular
 * direction. (Special case if a B-edge is actually parallel to that
 * A-edge - in that case you get an edge of the summed lengths of
 * both.)
 *
 * And at every _vertex_ of A, where the pencil stops moving in one
 * direction and starts moving in the other, you'll potentially get
 * some edges of B appearing. Specifically, an edge of B will appear
 * if neither the incoming nor outgoing direction of the pencil pushed
 * polygon B any further in the direction of that edge's normal.
 *
 * What this works out to - and if you do a few examples on paper this
 * becomes fairly clear - is that you expect to end up with an output
 * polygon whose edges have the same set of vectors as both the inputs
 * put together, and it will still be convex, i.e. those vectors
 * appear in order of their angle around the origin. It's a bit like
 * seeing each polygon as a list of displacement vectors sorted by
 * angle, and merging those sorted lists.
 *
 * That's an easy case. Next case: suppose polygon A isn't convex. Now
 * what happens?
 *
 * Now if you imagine dragging B around the edge of A, you find that
 * every time A makes a turn in the wrong direction, you have to keep
 * going in the _right_ direction around B, until you get back to the
 * direction that the outgoing edge of A wants to go in. So you make
 * multiple trips round the edge of B for one pass around the edge of
 * A, and as a result at every reflex (non-convex) vertex of A,
 * there's a little loop shaped like part of B's outline. Those loops
 * disappear if you then do a nonzero-winding-number fill of the
 * result (which is easy when we have OutlineMerge available). So this
 * still looks a lot like merging sorted lists; it's just that one
 * list isn't quite sorted, and when it goes the wrong way, you
 * compensate by going all the way round the other until you get back
 * to the place where the next element fits.
 *
 * Now what if _neither_ polygon is convex?
 *
 * Well, in that case, this technique stops working. So the easiest
 * thing is just to solve that by chopping up one of the polygons into
 * convex pieces. But at least we don't have to chop up both - by the
 * above discussion, just one is enough.
 *
 * Finally, what if the polygons are complex - they have holes in the
 * middle, or are disconnected? Turns out that we just have to
 * generate one of these traces-round-the-edge for each loop in its
 * outline, and that's good enough. (At least, for the polygon we
 * didn't chop up. For the one we did, this doesn't come up anyway
 * because each individual piece is nice and simple.)
 *
 * Since we're outputting into another OutlineMerge, there's no
 * particular need to output any of these twirly loops in their actual
 * edge order; as long as we emit the right _set_ of edges (with the
 * right multiplicity), that's sufficient. So in fact what we do is to
 * run a single 360-degree loop round all the possible _direction_,
 * from the positive x direction anticlockwise back to itself, and
 * process each edge as we get to its direction. Specifically: for a
 * pair of edges in polygons A,B in the same direction, we output the
 * Minkowski sum of those edges (i.e. a longer edge from
 * start(e_A)+start(e_B) to end(e_A)+end(e_B)), and for each edge in
 * either polygon we also output its Minkowski sum with every _vertex_
 * (i.e. just translate the edge to the right place) whose incoming
 * and outgoing edges are angled so as to make that output edge
 * required.
 */

template<class OutlineMerge, class Point, class InsideTest> class Minkowski {
    typedef typename OutlineMerge::Number Number;

    /*
     * We're going to want to go through vectors in order of their
     * angle around the origin. But actually doing trigonometry is
     * massive overkill for this. This function computes a value which
     * is _ordered_ the same as the angle, but is easy to compute in
     * rationals and can be compared exactly.
     *
     * Specifically: if x > 0 and y >= 0 (that is, we're in the upper
     * right quarter-plane or exactly on the positive x-axis), then
     * (x,y) |-> y/x is a perfectly good function that gives the
     * angular ordering between vectors. If we then transform that in
     * turn through the function a |-> a/(a+1), then we map it into
     * the interval [0,1).
     *
     * A general x,y can be moved into that quarter-plane by _exactly
     * one_ of the four possible rotations by a multiple of 90 degrees
     * (unless x=y=0, which is a disallowed input), so we find out
     * which, and add that on as an integer part.
     *
     * So this function always returns a value in the range [0,4),
     * where 0 is the positive x-axis, the positive sense is
     * anticlockwise, and 'angles' approaching 4 correspond to things
     * just below the positive x-axis.
     */
    static Number sortable_angle(const Point &p) {
        Number x = p.x, y = p.y;

        int quadrant = 0;
        while (!(x > 0 && y >= 0)) {
            Number tmp = x; x = y; y = -tmp;
            quadrant++;
        }

        Number yox = y / x;
        return quadrant + yox / (yox + 1);
    }

    /*
     * Parameters received from our client in the constructor.
     */
    const InsideTest &inside;
    const typename OutlineMerge::Aux &aux;

    /*
     * Helper functions.
     */
    inline Point edgevector(OutlineMerge &om, int ei) {
        return (Point(om.points[om.edges[ei].pi1]) -
                Point(om.points[om.edges[ei].pi0]));
    }

    inline Point edgevector_reversed(OutlineMerge &om, int ei) {
        return (Point(om.points[om.edges[ei].pi0]) -
                Point(om.points[om.edges[ei].pi1]));
    }

    /*
     * After our initial setup phase, we'll have queued up a bunch of
     * actions to be taken, all sorted by the angle at which they
     * occur.
     */
    struct Actions {
        list<int> points_removed;
        list<int> edges;
        list<int> points_inserted;
    };
    map<Number, Actions> actions[2];
    multiset<int> points[2];

    /*
     * Determine whether two edges of an OutlineMerge meet in a reflex
     * angle. The semantics of the edges are that both will be
     * outgoing from the same point (because that's what's convenient
     * to compute at the call site), but ei0 is considered as if it
     * was coming _in_ (because that's what I'm really interested in).
     * So the question is: if you follow ei0 backwards, and then ei1
     * forwards, did you turn right at the vertex in between?
     * Continuing in exactly the same direction (which can happen in
     * an OutlineMerge, if some edge we're not interested in
     * intersected one we were) counts as a reflex angle, on the basis
     * that I'm not sure what the main Minkowski sum algorithm will do
     * with that situation and so we should just arrange to break it
     * up.
     */
    bool reflex_angle(OutlineMerge &om, int ei0, int ei1) {
        Point v0 = edgevector_reversed(om, ei0);
        Point v1 = edgevector(om, ei1);
        return v0.y * v1.x - v0.x * v1.y >= 0;
    }

    /*
     * Preparation phase, called once for each input outline.
     *
     * Given an OutlineMerge equipped with a notion of which triangles
     * are 'inside', schedule actions to be taken in the main
     * algorithm for each of the edges that separate inside from
     * outside.
     *
     * If 'make_convex' is true, we also apply the Hertel-Mehlhorn
     * algorithm to add interior diagonals to the set of edges in such
     * a way as to decompose its interior into a set of convex
     * polygons, and then we schedule actions for those extra
     * diagonals as well.
     *
     * The Hertel-Mehlhorn algorithm, as distilled from sources on the
     * Internet such as
     * http://www.dma.fi.upm.es/recursos/aplicaciones/geometria_computacional_y_grafos/web/piezas_convexas/algoritmo_i_Partic.html
     * is as follows:
     *
     *  - Start with a complete triangulation of your polygon, which
     *    obviously does partition it into convex parts.
     *  - Then iterate over each diagonal in the triangulation, and
     *    remove that diagonal iff doing so does not create any
     *    non-convexity.
     *  - You can tell that simply by looking at the pair of diagonals
     *    or edges (at each endpoint) that would become adjacent if
     *    this diagonal were removed, and seeing if they form a reflex
     *    angle.
     *  - If neither endpoint has that problem, the diagonal can be
     *    removed without creating a non-convex part, so you now have
     *    a partition into one part fewer.
     *
     * Hertel-Mehlhorn doesn't create a minimal convex partition. But
     * there's a theorem that it never creates more than 4 times (i.e.
     * O(1) times) as many parts as the minimal one would contain. And
     * apparently to get a properly minimal partition you potentially
     * need to invent internal vertices, and the best known algorithm
     * is a cubic-time dynamic programming approach. So for these
     * purposes that would be overkill - we might well waste more time
     * on the convex partitioning than we save on doing it slightly
     * better. Apparently conventional wisdom is that this is the case
     * for most applications of convex partitioning, and
     * Hertel-Mehlhorn is the generally sensible compromise algorithm.
     */
    void prepare(OutlineMerge &om, int index, bool make_convex) {

#ifdef MINKOWSKI_DEBUG
        clog << "prepare outline " << index << ", make_convex=" <<
            make_convex << endl;
#endif

        int *prev = new int[om.edges.size()];
        int *next = new int[om.edges.size()];
        enum Kind { BORING, EDGE_L, EDGE_R, DIAGONAL };
#ifdef MINKOWSKI_DEBUG
        static const char *const kindnames[] = {
            "BORING", "EDGE_L", "EDGE_R", "DIAGONAL"
        };
#endif
        Kind *kind = new Kind[om.edges.size()];

        /*
         * Classify every edge of the triangulation into BORING
         * (completely outside any polygon and we have no use for it
         * at all), EDGE_* (separates outside from inside, and we say
         * which side is inside) and DIAGONAL (in the interior of a
         * polygon).
         *
         * If !make_convex, we never class any edge as DIAGONAL, which
         * will be sufficient to make the later loop that processes
         * them not bother to do anything.
         */
        Kind diagonal = make_convex ? DIAGONAL : BORING;
        for (int ei = 0; ei < om.edges.size(); ++ei) {
            const auto &e = om.edges[ei];
            bool inl = inside.inside(om, e.til);
            bool inr = inside.inside(om, e.tir);
            kind[ei] = (inl ?
                        (inr ? diagonal : EDGE_L) :
                        (inr ? EDGE_R   : BORING));
#ifdef MINKOWSKI_DEBUG
            clog << "  edge " << ei << ": (" <<
                string(om.points[e.pi0].x) << "," <<
                string(om.points[e.pi0].y) << ")-(" <<
                string(om.points[e.pi1].x) << "," <<
                string(om.points[e.pi1].y) << "): " <<
                kindnames[kind[ei]] << endl;
#endif
        }

        /*
         * At each vertex, form the non-BORING edges into a cyclic
         * doubly linked list.
         */
        for (int pi = 0; pi < om.points.size(); ++pi) {
            const auto &p = om.points[pi];

            int first = -1, last = -1;

            for (int eii = 0; eii < p.ei_out.size(); ++eii) {
                int ei = p.ei_out[eii];
                if (kind[ei] == BORING)
                    continue;
                if (first != -1) {
                    next[last] = ei;
                    prev[ei] = last;
                } else {
                    first = ei;
                }
                last = ei;
            }

            if (first != -1) {
                next[last] = first;
                prev[first] = last;
            }
        }

        /*
         * Iterate over the DIAGONAL edges (this time just once per
         * actual edge, not once per direction), removing each one (by
         * reclassifying it as BORING and unlinking it from its lists)
         * if we can do so without creating a non-convexity at either
         * end.
         */
        for (int ei = 0; ei < om.edges.size(); ++ei) {
            const auto &e = om.edges[ei];
            if (kind[ei] != DIAGONAL)
                continue;
            if (e.reverse < ei)
                continue;

#ifdef MINKOWSKI_DEBUG
            clog << "  checking diagonal " << ei << " ... ";
#endif

            if (!reflex_angle(om, prev[ei], next[ei]) &&
                !reflex_angle(om, prev[e.reverse], next[e.reverse])) {
                int n, p;

#ifdef MINKOWSKI_DEBUG
                clog << "remove" << endl;
#endif
                kind[ei] = kind[e.reverse] = BORING;

                n = next[ei];
                p = prev[ei];
                next[p] = n;
                next[n] = p;

                n = next[e.reverse];
                p = prev[e.reverse];
                next[p] = n;
                next[n] = p;
            } else {
#ifdef MINKOWSKI_DEBUG
                clog << "keep" << endl;
#endif
            }
        }

        /*
         * Go round each vertex again, and put the interesting edges
         * into our actions list.
         */
        for (int pi = 0; pi < om.points.size(); ++pi) {
            const auto &p = om.points[pi];

            multimap<Number,int> changes;

#ifdef MINKOWSKI_DEBUG
            clog << "  actions around point " << pi << " = (" <<
                string(p.x) << "," << string(p.y) << ")" << endl;
#endif

            for (int eii = 0; eii < p.ei_out.size(); ++eii) {
                int ei = p.ei_out[eii];

                if (kind[ei] == EDGE_L || kind[ei] == DIAGONAL) {

                    Number angle = sortable_angle(edgevector(om, ei));
                    auto it = actions[index].find(angle);
                    if (it == actions[index].end()) {
                        actions[index][angle] = Actions();
                        it = actions[index].find(angle);
                    }
                    Actions &acts = it->second;

#ifdef MINKOWSKI_DEBUG
                    clog << "    adding actions for edge " << ei <<
                        ", angle " << string(angle) << endl;
#endif

                    acts.points_removed.push_back(pi);
                    acts.edges.push_back(ei);
#ifdef MINKOWSKI_DEBUG
                    clog << "      remove point " << pi << endl;
                    clog << "      do edge " << ei << endl;
#endif
                    changes.insert(pair<Number,int>(angle, -1));
                }
            }

            for (int eii = 0; eii < p.ei_in.size(); ++eii) {
                int ei = p.ei_in[eii];

                if (kind[ei] == EDGE_L || kind[ei] == DIAGONAL) {

                    Number angle = sortable_angle(edgevector(om, ei));
                    auto it = actions[index].find(angle);
                    if (it == actions[index].end()) {
                        actions[index][angle] = Actions();
                        it = actions[index].find(angle);
                    }
                    Actions &acts = it->second;

#ifdef MINKOWSKI_DEBUG
                    clog << "    adding actions for edge " << ei <<
                        ", angle " << string(angle) << endl;
#endif

                    acts.points_inserted.push_back(pi);
#ifdef MINKOWSKI_DEBUG
                    clog << "      insert point " << pi << endl;
#endif
                    changes.insert(pair<Number,int>(angle, +1));
                }
            }

            /*
             * Figure out whether the point should start off in our
             * multiset of currently active points, and if so, how
             * many times.
             */
            int curr_int = 0, min_int = 0;
            for (auto &kv: changes) {
                curr_int += kv.second;
                if (min_int > curr_int) {
                    points[index].insert(pi);
                    min_int--;
                    assert(min_int == curr_int);
                }
            }
#ifdef MINKOWSKI_DEBUG
            clog << "    point " << pi << " initial multiplicity " <<
                (-min_int) << endl;
#endif
        }

        delete[] prev;
        delete[] next;
        delete[] kind;
    }

    /*
     * Helper function for adding edges to the output OutlineMerge.
     */
    void add(OutlineMerge &out, Point p0, Point p1) {
#ifdef MINKOWSKI_DEBUG
        clog << "    output edge: (" <<
             string(p0.x) << "," << string(p0.y) << ")-(" <<
             string(p1.x) << "," << string(p1.y) << ")" << endl;
#endif
        out.add_edge(p0.x, p0.y, p1.x, p1.y, aux);
    }

  public:
    Minkowski(const InsideTest &inside_,
              const typename OutlineMerge::Aux &aux_) :
        inside(inside_), aux(aux_) {}

    /*
     * Main function that does the work.
     */
    void compute(OutlineMerge &out, OutlineMerge &a, OutlineMerge &b) {
        OutlineMerge *inputs[2];
        inputs[0] = &a;
        inputs[1] = &b;

        /*
         * We need to break one of the input outlines into convex
         * pieces in order for the following algorithm to work, but we
         * don't need to break both.
         *
         * You could imagine trying to make this judgment optimally,
         * by counting reflex angles. Each reflex angle in the outline
         * we don't break up will cause us to go an extra time all the
         * way round the outline we do, so you could figure out an
         * added-cost value of (# reflex angles in one outline) x
         * (total size of the other), and break up whichever outline
         * made that cost smaller.
         *
         * However, since we're going to end up with a quadratic time
         * algorithm in the end anyway, and also since I anticipate
         * many applications of this code having one outline very
         * small anyway, I think that's overkill, so I'll just choose
         * the smaller outline (in the sense of number of triangles)
         * to break up.
         */
        int unbroken = a.exterior > b.exterior ? 0 : 1;

        for (int i = 0; i < 2; i++) {
            prepare(*inputs[i], i, i != unbroken);
        }

        /*
         * Now we've got our action queues, iterate round both of them
         * in angle order, doing the main sum.
         */
        while (!actions[0].empty() || !actions[1].empty()) {
            typename map<Number, Actions>::iterator it[2];
            for (int i = 0; i < 2; i++) {
                it[i] = actions[i].begin();
            }

            if (it[0] != actions[0].end() && it[1] != actions[1].end()) {
                if (it[0]->first > it[1]->first)
                    it[0] = actions[0].end();
                else if (it[0]->first < it[1]->first)
                    it[1] = actions[1].end();
            }

#ifdef MINKOWSKI_DEBUG
            Number this_angle = it[0] == actions[0].end() ?
                it[1]->first : it[0]->first;
            clog << "angle " << string(this_angle) << ": 0=" <<
                (it[0] == actions[0].end() ? "none" : "some") << ", 1=" <<
                (it[1] == actions[1].end() ? "none" : "some") << endl;
            for (int i = 0; i < 2; i++) {
                clog << "  points[" << i << "] = {";
                for (int j: points[i])
                    clog << " " << j;
                clog << " }" << endl;
            }
#endif

            for (int i = 0; i < 2; i++) {
                if (it[i] == actions[i].end())
                    continue;

                list<int> &pts = it[i]->second.points_removed;
                for (int j: pts) {
#ifdef MINKOWSKI_DEBUG
                    clog << "  remove outline " << i << " point " << j << endl;
#endif
                    points[i].erase(points[i].find(j));
                }
            }
            for (int i = 0; i < 2; i++) {
                if (it[i] == actions[i].end())
                    continue;

                list<int> &edges = it[i]->second.edges;
                multiset<int> &pts = points[1-i];
                for (int ei: edges) {
                    auto &e = inputs[i]->edges[ei];
                    auto &p0 = inputs[i]->points[e.pi0];
                    auto &p1 = inputs[i]->points[e.pi1];

                    for (int pi: pts) {
                        auto &p = inputs[1-i]->points[pi];

#ifdef MINKOWSKI_DEBUG
                        clog << "  sum outline " << i << " edge " << ei <<
                            " + outline " << (1-i) << " point " << pi <<
                            endl;
#endif
                        add(out, Point(p0) + Point(p), Point(p1) + Point(p));
                    }
                }
            }
            if (it[0] != actions[0].end() && it[1] != actions[1].end()) {
                list<int> &edges0 = it[0]->second.edges;
                list<int> &edges1 = it[1]->second.edges;

                for (int ei0: edges0) {
                    auto &e0 = inputs[0]->edges[ei0];
                    auto &p00 = inputs[0]->points[e0.pi0];
                    auto &p01 = inputs[0]->points[e0.pi1];
                    for (int ei1: edges1) {
                        auto &e1 = inputs[1]->edges[ei1];
                        auto &p10 = inputs[1]->points[e1.pi0];
                        auto &p11 = inputs[1]->points[e1.pi1];

#ifdef MINKOWSKI_DEBUG
                        clog << "  sum outline 0 edge " << ei0 <<
                            " + outline 1 edge " << ei1 << endl;
#endif
                        add(out, Point(p00) + Point(p10),
                            Point(p01) + Point(p11));
                    }
                }
            }
            for (int i = 0; i < 2; i++) {
                if (it[i] == actions[i].end())
                    continue;

                list<int> &pts = it[i]->second.points_inserted;
                for (int j: pts) {
#ifdef MINKOWSKI_DEBUG
                    clog << "  insert outline " << i << " point " << j <<
                        endl;
#endif
                    points[i].insert(j);
                }
            }

            for (int i = 0; i < 2; i++) {
                if (it[i] != actions[i].end())
                    actions[i].erase(it[i]);
            }
        }
    }
};

#if 0
{ // mess with editors' indenting
{ // mess with editors' indenting
#endif
} // close namespace ZigguratInternal
using ZigguratInternal::Minkowski;
} // close namespace Ziggurat
