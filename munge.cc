/*
 * Program to munge outline files in assorted ways.
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <limits.h>
#include <assert.h>
#include <math.h>

#include <string>
#include <vector>

#include <err.h>

#include "merge.hh"
#include "numparse.hh"
#include "linefit.hh"
#include "munge.hh"

using std::string;
using std::vector;
using std::pair;

using Ziggurat::bigrat;
using Ziggurat::numparse;
using Ziggurat::linefit;
using namespace Ziggurat::Munge;

enum Mode { MODE_UNION, MODE_INTERSECTION, MODE_DIFFERENCE,
            MODE_INTERSECT_WITH_UNION, MODE_CONVEX_HULL };

struct Matrix {
    /*
     * A six-element matrix in the Postscript style, so that
     * transforming the point (x,y) via the matrix [a,b,c,d,e,f] gives
     * (ax+cy+e,bx+dy+f). So the identity matrix is [1,0,0,1,0,0].
     *
     * (Mathematically, the idea is that really we have a 3x3 matrix
     * which maps the plane z=1 to itself, and we transform the point
     * (x,y,1) to get another point with z=1. So the 'real' matrix
     * represented by those six numbers is
     *
     *   (a c e)
     *   (b d f)
     *   (0 0 1)
     *
     * but of course we needn't bother storing the last three since
     * they're always the same.)
     */
    bigrat a,b,c,d,e,f;

    Matrix() : a(1), b(0), c(0), d(1), e(0), f(0) {}
    Matrix(bigrat a, bigrat b, bigrat c, bigrat d, bigrat e, bigrat f)
        : a(a), b(b), c(c), d(d), e(e), f(f) {}
    Matrix(vector<bigrat> v) : a(v[0]), b(v[1]), c(v[2]),
                                    d(v[3]), e(v[4]), f(v[5]) {}

    Point operator*(const Point &p) const {
        return Point(a*p.x + c*p.y + e, b*p.x + d*p.y + f);
    }

    Matrix operator*(const Matrix &m) const {
        return Matrix(b*m.d + a*m.a, b*m.e + a*m.b, c + b*m.f + a*m.c,
                      e*m.d + d*m.a, e*m.e + d*m.b, f + e*m.f + d*m.c);
    }
};

class InsideTest {
    int n;
    Mode mode;
  public:
    InsideTest(int n_, Mode mode_) : n(n_), mode(mode_) {}

    bool inside(const OurOutlineMerge &om, int ti) const {
        if (mode == MODE_CONVEX_HULL) // only the exterior face is outside
            return ti != om.exterior;

        const Aux &aux = om.triangles[ti].aux;
        bool ret;

        switch (mode) {
          case MODE_UNION:
            ret = false;
            for (int i = 0; i < n; i++)
                if (aux[i] != 0)
                    ret = true;
            break;
          case MODE_INTERSECTION:
            ret = true;
            for (int i = 0; i < n; i++)
                if (aux[i] == 0)
                    ret = false;
            break;
          case MODE_DIFFERENCE:
            ret = aux[0] != 0;
            for (int i = 1; i < n; i++)
                if (aux[i] != 0)
                    ret = false;
            break;
          case MODE_INTERSECT_WITH_UNION:
            ret = false;
            if (aux[0] != 0) {
                for (int i = 1; i < n; i++)
                    if (aux[i] != 0)
                        ret = true;
            }
            break;
          default:
            // placate compiler warnings; MODE_CONVEX_HULL was handled above
            break;
        }

        return ret;
    }
};

class LoopReducer {
    double epsilon;
  public:
    LoopReducer(double epsilon_) : epsilon(epsilon_) {}
    bool reduce_loop(Loop &loop) const {
        if (!epsilon)
            return true;

        int n = loop.size();
        linefit::point *pts = new linefit::point[n + 1];
        for (int i = 0; i < n; i++) {
            pts[i].x = loop[i].x;
            pts[i].y = loop[i].y;
        }
        pts[n] = pts[0];
        int *indices = new int[n + 1];
        int nindices = linefit::optimal(pts, n + 1, epsilon, indices);
        Loop newloop;
        // Ignore final entry in indices[] because it's the same as entry
        // 0. FIXME: it would be nicer here if we could optimise without
        // bias towards a particular starting point.
        for (int i = 0; i < nindices-1; i++)
            newloop.push_back(loop[indices[i]]);
        loop = newloop;
        delete[] indices;
        delete[] pts;

        // Return failure if we ended up with only one point.
        return loop.size() > 1;
    }
};

int main(int argc, char **argv)
{
    string outfile = "-";
    vector<pair<string, Matrix> > files;
    Mode mode = MODE_UNION;
    Matrix matrix;
    double reduce_eps = 0;

    bool doing_opts = true;
    while (--argc > 0) {
        char *p = *++argv;
        if (doing_opts && p[0] == '-' && p[1]) {
            if (p[1] == '-') {
                if (!strcmp(p, "--")) {
                    doing_opts = false;
                } else if (!strcmp(p, "--help")) {
                    printf("usage:   munge <options> <outline> [[options] <outline>...]\n"
                           "options: -o OUTFILE      output outline file name\n"
                           "         -u              compute union of all input outlines (default)\n"
                           "         -i              compute intersection of all input outlines\n"
                           "         -d              compute set difference of first input file with rest\n"
                           "         -I              compute intersection of 1st file with union of rest\n"
                           "         -c              compute convex hull of a single outline\n"
                           "         -m XX,XY,YX,YY,CX,CY   transform subsequent outlines by this matrix\n"
                           "         -r THRESHOLD    reduce outline complexity subject to error threshold\n");
                    return 0;
                } else {
                    errx(1, "unrecognised option '%s'", p);
                }
            } else {
                char c;
                while (*p && (c = *++p) != '\0') {
                    switch (c) {
                      case 'u':
                        mode = MODE_UNION;
                        break;

                      case 'i':
                        mode = MODE_INTERSECTION;
                        break;

                      case 'd':
                        mode = MODE_DIFFERENCE;
                        break;

                      case 'I':
                        mode = MODE_INTERSECT_WITH_UNION;
                        break;

                      case 'c':
                        mode = MODE_CONVEX_HULL;
                        break;

                      case 'm':
                      case 'o':
                      case 'r':
                        /*
                         * Options requiring an argument.
                         */
                        char *arg = p+1;
                        if (*arg)
                            p += strlen(p); // eat remainder of argv word
                        else if (--argc > 0)
                            arg = *++argv;
                        else
                            errx(1, "expected an argument to option '-%c'", c);

                        switch (c) {
                          case 'm':
                            {
                                vector<bigrat> newmatrix;
                                for (char *p = arg; *p ;) {
                                    char c;
                                    char *q = p;
                                    bigrat x;

                                    while (*p && *p != ',') p++;
                                    c = *p;
                                    *p = '\0';

                                    if (!numparse(q, &x))
                                        errx(1, "unable to parse '%s' as a matrix element\n", arg);
                                    newmatrix.push_back(x);

                                    if (c) p++;
                                }
                                if (newmatrix.size() != 6)
                                    errx(1, "unable to parse '%s' as a six-element matrix",
                                         arg);
                                matrix = Matrix(newmatrix);
                            }
                            break;
                          case 'o':
                            outfile = arg;
                            break;
                          case 'r':
                            reduce_eps = atof(arg);
                            break;
                        }
                        break;
                    }
                }
            }
        } else {
            files.push_back(pair<string, Matrix>(p, matrix));
        }
    }

    if (files.size() == 0)
        files.push_back(pair<string, Matrix>("-", matrix));

    if (files.size() != 1 && mode == MODE_CONVEX_HULL)
        errx(1, "expected only one input in convex hull mode");

    vector<Outline> outlines;

    for (int i = 0; i < files.size(); i++) {
        outlines.push_back(Outline());
        Outline &outline = outlines[outlines.size()-1];

        Matrix mx = files[i].second;
        LoopReducer reducer(reduce_eps);

        read_outline(outline, files[i].first, mx, reducer);
    }

    int n = outlines.size();

    OurOutlineMerge om;

    for (int i = 0; outlines.size() > i; i++) { // arrgh, emacs
        add_to_om(om, outlines[i], i);
    }

    om.compute();

    InsideTest inside(n, mode);
    Outline outline;
    om_result(outline, om, inside);

    write_outline(outfile, outline);

    return 0;
}
