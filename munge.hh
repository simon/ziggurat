/*
 * Common components shared between command-line tools that load and
 * save outline files and do set theory on them (munge, voronoi).
 */

#include "merge.hh"

namespace Ziggurat {
namespace Munge {
#if 0
} // mess with editors' indenting
} // mess with editors' indenting
#endif

using std::string;
using std::vector;
using std::map;

struct Aux {
    vector<int> v;
    Aux() {}
    Aux(int index, int val) {
        while (index-- > 0) v.push_back(0);
        v.push_back(val);
    }
    Aux add(const Aux &x, int mult) const {
        int i;
        Aux ret;
        for (i = 0; i < v.size() || i < x.v.size(); i++) {
            int val = 0;
            if (i < v.size()) val += v[i];
            if (i < x.v.size()) val += mult * x.v[i];
            ret.v.push_back(val);
        }
        return ret;
    }
    bool zero() const {
        bool ret = true;
        int i;
        for (i = 0; i < v.size(); i++)
            if (v[i])
                ret = false;
        return ret;
    }
    Aux operator+(const Aux &x) const { return add(x, +1); }
    Aux operator-(const Aux &x) const { return add(x, -1); }
    bool operator==(const Aux &x) const { return add(x, -1).zero(); }
    Aux operator-() const { return Aux().add(*this, -1); }
    int operator[](int i) const { return i >= v.size() ? 0 : v[i]; }
};

typedef OutlineMerge<bigrat, Aux> OurOutlineMerge;

struct Point {
    bigrat x, y;

    Point() : x(0), y(0) {}
    Point(const Point &p) : x(p.x), y(p.y) {}
    Point(const OurOutlineMerge::Point &p) : x(p.x), y(p.y) {}
    Point(bigrat x, bigrat y) : x(x), y(y) {}
    Point &operator+=(const Point &p) { x += p.x; y += p.y; return *this; }
    Point &operator-=(const Point &p) { x -= p.x; y -= p.y; return *this; }
    Point operator+(const Point &p) const {
        Point ret(*this); ret += p; return ret;
    }
    Point operator-(const Point &p) const {
        Point ret(*this); ret -= p; return ret;
    }
};

typedef vector<Point> Loop;
typedef vector<Loop> Outline;

string fgetstring(FILE *fp)
{
    string ret;
    char buf[1024];
    while (fgets(buf, sizeof(buf), fp)) {
	ret += buf;
	if (ret[ret.length()-1] == '\n')
	    break;		       /* got a newline, we're done */
    }
    return ret;
}

// I don't normally enjoy implicit reference parameters but I can't
// resist the temptation to make this look syntactically like the Perl
// version
void chomp(string &s)
{
    size_t len = s.length();
    if (len > 0 && s[len-1] == '\n') {
        len--;
        if (len > 0 && s[len-1] == '\r')
            len--;
    }
    s.resize(len);
}

template<class Transform, class Reducer>
void read_outline(Outline &outline, string filename,
                  const Transform &mx, const Reducer &reducer)
{
    const char *fname = filename.c_str();
    FILE *fp = filename == "-" ? stdin : fopen(fname, "r");
    if (!fp)
        err(1, "'%s': open", fname);

    string line;
    int lineno = 0;
    while ((lineno++, line = fgetstring(fp)) != "") {
        chomp(line);
        int npoints;
        if (sscanf(line.c_str(), "loop %d", &npoints) != 1)
            errx(1, "%s:%d: expected a 'loop' line", fname, lineno);

        outline.push_back(Loop());
        Loop &loop = outline[outline.size()-1];

        while (npoints-- > 0) {
            if ((lineno++, line = fgetstring(fp)) == "")
                errx(1, "%s:%d: expected a 'point' line", fname, lineno);
            chomp(line);
            if (line.substr(0, 6) != "point ")
                errx(1, "%s:%d: expected a 'point' line", fname, lineno);
            size_t nextspace = line.find(' ', 6);
            if (nextspace == string::npos)
                errx(1, "%s:%d: unable to parse point specification '%s'",
                     fname, lineno, line.c_str());
            string xs = line.substr(6, nextspace-6);
            string ys = line.substr(nextspace+1);
            bigrat x, y;
            if (!numparse(xs.c_str(), &x) || !numparse(ys.c_str(), &y))
                errx(1, "%s:%d: unable to parse point specification '%s'",
                     fname, lineno, line.c_str());
            loop.push_back(mx * Point(x, y));
        }

        if ((lineno++, line = fgetstring(fp)) == "")
            errx(1, "%s:%d: expected an 'endloop' line", fname, lineno);
        chomp(line);
        if (line != "endloop")
            errx(1, "%s:%d: expected an 'endloop' line", fname, lineno);

        if (!reducer.reduce_loop(loop))
            outline.pop_back();
    }

    if (fp != stdin)
        fclose(fp);
}

void add_to_om(OurOutlineMerge &om, const Outline &outline, int index)
{
    for (int j = 0; outline.size() > j; j++) {
        const Loop &loop = outline[j];
        const Point *prev = &loop[loop.size() - 1];
        for (int k = 0; loop.size() > k; k++) {
            om.add_edge(prev->x, prev->y,
                        loop[k].x, loop[k].y, Aux(index, 1));
            prev = &loop[k];
        }
    }
}

template<class InsideTest>
void om_result(Outline &outline, OurOutlineMerge &om, const InsideTest &inside)
{
    /*
     * Decide which edges will be part of the output outline.
     */
    vector<bool> edges;
    for (int i = 0; om.edges.size() > i; i++) {
        OurOutlineMerge::Edge &e = om.edges[i];

        if (!inside.inside(om, e.til) && inside.inside(om, e.tir)) {
            edges.push_back(true);
        } else {
            edges.push_back(false);
        }
    }

    /*
     * Work out which edge follows each edge in that vector.
     */
    map<int, int> nextedge;
    for (int i = 0; i < om.edges.size(); i++) {
        if (!edges[i])
            continue;
        OurOutlineMerge::Point &p = om.points[om.edges[i].pi1];
        int n = p.ei_in.size();
        int next = -1;
        for (int j = 0; j < n; j++) {
            if (p.ei_in[j] == i) {
                for (int k = 1; k <= n; k++) {
                    int kk = (j + k) % n;
                    int ei = p.ei_out[kk];
                    if (edges[ei]) {
                        next = ei;
                        break;
                    }
                }
                break;
            }
        }
        assert(next >= 0);
        nextedge[i] = next;
    }

    /*
     * And now we can construct our output outline by pulling a fresh
     * set of cycles out of that map.
     */
    map<int, int>::iterator nei;
    while ((nei = nextedge.begin()) != nextedge.end()) {
        outline.push_back(Loop());
        Loop &loop = outline[outline.size()-1];

        int start = nei->first;

        do {
            int p = om.edges[nei->first].pi0;
            int q = om.edges[nei->first].pi1;
            int r = om.edges[nei->second].pi1;

            /*
             * Output point q, unless the lines before and after
             * it are collinear.
             */
            OurOutlineMerge::Point &P = om.points[p];
            OurOutlineMerge::Point &Q = om.points[q];
            OurOutlineMerge::Point &R = om.points[r];
            bigrat pqx = Q.x - P.x, pqy = Q.y - P.y;
            bigrat qrx = R.x - Q.x, qry = R.y - Q.y;
            if (pqx * qry != pqy * qrx) {
                loop.push_back(Point(Q.x, Q.y));
            }

            nei = nextedge.find(nei->second);
        } while (nei->first != start);

        /*
         * Now go round again and erase this cycle, so that we
         * eventually empty the nextpoint map and stop.
         */
        do {
            int p = nei->second;
            nextedge.erase(nei);
            nei = nextedge.find(p);
        } while (nei != nextedge.end());
    }
}

void write_outline(string outfile, const Outline &outline)
{
    FILE *outfp;

    if (outfile == "-")
        outfp = stdout;
    else if ((outfp = fopen(outfile.c_str(), "w")) == NULL)
        err(1, "%s: open", outfile.c_str());

    for (int j = 0; outline.size() > j; j++) {
        const Loop &loop = outline[j];
        fprintf(outfp, "loop %d\n", (int)loop.size());
        for (int k = 0; loop.size() > k; k++) {
            char *xstr = loop[k].x.stringify();
            char *ystr = loop[k].y.stringify();
            fprintf(outfp, "point %s %s\n", xstr, ystr);
            delete[] xstr;
            delete[] ystr;
        }
        fprintf(outfp, "endloop\n");
    }

    if (outfp != stdout)
        fclose(outfp);
}

#if 0
{ // mess with editors' indenting
{ // mess with editors' indenting
#endif
} // close namespace Ziggurat::Munge
} // close namespace Ziggurat
