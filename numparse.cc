/*
 * numparse.cc: Function to parse a string into a rational number,
 * supporting as many syntaxes as I think sensible.
 */

#include <string.h>
#include <ctype.h>
#include "numparse.hh"

namespace Ziggurat {
namespace ZigguratInternal {
#if 0
} // mess with editors' indenting
} // mess with editors' indenting
#endif

/*
 * Returns false if it couldn't parse the input.
 */
bool numparse(const char *s, bigrat *ret)
{
    const char *p = s;
    int sign = +1;

    if (*p == '-') {
        p++;
        sign = -1;
    } else if (*p == '+') {
        p++;
    }

    if (strchr(p, '.') ||
        strchr(p, 'p') ||
        strchr(p, 'P') ||
        ((strchr(p, 'e') || strchr(p, 'E')) &&
         !(strchr(p, 'x') || strchr(p, 'X')))) {

        if (!((*p && isdigit((unsigned char)*p)) || *p == '.'))
            return false;

        int base, expbase;
        char expmarker;
        bool seendot = false;
        bigrat n = 0, d = 1;

        if (*p == '0' && tolower((unsigned char)p[1]) == 'x') {
            /*
             * Hex float literal.
             */
            p += 2;
            base = 16;
            expbase = 2;
            expmarker = 'p';
        } else {
            /*
             * Decimal float literal.
             */
            base = expbase = 10;
            expmarker = 'e';
        }

        while (*p == '.' ||
               (*p && (base == 16 ? isxdigit((unsigned char)*p) :
                       isdigit((unsigned char)*p)))) {
            if (*p == '.') {
                if (!seendot) {
                    seendot = true;
                    p++;
                    continue;
                } else {
                    return false;      // two dots in numeric literal
                }
            } else {
                int val = (*p >= '0' && *p <= '9' ? *p - '0' :
                           *p >= 'A' && *p <= 'F' ? *p - ('A'-10) :
                           *p >= 'a' && *p <= 'f' ? *p - ('a'-10) : 0);
                n *= base;
                n += val;
                if (seendot)
                    d *= base;
                p++;
            }
        }

        n /= d;

        if (*p && tolower((unsigned char)*p) == expmarker) {
            int exponent = 0, expsign = +1;
            p++;
            if (*p == '-' || *p == '+') {
                expsign = (*p == '-') ? -1 : +1;
                p++;
            }
            while (*p && isdigit((unsigned char)*p)) {
                exponent = 10 * exponent + (*p - '0');
                p++;
            }

            n *= bigrat_power(expbase, exponent * expsign);
        }

        *ret = n * sign;
        return true;
    } else {
        mpq_t x;
        mpq_init(x);
        int mpqret = mpq_set_str(x, p, 0);
        if (mpqret == 0) {
            if (sign < 0)
                mpq_neg(x, x);
            *ret = bigrat(x);
            mpq_clear(x);
            return true;
        } else {
            mpq_clear(x);
            return false;
        }
    }
}

#if 0
{ // mess with editors' indenting
{ // mess with editors' indenting
#endif
} // close namespace ZigguratInternal
} // close namespace Ziggurat

#ifdef NUMPARSE_TEST

/*
g++ -std=c++11 -g -O0 -DNUMPARSE_TEST -o numparse numparse.cc -lgmp && ./numparse
*/

#include <stdio.h>
#include <stdlib.h>

using Ziggurat::bigrat;
using Ziggurat::numparse;

int main(void)
{
#define TEST(instr) do                          \
    {                                           \
        bigrat n;                               \
        bool ret = numparse(instr, &n);         \
        printf("'%s': ", instr);                \
        if (ret) {                              \
            char *strval = n.stringify();       \
            printf("ok %s\n", strval);          \
            delete[] strval;                    \
        } else                                  \
            printf("invalid\n");                \
    } while (0)

    TEST("0");
    TEST("0.1");
    TEST("0.125");
    TEST("0.999");
    TEST("1");
    TEST("-1");
    TEST("-1e2");
    TEST("-0x1p2");
    TEST("-0x1.2p2");
    TEST("-0x1.2p-2");
    TEST("-0x1.2");
    TEST("1e-5");
    TEST("0b1011");
    TEST("0B1011");
    TEST("0x1A");
    TEST("0X1a");
    TEST("012");
}

#endif
