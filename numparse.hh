/*
 * numparse.hh: header for numparse.cc.
 */

#ifndef NUMPARSE_HH
#define NUMPARSE_HH

#include "bigrat.hh"

namespace Ziggurat {
namespace ZigguratInternal {
#if 0
} // mess with editors' indenting
} // mess with editors' indenting
#endif

bool numparse(const char *s, bigrat *ret);

#if 0
{ // mess with editors' indenting
{ // mess with editors' indenting
#endif
} // close namespace ZigguratInternal
using ZigguratInternal::numparse;
} // close namespace Ziggurat

#endif /* NUMPARSE_HH */
