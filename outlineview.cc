/*
 * Program to translate one or more outline files into a Postscript
 * file for convenient viewing.
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <limits.h>
#include <assert.h>
#include <math.h>

#include <string>
#include <vector>

#include <err.h>

#include "numparse.hh"

using std::string;
using std::vector;

using Ziggurat::bigrat;

struct Point {
    bigrat x, y;

    Point() : x(0), y(0) {}
    Point(bigrat x, bigrat y) : x(x), y(y) {}
};

typedef vector<Point> Loop;
typedef vector<Loop> Outline;

string fgetstring(FILE *fp)
{
    string ret;
    char buf[1024];
    while (fgets(buf, sizeof(buf), fp)) {
	ret += buf;
	if (ret[ret.length()-1] == '\n')
	    break;		       /* got a newline, we're done */
    }
    return ret;
}

// I don't normally enjoy implicit reference parameters but I can't
// resist the temptation to make this look syntactically like the Perl
// version
void chomp(string &s)
{
    size_t len = s.length();
    if (len > 0 && s[len-1] == '\n') {
        len--;
        if (len > 0 && s[len-1] == '\r')
            len--;
    }
    s.resize(len);
}

int main(int argc, char **argv)
{
    char *outfile = NULL;
    bigrat scale = 1;
    vector<string> files;
    bool filled = true, fill_only = false;

    bool doing_opts = true;
    while (--argc > 0) {
        char *p = *++argv;
        if (doing_opts && p[0] == '-' && p[1]) {
            if (p[1] == '-') {
                if (!strcmp(p, "--")) {
                    doing_opts = false;
                } else if (!strcmp(p, "--help")) {
                    printf("usage:   outlineview <options> <outline> [<outline>...]\n"
                           "options: -o OUTFILE      output outline file name\n"
                           "         -u              only stroke outlines, do not fill (gives easier PS)\n"
                           "         -f              only fill outlines in black, do not stroke\n"
                           "         -s SCALE        scale output by the specified factor\n");
                    return 0;
                } else {
                    errx(1, "unrecognised option '%s'", p);
                }
            } else {
                char c;
                while (*p && (c = *++p) != '\0') {
                    switch (c) {
                      case 'u':        // unfilled
                        filled = false;
                        break;
                      case 'f':        // filled only
                        fill_only = true;
                        break;
                      case 'o':
                      case 's':
                        /*
                         * Options requiring an argument.
                         */
                        char *arg = p+1;
                        if (*arg)
                            p += strlen(p); // eat remainder of argv word
                        else if (--argc > 0)
                            arg = *++argv;
                        else
                            errx(1, "expected an argument to option '-%c'", c);

                        switch (c) {
                          case 'o':
                            outfile = arg;
                            break;
                          case 's':
                            if (!numparse(arg, &scale))
                                errx(1, "unable to parse scale '%s'", arg);
                            break;
                        }
                        break;
                    }
                }
            }
        } else {
            files.push_back(p);
        }
    }

    if (files.size() == 0)
        files.push_back("-");

    vector<Outline> outlines;

    for (int i = 0; i < files.size(); i++) {
        const char *fname = files[i].c_str();
        FILE *fp = files[i] == "-" ? stdin : fopen(fname, "r");
        if (!fp)
            err(1, "'%s': open", fname);

        outlines.push_back(Outline());
        Outline &outline = outlines[outlines.size()-1];

        string line;
        int lineno = 0;
        while ((lineno++, line = fgetstring(fp)) != "") {
            chomp(line);
            int npoints;
            if (sscanf(line.c_str(), "loop %d", &npoints) != 1)
                errx(1, "%s:%d: expected a 'loop' line", fname, lineno);

            outline.push_back(Loop());
            Loop &loop = outline[outline.size()-1];

            while (npoints-- > 0) {
                if ((lineno++, line = fgetstring(fp)) == "")
                    errx(1, "%s:%d: expected a 'point' line", fname, lineno);
                chomp(line);
                if (line.substr(0, 6) != "point ")
                    errx(1, "%s:%d: expected a 'point' line", fname, lineno);
                size_t nextspace = line.find(' ', 6);
                if (nextspace == string::npos)
                    errx(1, "%s:%d: unable to parse point specification '%s'",
                         fname, lineno, line.c_str());
                string xs = line.substr(6, nextspace-6);
                string ys = line.substr(nextspace+1);
                bigrat x, y;
                if (!numparse(xs.c_str(), &x) || !numparse(ys.c_str(), &y))
                    errx(1, "%s:%d: unable to parse point specification '%s'",
                         fname, lineno, line.c_str());
                loop.push_back(Point(x, y));
            }

            if ((lineno++, line = fgetstring(fp)) == "")
                errx(1, "%s:%d: expected an 'endloop' line", fname, lineno);
            chomp(line);
            if (line != "endloop")
                errx(1, "%s:%d: expected an 'endloop' line", fname, lineno);
        }

        if (fp != stdin)
            fclose(fp);
    }

    int n = outlines.size();

    bool seen_point = false;
    bigrat xmin, ymin, xmax, ymax;

    for (int i = 0; outlines.size() > i; i++) { // arrgh, emacs
        const Outline &outline = outlines[i];
        for (int j = 0; outline.size() > j; j++) {
            const Loop &loop = outline[j];
            for (int k = 0; loop.size() > k; k++) {
                const Point &p = loop[k];
                if (!seen_point) {
                    xmin = xmax = p.x;
                    ymin = ymax = p.y;
                    seen_point = true;
                } else {
                    if (xmin > p.x) xmin = p.x;
                    if (xmax < p.x) xmax = p.x;
                    if (ymin > p.y) ymin = p.y;
                    if (ymax < p.y) ymax = p.y;
                }
            }
        }
    }

    FILE *outfp;

    if (!outfile)
        outfp = stdout;
    else if ((outfp = fopen(outfile, "w")) == NULL)
        err(1, "%s: open", outfile);

    fprintf(outfp, "%%!PS-Adobe-1.0\n"
            "%%%%Pages: %d\n"
            "%%%%BoundingBox: %d %d %d %d\n"
            "%%%%EndComments\n"
            "%%%%BeginProlog\n"
            "%%%%EndProlog\n"
            "%%%%BeginSetup\n"
            "%%%%EndSetup\n",
            (int)outlines.size(),
            (scale * xmin).floor(), (scale * ymin).floor(),
            (scale * xmax).ceil(), (scale * ymax).ceil());

    for (int i = 0; outlines.size() > i; i++) { // arrgh, emacs
        const Outline &outline = outlines[i];
        fprintf(outfp, "%%%%Page: %d %d\n", i+1, i+1);
        fprintf(outfp, "0.1 setlinewidth 1 setlinejoin\n");

        if (filled) {
            /*
             * In filled mode, we must do everything as a single path,
             * otherwise we'll mishandle paths that cut holes inside
             * the region enclosed by another path.
             */
            fprintf(outfp, "newpath\n");
        }

        for (int j = 0; outline.size() > j; j++) {
            const Loop &loop = outline[j];

            if (filled) {
                const char *cmd = "moveto";
                for (int k = 0; loop.size() > k; k++) {
                    const Point &p = loop[k];
                    fprintf(outfp, "%g %g %s\n",
                            (double)(scale * p.x), (double)(scale * p.y), cmd);
                    cmd = "lineto";
                }
                fprintf(outfp, "closepath\n");
            } else {
                for (int k = 0; loop.size() > k; k++) {
                    const Point &p = loop[k];
                    const Point &q = loop[(k+1) % loop.size()];
                    fprintf(outfp, "newpath %g %g moveto %g %g lineto "
                            "stroke\n",
                            (double)(scale * p.x), (double)(scale * p.y),
                            (double)(scale * q.x), (double)(scale * q.y));
                }
            }
        }

        if (fill_only) {
            fprintf(outfp, "0 setgray fill\n");
        } else if (filled) {
            /*
             * In filled mode, fill and stroke our one single path.
             */
            fprintf(outfp, "gsave 0.8 setgray fill grestore stroke\n");
        }

        fprintf(outfp, "showpage\n");
    }
    fprintf(outfp, "%%%%Trailer\n%%%%EOF\n");

    if (outfp != stdout)
        fclose(outfp);

    return 0;
}
