/*
 * planarise.hh: a C++ implementation of an algorithm to make a
 * non-planar graph embedded in the plane into a planar one, by adding
 * new vertices and splitting up edges where necessary.
 *
 * Strategy is basically the Bentley–Ottmann algorithm.
 */

#ifndef PLANARISE_HH
#define PLANARISE_HH

#include <set>
#include <map>
#include <vector>
#include <algorithm>
#include <assert.h>
#ifdef PLANARISE_DIAGNOSTICS
#include <stdio.h>
#endif

#include "crosspoint.hh"

namespace Ziggurat {
namespace ZigguratInternal {
#if 0
} // mess with editors' indenting
} // mess with editors' indenting
#endif

using std::vector;
using std::set;
using std::map;
using std::min;
using std::max;

/*
 * The Planarisation class is parametrised by a type Graph, and
 * expects that type to present the following API:

class Graph {
    class Number {
        // Number must be an ordered arithmetic type: it must support
        // the + - * / operators and comparisons, and also permit
        // initialisation to, assignment from, and comparison with
        // integers. Equality tests will be expected to be exactly
        // accurate, in that two arithmetic expressions whose ideal
        // mathematical results ought to be equal will be expected to
        // actually compare equal.
    };

    class Vertex {
        Number x() const;
        Number y() const;
    };

    // Both these functions are given an empty std::vector and
    // expected to populate it with (respectively) the full list of
    // vertices of the graph and the list of vertices connected to v
    // by an edge. It doesn't matter whether enum_neighbours returns v
    // as a neighbour of itself (either consistently or
    // intermittently); the self-edge will be ignored if it does.
    void enum_vertices(std::vector<const Vertex *> &out) const;
    void enum_neighbours(const Vertex *v, std::vector<const Vertex *> &out) const;

    // This function is required to return auxiliary information about
    // an edge in the input graph. The auxiliary information is of a
    // type described as Aux, and is intended to apply to a particular
    // _orientation_ of an edge.
    //
    // If none of this is even needed, the simplest thing is to do this:
    //     class Aux {
    //       public:
    //         Aux operator+(const Aux &) const { return Aux(); }
    //         Aux operator-() const { return Aux(); }
    //         Aux() {}
    //     };
    class Aux {
        Aux operator+(const Aux &) const; // aggregate two Auxes
        Aux operator-() const; // give the Aux for the same edge in reverse
        Aux(); // construct an identity value
        Aux(const Aux &); // copy constructor
    };
    Aux get_aux(const Vertex *start, const Vertex *end) const;
};

 * You perform a planarisation of a graph by means of instantiating a
 * Planarisation class and passing the graph as a constructor
 * parameter; after that, you can query the resulting object to get
 * its collection of edges and faces.
 */
template<class Graph>
class Planarisation {
  public:
    typedef typename Graph::Number Number;
  private:
    typedef typename Graph::Vertex OrigVertex;
    typedef typename Graph::Aux Aux;

  public:
    struct Point {
        Number xc, yc;
        Number x() const { return xc; }
        Number y() const { return yc; }
        Point(Number ax, Number ay) : xc(ax), yc(ay) {}
        Point(const OrigVertex *v) : xc(v->x()), yc(v->y()) {}
        Point(const Point &p) : xc(p.xc), yc(p.yc) {}
        Point() : xc(0), yc(0) {}
        Point &operator=(const Point &p) { xc = p.xc; yc = p.yc; return *this; }
        Point operator-(const Point &a) const {
            return Point(xc-a.xc, yc-a.yc);
        }
        Point operator+(const Point &a) const {
            return Point(xc+a.xc, yc+a.yc);
        }
        Number dot(const Point &a) const {
            return xc * a.xc + yc * a.yc;
        }
        Point rot90() const {
            return Point(-yc, xc);
        }
        bool operator<(const Point &b) const {
            if (yc != b.yc)
                return yc < b.yc;
            else
                return xc < b.xc;
        }
        bool operator==(const Point &b) const {
            return xc == b.xc && yc == b.yc;
        }
        bool operator!=(const Point &b) const { return !(*this == b); }
        int cyclic_class(Number *n, Number *d) {
            if (yc > 0) {
                *n = xc;
                *d = yc;
                return 1;
            } else if (yc < 0) {
                *n = -xc;
                *d = -yc;
                return 3;
            } else {
                *n = *d = 1;
                assert(xc != 0);
                if (xc < 0)
                    return 0;
                else
                    return 2;
            }
        }
    };
  private:

    static bool vertexcmp(const OrigVertex *a, const OrigVertex *b) {
        return Point(a) < Point(b);
    }

    struct Edge {
        Point a, b;
        Edge(const Point &aa, const Point &bb) : a(aa), b(bb) {}

        inline Number dx() const { return b.x() - a.x(); }
        inline Number dy() const { return b.y() - a.y(); }

        inline Number x_at_sweep_pos(Point sweep) const {
            if (dy() == 0) {
                // Special case: a horizontal line.
                return sweep.x();
            }
            return a.x() + (sweep.y() - a.y()) * dx() / dy();
        }

        bool operator<(const Edge &that) const {
            if (a == that.a)
                return b < that.b;
            else
                return a < that.a;
        }
        bool operator==(const Edge &that) const {
            return a == that.a && b == that.b;
        }
    };

    typedef map<Edge, Aux> ActiveEdgeBundle;

    struct BundleComparer {
        Point *sweep;

        BundleComparer(Point *asweep) : sweep(asweep) {}

        bool operator()(const ActiveEdgeBundle &A, const ActiveEdgeBundle &B) {
            const Edge &a = A.rbegin()->first, &b = B.rbegin()->first;
#ifdef MORE_PLANARISE_DIAGNOSTICS
            fprintf(stderr, "        edge compare: (%g,%g) - (%g,%g) vs (%g,%g) - (%g,%g); sweep=(%g,%g)\n",
                    (double)a.a.x(), (double)a.a.y(), (double)a.b.x(), (double)a.b.y(),
                    (double)b.a.x(), (double)b.a.y(), (double)b.b.x(), (double)b.b.y(),
                    (double)sweep->x(), (double)sweep->y());
#endif
            // Work out the x-coordinate at which each of the two
            // lines intersects our current y position, and compare
            // those.
            Number ax = a.x_at_sweep_pos(*sweep);
            Number bx = b.x_at_sweep_pos(*sweep);
#ifdef MORE_PLANARISE_DIAGNOSTICS
            fprintf(stderr, "          x at sweep pos: %g vs %g\n",
                    (double)ax, (double)bx);
#endif
            if (ax != bx)
                return ax < bx;
            // Tiebreak using the gradients of the lines, if they meet
            // at this point. Horizontal lines sort rightmost; edges
            // with both ends equal sort leftmost, because those are a
            // special edge type used only as a search key.
            bool adyzero = a.dy()==0;
            bool bdyzero = b.dy()==0;
            if (adyzero || bdyzero) {
                int atype = (!adyzero ? 0 :
                             a.a != a.b ? 1 : -1);
                int btype = (!bdyzero ? 0 :
                             b.a != b.b ? 1 : -1);
#ifdef MORE_PLANARISE_DIAGNOSTICS
                fprintf(stderr, "          tiebreak horizontals: %+d vs %+d\n",
                        atype, btype);
#endif
                return atype < btype;
            }
            Number aslope = a.dx() / a.dy();
            Number bslope = b.dx() / b.dy();
#ifdef MORE_PLANARISE_DIAGNOSTICS
            fprintf(stderr, "          tiebreak slope: %g vs %g\n",
                    (double)aslope, (double)bslope);
#endif
            return aslope < bslope;
        }
    };

    typedef map<Edge, Aux> edgemap;
    edgemap our_edges;

  public:
    typedef set<Point> pointset;
    pointset our_points;
  private:
    typedef typename pointset::iterator pointiterator;

    void connect(const Point &a, const Point &b, Aux aux) {
#ifdef PLANARISE_DIAGNOSTICS
        fprintf(stderr, "-- connect: (%g,%g) - (%g,%g) aux=%d\n",
                (double)a.x(), (double)a.y(), (double)b.x(), (double)b.y(), aux.i);
#endif
        our_edges[Edge(a,b)] = our_edges[Edge(a,b)] + aux;
        our_edges[Edge(b,a)] = our_edges[Edge(b,a)] + (-aux);
    }

    void collision_check(const Edge &a, const Edge &b) {
        Number xout, yout;
#ifdef PLANARISE_DIAGNOSTICS
        fprintf(stderr, "-- collision check: (%g,%g) - (%g,%g) vs (%g,%g) - (%g,%g)\n",
                (double)a.a.x(), (double)a.a.y(), (double)a.b.x(), (double)a.b.y(),
                (double)b.a.x(), (double)b.a.y(), (double)b.b.x(), (double)b.b.y());
#endif
        bool ret = crosspoint(a.a.x(), a.a.y(), a.b.x(), a.b.y(),
                              b.a.x(), b.a.y(), b.b.x(), b.b.y(),
                              &xout, &yout);
        if (ret) {
            /*
             * The returned crossing point might not be within the
             * lines. Check whether it is.
             */
            if (min(a.a.x(), a.b.x()) <= xout &&
                xout <= max(a.a.x(), a.b.x()) &&
                min(a.a.y(), a.b.y()) <= yout &&
                yout <= max(a.a.y(), a.b.y()) &&
                min(b.a.x(), b.b.x()) <= xout &&
                xout <= max(b.a.x(), b.b.x()) &&
                min(b.a.y(), b.b.y()) <= yout &&
                yout <= max(b.a.y(), b.b.y())) {
                /*
                 * These line segments really do intersect. Add the
                 * new point to our vertex list.
                 */
#ifdef PLANARISE_DIAGNOSTICS
                fprintf(stderr, "   collision found: (%g,%g)\n",
                        (double)xout, (double)yout);
#endif
                our_points.insert(Point(xout, yout));
            }
        }
    }

  public:
    Planarisation(const Graph &G) {
        /*
         * Initialise our output vertex set with every input vertex.
         * Also, set up a map from vertices' coordinates to lists of
         * actual 'OrigVertex *' pointers at those locations.
         */
        vector<const OrigVertex *> vertices;
        map<Point, vector<const OrigVertex *> > pointmap;
        G.enum_vertices(vertices);
        for (const OrigVertex *v: vertices) {
            our_points.insert(v);
            auto mi = pointmap.find(Point(v));
            if (mi == pointmap.end()) {
                vector<const OrigVertex *> newentry;
                newentry.push_back(v);
                pointmap[Point(v)] = newentry;
            } else {
                mi->second.push_back(v);
            }
        }

        /*
         * We're going to maintain a set of currently active edges,
         * sorted by the point where they cross our sweep line.
         *
         * Well, sort of. In fact what we're going to maintain is a
         * set of edge 'bundles', each of which consists of a
         * collection of collinear edges each with an associated Aux
         * value. Within the bundle we sort by the normal static
         * comparison criterion, which means the longest edge in the
         * bundle always sorts last.
         */
        Point sweep(0, 0);
        BundleComparer active_comparer(&sweep);
        typedef set<ActiveEdgeBundle, BundleComparer> ActiveEdgeSet;
        ActiveEdgeSet active_edges(active_comparer);
        typedef typename ActiveEdgeSet::iterator ActiveEdgeIterator;
        typedef typename ActiveEdgeBundle::const_iterator BundleConstIterator;

#ifdef PLANARISE_DIAGNOSTICS
        fprintf(stderr, "-- START\n");
#endif

        /*
         * Iterate through our output vertex set in order, adding new
         * vertices as we discover new projected collision points.
         */
        for (const Point &p: our_points) {
#ifdef PLANARISE_DIAGNOSTICS
            fprintf(stderr, "-- processing point: (%g,%g)\n",
                        (double)p.x(), (double)p.y());
#endif

#ifdef MORE_PLANARISE_DIAGNOSTICS
            fprintf(stderr, "   active bundle list is:\n");
            Number sorting_criterion;
            bool started = false, bad = false;
            for (auto &e: active_edges) {
                fprintf(stderr, "     ----\n");
                for (auto &b: e) {
                    fprintf(stderr, "       (%g,%g) - (%g,%g) aux=%d\n",
                            (double)b.first.a.x(), (double)b.first.a.y(),
                            (double)b.first.b.x(), (double)b.first.b.y(),
                            b.second.i);
                }
                Number newone = e.rbegin()->first.x_at_sweep_pos(p);
                fprintf(stderr, "     at sweep pos: %g (want %g)\n",
                        (double)newone, (double)p.x());
                if (started) {
                    if (sorting_criterion > newone) {
                        bad = true;
                        fprintf(stderr, "      MISORDERED!\n");
                    }
                }
                started = true;
                sorting_criterion = newone;
            }
            fprintf(stderr, "   end of active bundle list\n");
            assert(!bad);
#endif

            /*
             * Search the active edge set for edges which encounter
             * this point.
             */
            sweep = p;
            Edge searchedge(p, p);
            ActiveEdgeIterator ei;
            ActiveEdgeSet newedges(active_comparer);
            bool erased_something = false;
            while (1) {
                ActiveEdgeBundle tmpbundle;
                tmpbundle[searchedge] = Aux();
                ei = active_edges.lower_bound(tmpbundle);

                if (ei == active_edges.end())
                    break;

#ifdef PLANARISE_DIAGNOSTICS
                if (ei != active_edges.end()) {
                    fprintf(stderr, "     testing active edge bundle:\n");
                    for (BundleConstIterator bi = ei->begin();
                         bi != ei->end(); ++bi) {
                        fprintf(stderr, "       (%g,%g) - (%g,%g) aux=%d\n",
                                (double)bi->first.a.x(), (double)bi->first.a.y(),
                                (double)bi->first.b.x(), (double)bi->first.b.y(),
                                bi->second.i);
                    }
                    fprintf(stderr, "     at sweep pos: %g (want %g)\n",
                            (double)ei->rbegin()->first.x_at_sweep_pos(p),
                            (double)p.x());
                }
#endif

                if (ei->rbegin()->first.x_at_sweep_pos(p) != p.x()) {
#ifdef PLANARISE_DIAGNOSTICS
                    fprintf(stderr, "     this bundle does not meet our point, finishing\n");
#endif
                    break;             // we've done them all
                }

#ifdef PLANARISE_DIAGNOSTICS
                fprintf(stderr, "     this bundle meets our point\n");
#endif

                /*
                 * Iterate over the edges in this bundle, and process
                 * each separately.
                 */
                ActiveEdgeBundle newbundle;
                for (BundleConstIterator bi = ei->begin(); bi != ei->end(); ++bi) {
#ifdef PLANARISE_DIAGNOSTICS
                    fprintf(stderr, "     checking bundled edge (%g,%g) - (%g,%g) aux=%d\n",
                            (double)bi->first.a.x(), (double)bi->first.a.y(),
                            (double)bi->first.b.x(), (double)bi->first.b.y(),
                            bi->second.i);
#endif

                    /*
                     * If this edge does not start here (i.e. it has
                     * already started), emit an output edge from its
                     * start point to here.
                     */
                    if (bi->first.a != p) {
#ifdef PLANARISE_DIAGNOSTICS
                        fprintf(stderr, "       this edge does not start here\n");
#endif
                        connect(bi->first.a, p, bi->second);
                    }
                    /*
                     * If this edge does not end here, queue up a new edge
                     * to re-add to the active set in a moment.
                     */
                    if (bi->first.b != p) {
#ifdef PLANARISE_DIAGNOSTICS
                        fprintf(stderr, "       this edge does not end here\n");
#endif
                        newbundle[Edge(p, bi->first.b)] =
                            newbundle[Edge(p, bi->first.b)] + bi->second;
                    }
                }

                /*
                 * Delete this bundle from the active set, and queue
                 * up the modified bundle to insert in its place
                 * later.
                 */
                active_edges.erase(ei);
                erased_something = true;
                if (newbundle.size() > 0)
                    newedges.insert(newbundle);
            }

            /*
             * Now look for new edges starting from vertices at this
             * point. If we find any upward ones, add them as
             * additional active edges.
             */
            auto pmi = pointmap.find(p);
            if (pmi != pointmap.end()) {
                for (const OrigVertex *v: pmi->second) {
                    vector<const OrigVertex *> neighbours_of_v;
                    G.enum_neighbours(v, neighbours_of_v);
                    for (const OrigVertex *v2: neighbours_of_v) {
                        // Only consider upward edges
                        if (Point(v) < Point(v2)) {
                            Edge e(v, v2);
#ifdef PLANARISE_DIAGNOSTICS
                            fprintf(stderr, "     new original edge: "
                                    "(%g,%g) - (%g,%g) aux=%d\n",
                                    (double)e.a.x(), (double)e.a.y(),
                                    (double)e.b.x(), (double)e.b.y(),
                                    G.get_aux(v, v2).i);
#endif
                            ActiveEdgeBundle tmpbundle;
                            Aux aux = G.get_aux(v, v2);
                            tmpbundle[e] = aux;
                            ActiveEdgeIterator ei = newedges.find(tmpbundle);
                            if (ei == newedges.end()) {
                                newedges.insert(tmpbundle);
                            } else {
                                ActiveEdgeBundle oldbundle = *ei;
                                newedges.erase(oldbundle);
                                oldbundle[e] = oldbundle[e] + aux;
                                newedges.insert(oldbundle);
                            }
                        }
                    }
                }
            }

            /*
             * All the active edge bundles which encountered this
             * point have now been deleted from the set, but 'ev'
             * contains a collection of fresh bundles to add back in.
             */
            for (ActiveEdgeIterator nei = newedges.begin();
                 nei != newedges.end(); ++nei) {
#ifdef PLANARISE_DIAGNOSTICS
                fprintf(stderr, "     inserting active edge bundle:\n");
                for (BundleConstIterator bi = nei->begin();
                     bi != nei->end(); ++bi) {
                    fprintf(stderr, "       (%g,%g) - (%g,%g) aux=%d\n",
                            (double)bi->first.a.x(), (double)bi->first.a.y(),
                            (double)bi->first.b.x(), (double)bi->first.b.y(),
                            bi->second.i);
                }
#endif
                active_edges.insert(*nei);
            }

            /*
             * Now check the first and last of those new active edges
             * against their neighbours in the set. If they meet,
             * invent a new output point and add it to our set.
             */
            if (newedges.size() > 0) {
                ei = active_edges.find(*newedges.begin());
                if (ei != active_edges.begin() &&
                    ei != active_edges.end()) {
                    ActiveEdgeIterator prev = ei;
                    --prev;
                    collision_check(prev->rbegin()->first,
                                    ei->rbegin()->first);
                }
                ei = active_edges.find(*newedges.rbegin());
                if (ei != active_edges.end()) {
                    ActiveEdgeIterator next = ei;
                    ++next;
                    if (next != active_edges.end()) {
                        collision_check(ei->rbegin()->first,
                                        next->rbegin()->first);
                    }
                }
            } else if (erased_something) {
                /*
                 * If we've only taken edges out and not added any new
                 * ones, then we have brought a new pair of bundles
                 * into adjacency in the list, so we must
                 * collision-check those. The iterator 'ei' is still
                 * valid from the above loop, and points to the
                 * element just after the things we deleted.
                 */
                if (ei != active_edges.begin() &&
                    ei != active_edges.end()) {
                    ActiveEdgeIterator prev = ei;
                    --prev;
                    collision_check(prev->rbegin()->first,
                                    ei->rbegin()->first);
                }
            }
        }
    }

    bool adjacent(const Point &a, const Point &b) const {
        return (our_edges.find(Edge(a,b)) != our_edges.end());
    }

    Aux get_aux(const Point &a, const Point &b) const {
        return our_edges.find(Edge(a,b))->second;
    }

    /*
     * Now we provide in turn the interface expected by the
     * Triangulation class.
     */
    typedef Point Vertex;

    void enum_vertices(vector<const Vertex *> &out) const {
        for (const Point &p: our_points)
            out.push_back(&p);
    }
    void enum_neighbours(const Vertex *v,
                         vector<const Vertex *> &out) const {
        auto it = our_edges.lower_bound(Edge(*v, *v));
        while (it != our_edges.begin()) {
            --it;
            if (it->first.a != *v) {
                ++it;
                break;
            }
        }
        while (it != our_edges.end() && it->first.a == *v) {
            out.push_back(&(*our_points.find(it->first.b)));
            ++it;
        }
    }
};

#if 0
{ // mess with editors' indenting
{ // mess with editors' indenting
#endif
} // close namespace ZigguratInternal
using ZigguratInternal::Planarisation;
} // close namespace Ziggurat

#ifdef PLANARISE_TEST

#include <stdio.h>
#include <string.h>
#include <math.h>
#include <limits.h>

#include "bigrat.hh"

/*
g++ -std=c++11 -Wall -Werror -W -g -O0 -DPLANARISE_TEST -o planarise -x c++ planarise.hh -lm -lgmp && ./planarise > z.ps && gv z.ps
*/

using std::vector;

using Ziggurat::bigrat;
using Ziggurat::Planarisation;

#define MAXVERTS 200

int pages = 0;

struct Graph {
    typedef bigrat Number;

    struct Vertex {
        Number xc, yc;
        Number x() const { return xc; }
        Number y() const { return yc; }
    };

    Vertex verts[MAXVERTS];
    int nverts;
    int adjacency[MAXVERTS][MAXVERTS];
    bool aux_interesting;

    void enum_vertices(vector<const Vertex *> &out) const {
        for (int i = 0; i < nverts; i++)
            out.push_back(&verts[i]);
    }

    void enum_neighbours(const Vertex *v, vector<const Vertex *> &out) const {
        int vindex = v - (const Vertex *)verts;
        for (int i = 0; i < nverts; i++)
            if (adjacency[vindex][i] != INT_MIN)
                out.push_back(&verts[i]);
    }

    struct Aux {
        int i;
        Aux operator+(const Aux &b) const { return Aux(i+b.i); }
        Aux operator-() const { return Aux(-i); }
        Aux() : i(0) {}
        Aux(int ai) : i(ai) {}
        Aux(const Aux &b) : i(b.i) {}
    };
    Aux get_aux(const Vertex *v, const Vertex *w) const {
        int vindex = v - (const Vertex *)verts;
        int windex = w - (const Vertex *)verts;
        return Aux(adjacency[vindex][windex]);
    }

    Graph() {
        nverts = 0;
        for (int i = 0; i < MAXVERTS; i++)
            for (int j = 0; j < MAXVERTS; j++)
                adjacency[i][j] = INT_MIN;
        aux_interesting = false;
    }

    Vertex *add_vertex(Number x, Number y) {
        assert(nverts < MAXVERTS);
        verts[nverts].xc = x;
        verts[nverts].yc = y;
        return &verts[nverts++];
    }

    void add_edge(Vertex *v, Vertex *w, int aux = 0) {
        int vindex = v - verts, windex = w - verts;

        if (adjacency[vindex][windex] == INT_MIN)
            adjacency[vindex][windex] = 0;
        if (adjacency[windex][vindex] == INT_MIN)
            adjacency[windex][vindex] = 0;

        if (aux) {
            adjacency[vindex][windex] += aux;
            adjacency[windex][vindex] -= aux;
            aux_interesting = true;
        }
    }

    void runtest() {
        Planarisation<Graph> P(*this);

        Number xmin, xmax, ymin, ymax;
        xmin = xmax = verts[0].xc;
        ymin = ymax = verts[0].yc;
        for (int i = 1; i < nverts; i++) {
            xmin = min(xmin, verts[i].xc);
            xmax = max(xmax, verts[i].xc);
            ymin = min(ymin, verts[i].yc);
            ymax = max(ymax, verts[i].yc);
        }

        ++pages;
        printf("%%%%Page: %d %d\n", pages, pages);
        printf("%f %f %f %f graphbbox\n",
               (double)xmin, (double)ymin, (double)xmax, (double)ymax);
        for (int i = 0; i < nverts; i++)
            for (int j = i+1; j < nverts; j++)
                if (adjacency[i][j] != INT_MIN)
                    printf("%f %f %f %f originaledge\n",
                        (double)verts[i].xc, (double)verts[i].yc,
                        (double)verts[j].xc, (double)verts[j].yc);
        if (aux_interesting) {
            for (int i = 0; i < nverts; i++)
                for (int j = i+1; j < nverts; j++)
                    if (adjacency[i][j] != INT_MIN)
                        printf("%d %f %f %f %f originalaux\n",
                               adjacency[i][j],
                               (double)verts[i].xc, (double)verts[i].yc,
                               (double)verts[j].xc, (double)verts[j].yc);
        }
        for (int i = 0; i < nverts; i++)
            printf("%f %f vertex\n", (double)verts[i].xc, (double)verts[i].yc);
        printf("showpage\n");

        ++pages;
        printf("%%%%Page: %d %d\n", pages, pages);
        printf("%f %f %f %f graphbbox\n",
               (double)xmin, (double)ymin, (double)xmax, (double)ymax);
        for (auto &p: P.our_points)
            printf("%f %f vertex\n", (double)p.x(), (double)p.y());
        for (Planarisation<Graph>::pointset::iterator
                 pit = P.our_points.begin();
             pit != P.our_points.end(); ++pit) {
            Planarisation<Graph>::pointset::iterator pit2 = pit;
            for (++pit2; pit2 != P.our_points.end(); ++pit2)
                if (P.adjacent(*pit, *pit2))
                    printf("%f %f %f %f newedge\n",
                           (double)pit->x(), (double)pit->y(),
                           (double)pit2->x(), (double)pit2->y());
        }
        if (aux_interesting) {
            for (Planarisation<Graph>::pointset::iterator
                     pit = P.our_points.begin();
                 pit != P.our_points.end(); ++pit) {
                Planarisation<Graph>::pointset::iterator pit2 = pit;
                for (++pit2; pit2 != P.our_points.end(); ++pit2)
                    if (P.adjacent(*pit, *pit2))
                        printf("%d %f %f %f %f newaux\n",
                               P.get_aux(*pit, *pit2).i,
                               (double)pit->x(), (double)pit->y(),
                               (double)pit2->x(), (double)pit2->y());
            }
        }
        printf("showpage\n");
    }
};
typedef Graph::Vertex Vertex;

#define lenof(a) (sizeof((a))/sizeof(*(a)))

int main(int, char **)
{
    puts("%!PS-Adobe-1.0\n"
         "%%Pages: atend\n"
         "%%BoundingBox: 25 25 570 815\n"
         "%%EndComments\n"
         "%%BeginProlog\n"
         "/graphbbox {\n"
         "    /y1 exch def /x1 exch def /y0 exch def /x0 exch def\n"
         "    /xscale x1 x0 sub 495 exch div def\n"
         "    /yscale y1 y0 sub 740 exch div def\n"
         "    /ourscale xscale yscale min def\n"
         "    /xbase x1 x0 add ourscale mul 595 exch sub 2 div def\n"
         "    /ybase y1 y0 add ourscale mul 840 exch sub 2 div def\n"
         "} def\n"
         "/ourtransform {\n"
         "    ourscale mul ybase add exch\n"
         "    ourscale mul xbase add exch\n"
         "} def\n"
         "/edge {\n"
         "    ourtransform /y1 exch def /x1 exch def\n"
         "    ourtransform /y0 exch def /x0 exch def\n"
         "    /len x1 x0 sub dup mul y1 y0 sub dup mul add sqrt def\n"
         "    newpath\n"
         "    x0 x1 x0 sub len div 8 mul add\n"
         "    y0 y1 y0 sub len div 8 mul add moveto\n"
         "    x1 x0 x1 sub len div 8 mul add\n"
         "    y1 y0 y1 sub len div 8 mul add lineto\n"
         "    1 setlinewidth stroke\n"
         "} def\n"
         "/aux {\n"
         "    ourtransform 4 2 roll ourtransform 4 2 roll\n"
         "    3 index 2 index add 2 div 3 index 2 index add 2 div\n"
         "    gsave translate\n"
         "    3 2 roll sub 3 1 roll exch sub atan rotate\n"
         "    1 setlinecap 1 setlinejoin 1 setlinewidth\n"
         "    dup 0 eq {\n"
         "        pop 0 -5 moveto 0 5 lineto stroke\n"
         "    } {\n"
         "        dup 0 lt { neg 180 rotate } if\n"
         "        1 exch 1 exch { pop\n"
         "            newpath 0 5 moveto 5 0 lineto 0 -5 lineto stroke\n"
         "            5 0 translate\n"
         "        } for\n"
         "    } ifelse\n"
         "    grestore\n"
         "} def\n"
         "/vertex {\n"
         "    newpath ourtransform 3 0 360 arc 0 setgray fill\n"
         "} def\n"
         "/triangle {\n"
         "    newpath ourtransform moveto ourtransform lineto ourtransform lineto\n"
         "    closepath gsave clip clippath 0.5 setgray fill\n"
         "    clippath 1 setgray 5 setlinewidth stroke grestore\n"
         "} def\n"
         "/newedge { 1 0 0 setrgbcolor edge } def\n"
         "/originaledge { 0 setgray edge } def\n"
         "/newaux { 1 0 0 setrgbcolor aux } def\n"
         "/originalaux { 0 setgray aux } def\n"
         "%%EndProlog\n"
         "%%BeginSetup\n"
         "%%EndSetup");

    {
        Graph G;
        Vertex *a = G.add_vertex(10, 10);
        Vertex *b = G.add_vertex(20, 20);
        G.add_edge(a, b);
        Vertex *c = G.add_vertex(10, 20);
        Vertex *d = G.add_vertex(20, 10);
        G.add_edge(c, d);
        G.runtest();
    }

    {
        Graph G;
        Vertex *a = G.add_vertex(10, 10);
        Vertex *b = G.add_vertex(20, 20);
        G.add_edge(a, b);
        Vertex *c = G.add_vertex(10, 20);
        Vertex *d = G.add_vertex(20, 10);
        G.add_edge(c, d);
        G.add_vertex(15, 15);
        G.runtest();
    }

    {
        Graph G;
        Vertex *left[11], *right[11];
        left[0] = right[0] = G.add_vertex(0, 0);
        for (unsigned i = 1; i < lenof(left); i++)
            left[i] = G.add_vertex(-(int)i, i);
        for (unsigned i = 1; i < lenof(left); i++)
            right[i] = G.add_vertex(i, i);
        for (unsigned i = 0; i < lenof(left); i++)
            G.add_edge(left[i], right[lenof(right)-1-i]);
        G.runtest();
    }

    {
        Graph G;
        Vertex *a, *b, *d, *e;
        a = G.add_vertex(-3, 0);
        b = G.add_vertex(-1, 2);
        d = G.add_vertex(+1, 2);
        e = G.add_vertex(+3, 0);

        G.add_edge(a, b, 1);
        G.add_edge(b, e, 1);
        G.add_edge(e, a, 1);

        G.add_edge(a, G.add_vertex(+1,4), 1);
        G.add_edge(G.add_vertex(-1,4), e, 1);
        G.add_edge(e, a, 1);

        G.add_edge(a, d, 1);
        G.add_edge(d, e, 1);
        G.add_edge(e, a, 1);

        G.runtest();
    }

    {
        Graph G;
        Vertex *a;
        G.add_edge(G.add_vertex(-3,0),G.add_vertex(+2,10));
        G.add_edge(G.add_vertex(+3,0),G.add_vertex(-2,10));
        a = G.add_vertex(0,3);
        G.add_edge(G.add_vertex(-7,-1), a);
        G.add_edge(G.add_vertex( 0,-1), a);
        G.add_edge(G.add_vertex(+7,-1), a);
        G.runtest();
    }

    printf("%%%%Trailer\n"
           "%%%%Pages: %d\n"
           "%%%%EOF\n", pages);

    return 0;
}

#endif

#endif /* PLANARISE_HH */
