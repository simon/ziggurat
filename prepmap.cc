/*
 * Program to prepare a height-map file from input height data, ready
 * to trace and convert into an STL model.
 */

#include <assert.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <limits.h>

#include <err.h>

int main(int argc, char **argv)
{
    char *infile = NULL, *outfile = NULL;
    int iw = 0, ih = 0;                // pixels
    float inxres = 0;                  // mm/pixel
    float inyres = 0;                  // mm/pixel
    float outres = 0;                  // mm/pixel
    float blobradius = 0;              // mm
    enum { NORMAL, DIM_W, DIM_H } outtype = NORMAL;

    bool doing_opts = true;
    while (--argc > 0) {
        char *p = *++argv;
        if (doing_opts && p[0] == '-') {
            if (p[1] == '-') {
                if (!strcmp(p, "--")) {
                    doing_opts = false;
                } else if (!strcmp(p, "--help")) {
                    printf("usage:   prepmap <options> <infile>\n"
                           "options: -o OUTFILE      output map file name\n"
                           "         -w XSIZE        width of input map file, pixels\n"
                           "         -h XSIZE        height of input map file, pixels\n"
                           "         -x INXRES       resolution of input map file in x directionm, mm/pixel\n"
                           "         -y INYRES       resolution of input map file in x directionm, mm/pixel\n"
                           "         -r OUTRES       resolution of output image, mm/pixel\n"
                           "         -b BLOBSIZE     radius of circular blob to convolve with image, mm\n"
                           "         -D DIMENSION    print an output grid dimension ('w' or 'h') instead\n"
                           "                         of generating the main output data\n");
                    return 0;
                } else {
                    errx(1, "unrecognised option '%s'", p);
                }
            } else {
                char c;
                while (*p && (c = *++p) != '\0') {
                    switch (c) {
                      case 'w':
                      case 'h':
                      case 'x':
                      case 'y':
                      case 'r':
                      case 'b':
                      case 'o':
                      case 'D':
                        /*
                         * Options requiring an argument.
                         */
                        char *arg = p+1;
                        if (*arg)
                            p += strlen(p); // eat remainder of argv word
                        else if (--argc > 0)
                            arg = *++argv;
                        else
                            errx(1, "expected an argument to option '-%c'", c);

                        switch (c) {
                          case 'w':
                            iw = atoi(arg);
                            break;
                          case 'h':
                            ih = atoi(arg);
                            break;
                          case 'x':
                            inxres = atof(arg);
                            break;
                          case 'y':
                            inyres = atof(arg);
                            break;
                          case 'r':
                            outres = atof(arg);
                            break;
                          case 'b':
                            blobradius = atof(arg);
                            break;
                          case 'o':
                            outfile = arg;
                            break;
                          case 'D':
                            if (!strcmp(arg, "w"))
                                outtype = DIM_W;
                            else if (!strcmp(arg, "h"))
                                outtype = DIM_H;
                            else
                                errx(1, "unrecognised dimension '%s'", arg);
                            break;
                        }
                    }
                }
            }
        } else {
            if (!infile)
                infile = p;
            else
                errx(1, "unexpected additional argument '%s'", p);
        }
    }

    if (!iw) errx(1, "expected an image width (-w option)");
    if (!ih) errx(1, "expected an image height (-h option)");
    if (!inxres) errx(1, "expected an input x resolution (-x option)");
    if (!inyres) errx(1, "expected an input y resolution (-y option)");
    if (!outres) errx(1, "expected an output resolution (-r option)");
    if (!blobradius) errx(1, "expected a blob radius (-b option)");
    if (!infile) errx(1, "expected an input file name");
    if (outtype == NORMAL && !outfile) errx(1, "expected an output file name");

    /*
     * Compute the output map dimensions, and if all we're doing is
     * writing them to the output, do that and stop.
     */
    int ow = (iw * inxres + 2 * blobradius) / outres + 2;
    int oh = (ih * inyres + 2 * blobradius) / outres + 2;
    int oorigin = (int)(blobradius/outres) + 1;
    if (outtype != NORMAL) {
        FILE *ofp;
        if (outfile) {
            ofp = fopen(outfile, "wb");
            if (!ofp)
                err(1, "outfile: open");
        } else {
            ofp = stdout;
        }
        switch (outtype) {
          case DIM_W:
            fprintf(ofp, "%d\n", ow);
            break;
          case DIM_H:
            fprintf(ofp, "%d\n", oh);
            break;
          default:
            assert(false && "unreachable");
        }
        if (outfile)
            fclose(ofp);
        return 0;
    }

    /*
     * Read the input map file.
     */
    short *inmap = new short[iw*ih];
    FILE *ifp = fopen(infile, "rb");
    for (int iy = 0; iy < ih; iy++)
        for (int ix = 0; ix < iw; ix++) {
            int hi = fgetc(ifp);
            if (hi < 0)
                errx(1, "EOF at (%d,%d) in map", ix, iy);
            int lo = fgetc(ifp);
            if (lo < 0)
                errx(1, "EOF half way through (%d,%d) in map", ix, iy);
            long val = hi * 256 + lo;
            val -= (val & 0x8000L) << 1;
            inmap[iy*iw+ix] = val;
        }
    fclose(ifp);

    /*
     * Write the modified output map.
     */
    FILE *ofp = fopen(outfile, "wb");
    if (!ofp)
        err(1, "outfile: open");
    for (int oy = 0; oy < oh; oy++)
        for (int ox = 0; ox < ow; ox++) {
            float x = (ox - oorigin) * outres;
            float y = (oy - oorigin) * outres;

            int xmin = (x - blobradius) / inxres;
            int ymin = (y - blobradius) / inyres;
            int xmax = (x + blobradius) / inxres + 1;
            int ymax = (y + blobradius) / inyres + 1;

            short max = -9999;
            for (int by = ymin; by <= ymax; by++) {
                if (by < 0 || by >= ih) continue;
                float dy = by * inyres - y;
                for (int bx = xmin; bx <= xmax; bx++) {
                    if (bx < 0 || bx >= iw) continue;
                    float dx = bx * inxres - x;
                    if (dx*dx + dy*dy <= blobradius*blobradius) {
                        if (max < inmap[by*iw+bx])
                            max = inmap[by*iw+bx];
                    }
                }
            }

            fputc((max >> 8) & 0xFF, ofp);
            fputc(max & 0xFF, ofp);
        }
    fclose(ofp);

    return 0;
}
