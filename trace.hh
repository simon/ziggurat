/*
 * Trace round a bitmap.
 */

#include <vector>
#include <set>
#include <map>
#include <algorithm>
#include <assert.h>
#include <stddef.h>
#include <climits>
#ifdef TRACE_DIAGNOSTICS
#include <stdio.h>
#endif

#include "triangulate.hh"
#include "planarise.hh"
#include "merge.hh"
#include "edsf.hh"
#include "bigrat.hh"

namespace Ziggurat {
namespace ZigguratInternal {
#if 0
} // mess with editors' indenting
} // mess with editors' indenting
#endif

using std::vector;
using std::map;

/*
 * Expect 'Bitmap' type to support:
 *   int width();
 *   int height();
 *   bool getpixel(int x, int y); // 0 <= x < width, 0 <= y < height
 *
 * We presume that y-coordinates go up from the bottom (and all
 * comments within this class are written on that basis), and we
 * output a point list tracing round each region clockwise. If you
 * think y-coordinates go down from the top, you'll still get a valid
 * trace, but it'll go round each region anticlockwise. (Of course,
 * you can easily iterate over the output point lists in the opposite
 * order if you need to.)
 */
template<class Bitmap>
class Trace {
    typedef bigrat Number;
    typedef enum { RIGHT, UP, LEFT, DOWN } Direction;

    static Direction finddir(int xs, int ys, int xe, int ye) {
        if (ys == ye && xs < xe)
            return RIGHT;
        else if (ys < ye && xs == xe)
            return UP;
        else if (ys == ye && xs > xe)
            return LEFT;
        else {
            assert(ys > ye && xs == xe);
            return DOWN;
        }
    }

    static int dx(Direction dir) {
        return dir == LEFT ? -1 : dir == RIGHT ? +1 : 0;
    }
    static int dy(Direction dir) {
        return dir == DOWN ? -1 : dir == UP ? +1 : 0;
    }

    static Direction turnleft(Direction d) { return (Direction)((d+1) & 3); }
    static Direction reverse(Direction d) { return (Direction)((d+2) & 3); }
    static Direction turnright(Direction d) { return (Direction)((d+3) & 3); }

    struct GridEdge {
        Direction dir;
        int xstart, ystart, xend, yend;
        GridEdge *next, *prev;
        void setup(int xs, int ys, int xe, int ye) {
            dir = finddir(xs, ys, xe, ye);
            xstart = xs; ystart = ys; xend = xe; yend = ye;
        }
        GridEdge(int xs, int ys, int xe, int ye) : next(NULL), prev(NULL) {
            setup(xs, ys, xe, ye);
        }
        void linkto(GridEdge *anext) {
            next = anext;
            anext->prev = this;
        }
        int len() { return abs(xend-xstart) + abs(yend-ystart); }
    };

    GridEdge **edges;

    Number fillratio;

    const Bitmap *bmp;
    int w, h, W, H;
    bool getpixel(int x, int y) {
        return (x >= 0 && x < w && y >= 0 && y < h) ?
                bmp->getpixel(x,y) : false;
    }

  public:
    struct Point {
        Number x, y;
        Point(const Number &ax, const Number &ay) : x(ax), y(ay) {}
    };

    typedef vector<Point> Cycle;
    typedef vector<Cycle> CycleList;

    CycleList cycles;
    bool convex;

    Trace(Number afillratio, bool aconvex)
        : edges(NULL), fillratio(afillratio), convex(aconvex) {}

#ifndef TRACE_TEST
  private:
#endif
    void makecycles(const Bitmap &bitmap) {
        bmp = &bitmap;
        w = bitmap.width();
        h = bitmap.height();
        W = w+1;
        H = h+1;

        /*
         * Find all grid edges.
         */
        edges = new GridEdge *[W*H*4];
        for (int i = 0; i < W*H*4; i++) edges[i] = NULL;

        for (int y = 0; y < h; y++) {
            for (int x = 0; x < W; x++) {
                bool lpix = getpixel(x-1, y);
                bool rpix = getpixel(x  , y);
                if (!lpix && rpix)
                    edges[(y*W+x) * 4 + UP] = new GridEdge(x,y,x,y+1);
                if (lpix && !rpix)
                    edges[((y+1)*W+x) * 4 + DOWN] = new GridEdge(x,y+1,x,y);
            }
        }
        for (int y = 0; y < H; y++) {
            for (int x = 0; x < w; x++) {
                bool dpix = getpixel(x, y-1);
                bool upix = getpixel(x, y  );
                if (!dpix && upix)
                    edges[(y*W+(x+1)) * 4 + LEFT] = new GridEdge(x+1,y,x,y);
                if (dpix && !upix)
                    edges[(y*W+x) * 4 + RIGHT] = new GridEdge(x,y,x+1,y);
            }
        }

        /*
         * Join the edges into cyclic lists.
         */
        for (int i = 0; i < W*H*4; i++) {
            GridEdge *e = edges[i];
            if (!e || e->next)         // this is not an as-yet-unlinked edge
                continue;
            GridEdge *start = e;
            bool moved = false;
#ifdef TRACE_DIAGNOSTICS
            fprintf(stderr, "New loop\n");
#endif
            while (!(moved && e == start)) {
#ifdef TRACE_DIAGNOSTICS
                fprintf(stderr, "  Edge %p: (%d,%d) going %s to (%d,%d)\n",
                        e, e->xstart, e->ystart,
                        e->dir == LEFT ? "left" :
                        e->dir == RIGHT ? "right" :
                        e->dir == UP ? "up" : "down",
                        e->xend, e->yend);
#endif
                // Turn right wherever possible.
                int point = (e->yend * W + e->xend);
                GridEdge *next;
                if ((next = edges[point * 4 + turnright(e->dir)]) != NULL) {
#ifdef TRACE_DIAGNOSTICS
                    fprintf(stderr, "  Turn right\n");
#endif
                    e->linkto(next);
                    e = next;
                    moved = true;
                } else if (edges[point * 4 + e->dir]) {
                    // Go straight on by lengthening this edge, rather
                    // than joining two collinear short ones together.
                    // Completely delete the edge we're subsuming into
                    // this one.
#ifdef TRACE_DIAGNOSTICS
                    fprintf(stderr, "  Straight on\n");
#endif
                    delete edges[point * 4 + e->dir];
                    edges[point * 4 + e->dir] = NULL;
                    e->xend += dx(e->dir);
                    e->yend += dy(e->dir);
                } else {
#ifdef TRACE_DIAGNOSTICS
                    fprintf(stderr, "  Turn left\n");
#endif
                    next = edges[point * 4 + turnleft(e->dir)];
                    assert(next);      // this _must_ work, if we get here
                    e->linkto(next);
                    e = next;
                    moved = true;
                }
            }
        }

        /*
         * Iterate around each list constructing loops of points.
         */
        for (int i = 0; i < W*H*4; i++) {
            GridEdge *e;
            GridEdge *start = edges[i];

            /*
             * We want to start on the bottommost edge of the region,
             * just before a right turn. This makes the convex-corner
             * finding pass easier, because we know we're at a corner
             * that's going to be kept.
             */
            if (!start || start->dir == LEFT || start->next->dir == UP)
                continue;

            /*
             * First look at all the corners, to decide which ones
             * give rise to diagonals for convexity.
             */
            if (convex) {
                e = start;
                do {
                    convex_corner_scan(e, e->next);
                    e = e->next;
                } while (e != start);
            }

            start_loop();
            e = start;
            do {
                do_corner(e, e->next);
                e = e->next;
            } while (e != start);
            end_loop();

            // Erase this whole cycle from the edges array, to avoid
            // going over them again.
            e = start;
            do {
                GridEdge *next = e->next;
                assert(edges[(e->ystart * W + e->xstart) * 4 + e->dir] == e);
                edges[(e->ystart * W + e->xstart) * 4 + e->dir] = NULL;
                delete e;
                e = next;
            } while (e != start);
        }

        bmp = NULL;                    // clean up, just in case
    }

  private:
    // used for OutlineMerge
    struct Aux {
        int i;
        Aux() : i(0) {}
        Aux(int ai) : i(ai) {}
        Aux(const Aux &b) : i(b.i) {}
        Aux operator+(const Aux &b) const { return Aux(i + b.i); }
        Aux operator-(const Aux &b) const { return Aux(i - b.i); }
        bool operator==(const Aux &b) const { return i == b.i; }
        Aux operator-() const { return Aux(-i); };
    };

  public:
    void trace(const Bitmap &bitmap) {
        makecycles(bitmap);

        typedef OutlineMerge<Number, Aux> OurOutlineMerge;
        OurOutlineMerge om;

#ifdef TRACE_DIAGNOSTICS
        fprintf(stderr, "postprocessing: begin\n");
#endif
        for (Cycle &c: cycles) {
            Point *p0p = &(*c.rbegin());
            for (Point &p1: c) {
                om.add_edge(p0p->x, p0p->y, p1.x, p1.y, Aux(+1));
                p0p = &p1;
            }
        }

        om.compute();

        /*
         * Find the edges of the merged outline which we're keeping,
         * by seeing whether each edge separates a triangle at the
         * same level as the exterior face from one at a 'more
         * interior' level than that.
         */
        vector<bool> edges;
        for (int i = 0; i < om.edges.size(); i++) {
            auto &e = om.edges[i];
            Aux &auxl = om.triangles[e.til].aux;
            Aux &auxr = om.triangles[e.tir].aux;
#ifdef TRACE_DIAGNOSTICS
            fprintf(stderr,
                    "postprocessing: edge (%g,%g) -> (%g,%g): left #%d (%d), right #%d (%d)\n",
                    (double)om.points[e.pi0].x, (double)om.points[e.pi0].y,
                    (double)om.points[e.pi1].x, (double)om.points[e.pi1].y,
                    e.til, auxl.i, e.tir, auxr.i);
#endif
            if (auxl.i == 0 && auxr.i > 0) {
#ifdef TRACE_DIAGNOSTICS
                fprintf(stderr,
                        "postprocessing: keeping edge (%g,%g) -> (%g,%g)\n",
                        (double)om.points[e.pi0].x, (double)om.points[e.pi0].y,
                        (double)om.points[e.pi1].x, (double)om.points[e.pi1].y);
#endif
                edges.push_back(true);
            } else {
                edges.push_back(false);
            }
        }

        /*
         * Work out which edge follows each edge in that vector.
         */
        map<int, int> nextedge;
        for (int i = 0; i < om.edges.size(); i++) {
            if (!edges[i])
                continue;
            auto &p = om.points[om.edges[i].pi1];
            int n = p.ei_in.size();
            int next = -1;
            for (int j = 0; j < n; j++) {
                if (p.ei_in[j] == i) {
                    for (int k = 1; k <= n; k++) {
                        int kk = (j + k) % n;
                        int ei = p.ei_out[kk];
                        if (edges[ei]) {
                            next = ei;
                            break;
                        }
                    }
                    break;
                }
            }
            assert(next >= 0);
            nextedge[i] = next;
        }

        /*
         * And now we can rewrite our entire cycle representation by
         * pulling a fresh set of cycles out of that map.
         */
        cycles.clear();
        for (auto nei = nextedge.begin(); nei != nextedge.end();
             nei = nextedge.begin()) {
            start_loop();
#ifdef TRACE_DIAGNOSTICS
            fprintf(stderr, "postprocessing: start loop\n");
#endif
            int start = nei->first;

            do {
                int p = om.edges[nei->first].pi0;
                int q = om.edges[nei->first].pi1;
                int r = om.edges[nei->second].pi1;

                /*
                 * Output point q, unless the lines before and after
                 * it are collinear.
                 */
                auto &P = om.points[p], &Q = om.points[q], &R = om.points[r];
                Number pqx = Q.x - P.x, pqy = Q.y - P.y;
                Number qrx = R.x - Q.x, qry = R.y - Q.y;
#ifdef TRACE_DIAGNOSTICS
                fprintf(stderr, "postprocessing: collinearity check (%g,%g)-(%g,%g)-(%g,%g): %g*%g =?= %g*%g\n",
                        (double)P.x, (double)P.y,
                        (double)Q.x, (double)Q.y,
                        (double)R.x, (double)R.y,
                        (double)pqx, (double)qry, (double)pqy, (double)qrx);
#endif
                if (pqx * qry != pqy * qrx) {
#ifdef TRACE_DIAGNOSTICS
                    fprintf(stderr, "postprocessing: add point (%g,%g)\n",
                            (double)Q.x, (double)Q.y);
#endif
                    add_point(Q.x, Q.y);
                }

                nei = nextedge.find(nei->second);
            } while (nei->first != start);
            end_loop();
#ifdef TRACE_DIAGNOSTICS
            fprintf(stderr, "postprocessing: end loop\n");
#endif

            /*
             * Now go round again and erase this cycle, so that we
             * eventually empty the nextpoint map and stop.
             */
            do {
                int p = nei->second;
                nextedge.erase(nei);
                nei = nextedge.find(p);
            } while (nei != nextedge.end());
        }

#ifdef TRACE_DIAGNOSTICS
        fprintf(stderr, "postprocessing: finish\n");
#endif
    }

  private:
    Cycle current;

    void start_loop() {
        current.clear();
    }

    void add_point(Point xy) {
        current.push_back(xy);
    }
    void add_point(Number x, Number y) {
        add_point(Point(x, y));
    }

    Number makecoord(int a, int dir) {
        return (Number)a + dir * (1-fillratio) / Number(2);
    }

    void end_loop() {
        cycles.push_back(current);
    }

    void convex_corner_scan_oneway(int xstart, int ystart,
                                   Direction sdir, Direction fdir) {
        int fdx = dx(fdir), fdy = dy(fdir);
        int sdx = dx(sdir), sdy = dy(sdir);

        /*
         * This is the meat of the convex corner scan. We are given a
         * starting grid point (not pixel centre) and two right-angle
         * directions which we'll call 'forwards' and 'sideways' (to
         * avoid confusion with any of the fixed directions LRUD). Our
         * aim is to scan forwards-and-sideways from here and identify
         * a right-angled triangle between grid points, with
         * hypotenuse sloping as near to the 'f' direction as
         * possible, such that every point contained within the
         * triangle is the forwards-antisideways corner of a set
         * pixel. We will then add that triangle to our trace as a
         * small path.
         */

        /*
         * Start by working out the offset which converts a grid point
         * into the bitmap coordinates of the pixel whose FAS corner
         * it is. That is, we head half a square backwards and
         * sideways, and then round down. Having done that, we can
         * define a macro to do that conversion on calling getpixel,
         * and add the starting point coordinates while we're at it.
         */
        int pdx = (sdx - fdx - 1) / 2, pdy = (sdy - fdy - 1) / 2;
#define PIXEL(s,f) getpixel(xstart+(f)*fdx+(s)*sdx+pdx, \
                            ystart+(f)*fdy+(s)*sdy+pdy)

        /*
         * So we expect for a start that PIXEL(0,0) is true, and
         * PIXEL(0,1) is not.
         */
        assert(PIXEL(0,0));
        assert(!PIXEL(0,1));

        /*
         * Move one pixel sideways at a time, and scan forwards along
         * the new file of pixels to find where the set pixels stop.
         */
        int s = 0;    // last s-coordinate we looked at
        int fset = 0; // f-coord of last (useful) set pixel in that file

        while (1) {
            int snew = s+1;

            /*
             * We aren't interested in set pixels beyond the maximum
             * slope to which we're already constrained by previous
             * files (if any).
             */
            int flimit;
            if (s > 0)
                flimit = (fset+1) * snew / s - 1;
            else
                flimit = INT_MAX;      // just getting started; no limit yet

            /*
             * Scan along this file for the last interesting set pixel.
             */
            int f;
            for (f = 0; f <= flimit && PIXEL(snew,f); f++);
            int fset_new = f - 1;

            /*
             * If including this file would reduce our maximum slope,
             * then we're done, so stop.
             */
            if (fset_new * s < fset * snew)
                break;

            /*
             * Otherwise, this file is good; accept it and continue.
             */
            s = snew;
            fset = fset_new;

            break;                     // hack hack: be less ambitious
        }

#undef PIXEL

        /*
         * Convert back to coordinates of a grid point.
         */
        int xend = xstart + s*sdx + fset*fdx;
        int yend = ystart + s*sdy + fset*fdy;

#ifdef TRACE_DIAGNOSTICS
        fprintf(stderr, "    found (%d,%d)\n", xend, yend);
#endif

        /*
         * Now we want to make a triangle to cover this jagged edge.
         * Our third point wants to have the 'forwards' coordinate of
         * (xstart,start), and the 'sideways' coordinate of
         * (xend,yend).
         */
        int xthird = xstart + s*sdx;
        int ythird = ystart + s*sdy;

        /*
         * We want all paths to be clockwise, so see which way this
         * triangle goes.
         */
        Direction start_to_third = finddir(xstart, ystart, xthird, ythird);
        Direction third_to_end = finddir(xthird, ythird, xend, yend);
        bool right = (third_to_end == turnright(start_to_third));

        /*
         * Now actually construct the point list for this triangle,
         * putting the third point in an appropriate place depending
         * on the orientation we just computed.
         */
        int xmod = sdx-fdx, ymod = sdy-fdy;
        start_loop();
        add_point(makecoord(xstart, xmod), makecoord(ystart, ymod));
        if (right)
            add_point(makecoord(xthird, xmod), makecoord(ythird, ymod));
        add_point(makecoord(xend, xmod), makecoord(yend, ymod));
        if (!right)
            add_point(makecoord(xthird, xmod), makecoord(ythird, ymod));
        end_loop();
    }

    void convex_corner_scan(GridEdge *e, GridEdge *f) {
        if (f->dir == turnleft(e->dir)) {
#ifdef TRACE_DIAGNOSTICS
            fprintf(stderr, "Convex scan at (%d,%d) == %p / %p\n",
                    e->xend, e->yend, e, f);
#endif
            /*
             * Left-turn corner. We must look to see what the
             * shallowest diagonal is that will start from one step
             * before the end of the incoming edge e and leave in the
             * general direction of the outgoing edge f. We also do
             * the same thing in the reverse direction, from one step
             * after the start of f and heading back along the end of
             * e.
             */
            convex_corner_scan_oneway(e->xend - dx(e->dir),
                                      e->yend - dy(e->dir),
                                      e->dir, f->dir);
            convex_corner_scan_oneway(f->xstart + dx(f->dir),
                                      f->ystart + dy(f->dir),
                                      reverse(f->dir), reverse(e->dir));
        }
    }

    void do_corner(GridEdge *e, GridEdge *f) {
        int xmod = 0, ymod = 0;    // which way to perturb
        /*
         * We always have a filled region on our right, so start
         * by shifting the point's coordinates rightward.
         */
        xmod += dx(turnright(e->dir));
        ymod += dy(turnright(e->dir));
        /*
         * Now shift forward or backward depending on whether
         * we're turning left or right respectively. This is most
         * easily done by looking at the direction of the next
         * edge.
         */
        xmod += dx(turnright(f->dir));
        ymod += dy(turnright(f->dir));

        add_point(makecoord(e->xend, xmod), makecoord(e->yend, ymod));
    }
};

#if 0
{ // mess with editors' indenting
{ // mess with editors' indenting
#endif
} // close namespace ZigguratInternal
using ZigguratInternal::Trace;
} // close namespace Ziggurat

#ifdef TRACE_TEST

/*
g++ -std=c++11 -DTRACE_TEST -g -O0 -o trace -x c++ trace.hh -lgmp && ./trace > z.ps && gv z.ps
 */

#include <stdarg.h>
#include <string.h>
#include <stdio.h>

using Ziggurat::bigrat;
using Ziggurat::Trace;

int pages = 0;

struct TestBitmap {
    int w, h;
    unsigned char *pixels;

    TestBitmap(const char *firstline, ...) {
        va_list ap;
        int x, y;
        const char *s;

        w = strlen(firstline);
        h = 0;

        va_start(ap, firstline);
        for (s = firstline; s != NULL; s = va_arg(ap, const char *)) {
            assert(strlen(s) == w);
            h++;
        }
        va_end(ap);

        pixels = new unsigned char[w*h];

        y = 0;
        va_start(ap, firstline);
        for (s = firstline; s != NULL; s = va_arg(ap, const char *)) {
            for (x = 0; x < w; x++)
                pixels[y*w+x] = (s[x] == ' ' ? 0 : 1);
            y++;
        }
        va_end(ap);
    }

    ~TestBitmap() {
        delete pixels;
    }

    int width() const { return w; }
    int height() const { return h; }
    bool getpixel(int x, int y) const {
        assert(0 <= x); assert(x < w);
        assert(0 <= y); assert(y < h);
        // This is where we do the y-flip
        return pixels[(h-1-y)*w+x] != 0;
    }

    void singletest(int pixpercent, bool intermediate, bool convex) {
        Trace<TestBitmap> tr(bigrat(pixpercent, 100), convex);
        if (intermediate)
            tr.makecycles(*this);
        else
            tr.trace(*this);

        ++pages;
        printf("%%%%Page: %d %d\n", pages, pages);

        printf("%d %d bbox\n", w, h);

        for (int y = 0; y < h; y++)
            for (int x = 0; x < w; x++)
                printf("%s %.2f %d.5 %d.5 pixel\n",
                       pixels[y*w+x] ? "0.45" : "0.9",
                       pixpercent/100.0, x, h-1-y);

        for (Trace<TestBitmap>::Cycle &c: tr.cycles) {
            printf("newcycle\n");
            for (const Trace<TestBitmap>::Point &p: c)
                printf("%f %f point\n", (double)p.x, (double)p.y);
            printf("endcycle\n");
        }

        printf("showpage\n");
    }

    void runtest() {
        singletest(75, false, false);
        singletest(75, true, true);
        singletest(75, false, true);
        singletest(100, false, false);
        singletest(100, true, true);
        singletest(100, false, true);
    }
};

int main(int, char **)
{
    puts("%!PS-Adobe-1.0\n"
         "%%Pages: atend\n"
         "%%BoundingBox: 25 25 570 815\n"
         "%%EndComments\n"
         "%%BeginProlog\n"
         "/bbox {\n"
         "    /y1 exch def /x1 exch def /y0 0 def /x0 0 def\n"
         "    /xscale x1 x0 sub 495 exch div def\n"
         "    /yscale y1 y0 sub 740 exch div def\n"
         "    /ascale xscale yscale min def\n"
         "    /xbase x1 x0 add ascale mul 595 exch sub 2 div def\n"
         "    /ybase y1 y0 add ascale mul 840 exch sub 2 div def\n"
         "} def\n"
         "/transform {\n"
         "    ascale mul ybase add exch\n"
         "    ascale mul xbase add exch\n"
         "} def\n"
         "/pixel {\n"
         "    gsave transform translate ascale dup scale\n"
         "    2 div\n"
         "    dup dup moveto dup dup neg lineto\n"
         "    dup neg dup lineto neg dup neg lineto\n"
         "    closepath setgray fill grestore\n"
         "} def\n"
         "/newcycle {\n"
         "    /nextaction /moveto load def\n"
         "    newpath\n"
         "} def\n"
         "/point {\n"
         "    1 index 1 index transform\n"
         "    gsave newpath 4 0 360 arc 0 setgray fill grestore\n"
         "    transform nextaction\n"
         "    /nextaction /lineto load def\n"
         "} def\n"
         "/endcycle {\n"
         "    closepath 0 setgray 2 setlinewidth 1 setlinejoin stroke\n"
         "} def\n"
         "%%EndProlog\n"
         "%%BeginSetup\n"
         "%%EndSetup");

    TestBitmap("   ***   ",
               " ******* ",
               " ******* ",
               "*********",
               "*********",
               "*********",
               " ******* ",
               " ******* ",
               "   ***   ",
               NULL).runtest();

    TestBitmap("  ** *  *",
               "  ** *  *",
               "  ** *  *",
               "**** ****",
               "**     **",
               "*********",
               "*** *  **",
               "*** *  * ",
               "***      ",
               NULL).runtest();

    TestBitmap(" *    *  ",
               "*******  ",
               "******** ",
               " ********",
               "  *******",
               " ********",
               "*******  ",
               " *****   ",
               "   ***   ",
               NULL).runtest();

    TestBitmap("*****************",
               "*    ************",
               "**    ***********",
               "****** **********",
               "*******      *** ",
               "**********       ",
               "***********      ",
               "***********      ",
               "************     ",
               NULL).runtest();

    TestBitmap("        ****  ",
               "        ***** ",
               "       ****** ",
               "       ****** ",
               "     ******** ",
               "     *******  ",
               "    ******    ",
               "     *****    ",
               "    ******    ",
               " ***********  ",
               "************* ",
               "************* ",
               "**************",
               " *************",
               "  ************",
               "      ******* ",
               "        ***** ",
               "        ****  ",
               NULL).runtest();

    TestBitmap("        ***** ",
               "        ***** ",
               "       *******",
               "       *******",
               "     ******** ",
               "     ******** ",
               "    ********  ",
               "     *****    ",
               "   *******    ",
               " ***********  ",
               "************* ",
               "**************",
               "**************",
               "**************",
               " *************",
               "      ******* ",
               "        ***** ",
               "        ****  ",
               NULL).runtest();

    printf("%%%%Trailer\n"
           "%%%%Pages: %d\n"
           "%%%%EOF\n", pages);

    return 0;
}
#endif
