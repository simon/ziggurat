/*
 * Program to take a slice through a height-map file, trace round it,
 * and output a textual outline description.
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <limits.h>
#include <assert.h>

#include <err.h>

#include <png.h>

#include "trace.hh"
#include "numparse.hh"

using Ziggurat::bigrat;
using Ziggurat::Trace;

struct Bitmap {
    int w, h, H;
    short *pixels;

    int width() const { return w; }
    int height() const { return h; }
    bool getpixel(int x, int y) const {
        assert(0 <= x); assert(x < w);
        assert(0 <= y); assert(y < h);
        // This is where we do the y-flip
        return pixels[(h-1-y)*w+x] >= H;
    }
};

int main(int argc, char **argv)
{
    char *infile = NULL, *outline = NULL, *bitmap = NULL;
    bigrat fillratio(3,4);             // default 0.75
    bigrat scale(1);
    int iw = 0, ih = 0;                // pixels
    int height = -9998;
    bool convex = true;

    bool doing_opts = true;
    while (--argc > 0) {
        char *p = *++argv;
        if (doing_opts && p[0] == '-') {
            if (p[1] == '-') {
                if (!strcmp(p, "--")) {
                    doing_opts = false;
                } else if (!strcmp(p, "--help")) {
                    printf("usage:   tracemap <options> <infile>\n"
                           "options: -o OUTLINEFILE  output file name for traced outline\n"
                           "         -b BITMAPFILE   output file name for the bitmap that was traced\n"
                           "         -w XSIZE        width of input map file, pixels\n"
                           "         -h XSIZE        height of input map file, pixels\n"
                           "         -H HEIGHT       elevation at which to slice the map, map height units\n"
                           "         -F FILLRATIO    proportion of pixel edge to retain in trace\n"
                           "         -N              trace non-convexly\n");
                    return 0;
                } else {
                    errx(1, "unrecognised option '%s'", p);
                }
            } else {
                char c;
                while (*p && (c = *++p) != '\0') {
                    switch (c) {
                      case 'N':
                        convex = false;
                        break;

                      case 'w':
                      case 'h':
                      case 'H':
                      case 'F':
                      case 'r':
                      case 'o':
                      case 'b':
                        /*
                         * Options requiring an argument.
                         */
                        char *arg = p+1;
                        if (*arg)
                            p += strlen(p); // eat remainder of argv word
                        else if (--argc > 0)
                            arg = *++argv;
                        else
                            errx(1, "expected an argument to option '-%c'", c);

                        switch (c) {
                          case 'w':
                            iw = atoi(arg);
                            break;
                          case 'h':
                            ih = atoi(arg);
                            break;
                          case 'H':
                            height = atoi(arg);
                            break;
                          case 'F':
                            if (!numparse(arg, &fillratio))
                                errx(1, "unable to parse '%s' as a fill ratio",
                                    arg);
                            break;
                          case 'r':
                            if (!numparse(arg, &scale))
                                errx(1, "unable to parse '%s' as a scale",
                                    arg);
                            break;
                          case 'o':
                            outline = arg;
                            break;
                          case 'b':
                            bitmap = arg;
                            break;
                        }
                    }
                }
            }
        } else {
            if (!infile)
                infile = p;
            else
                errx(1, "unexpected additional argument '%s'", p);
        }
    }

    if (!iw) errx(1, "expected an image width (-w option)");
    if (!ih) errx(1, "expected an image height (-h option)");
    if (!infile) errx(1, "expected an input file name");

    /*
     * Read the input map file.
     */
    short *inmap = new short[iw*ih];
    FILE *ifp = fopen(infile, "rb");
    if (!ifp)
        err(1, "%s: open", infile);
    for (int iy = 0; iy < ih; iy++)
        for (int ix = 0; ix < iw; ix++) {
            int hi = fgetc(ifp);
            if (hi < 0)
                errx(1, "EOF at (%d,%d) in map", ix, iy);
            int lo = fgetc(ifp);
            if (lo < 0)
                errx(1, "EOF half way through (%d,%d) in map", ix, iy);
            long val = hi * 256 + lo;
            val -= (val & 0x8000L) << 1;
            inmap[iy*iw+ix] = val;
        }
    fclose(ifp);

    Bitmap bm;
    bm.w = iw;
    bm.h = ih;
    bm.H = height;
    bm.pixels = inmap;

    if (bitmap) {
        FILE *outfp = fopen(bitmap, "wb");
        if (outfp == NULL)
            err(1, "%s: open", bitmap);

        png_structp png = png_create_write_struct(PNG_LIBPNG_VER_STRING,
                                                  NULL, NULL, NULL);
        assert(png);
        png_infop info = png_create_info_struct(png);
        assert(info);
        if (setjmp(png_jmpbuf(png)))
            assert(!"Something horrible happened in the PNG");

        png_byte **rows = new png_byte *[ih];
        for (int y = 0; y < ih; y++) {
            rows[y] = new png_byte[(iw+7)/8];
            for (int byte = 0; byte < (iw+7)/8; byte++)
                rows[y][byte] = 0;

            for (int x = 0; x < iw; x++) {
                int pixval = !bm.getpixel(x, ih-1-y); // reflip and set 0=w 1=b
                int byte = x/8;
                if (pixval)
                    rows[y][byte] |= 0x80 >> (x % 8);
            }
        }

        png_init_io(png, outfp);
        png_set_IHDR(png, info, iw, ih, 1, 0, PNG_INTERLACE_NONE,
                     PNG_COMPRESSION_TYPE_BASE, PNG_FILTER_TYPE_BASE);
        png_set_rows(png, info, rows);
        png_write_png(png, info, PNG_TRANSFORM_IDENTITY, NULL);

        for (int y = 0; y < ih; y++)
            delete[] rows[y];
        delete[] rows;
    }

    if (outline) {
        Trace<Bitmap> tr(fillratio, convex);
        tr.trace(bm);

        FILE *outfp = fopen(outline, "w");
        if (outfp == NULL)
            err(1, "%s: open", outline);

        for (Trace<Bitmap>::Cycle c: tr.cycles) {
            fprintf(outfp, "loop %d\n", (int)c.size());
            for (Trace<Bitmap>::Point &p: c) {
                bigrat x = p.x * scale, y = p.y * scale;
                char *xstr = x.stringify();
                char *ystr = y.stringify();
                fprintf(outfp, "point %s %s\n", xstr, ystr);
                delete[] xstr;
                delete[] ystr;
            }
            fprintf(outfp, "endloop\n");
        }

        fclose(outfp);
    }

    return 0;
}
