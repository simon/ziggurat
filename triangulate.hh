/*
 * triangulate.hh: a C++ implementation of an algorithm to triangulate
 * a planar graph.
 *
 * Given a collection of points in the plane, and an existing planar
 * graph on those points (i.e. a collection of edges between the
 * points, no two of which cross), we construct a planar graph on the
 * same set of points which is a full triangulation (every face has
 * degree 3 except the exterior) and which contains the original graph
 * as a subgraph.
 */

#ifndef TRIANGULATE_HH
#define TRIANGULATE_HH

#include <set>
#include <map>
#include <vector>
#include <algorithm>
#include <assert.h>
#ifdef TRIANGULATE_DIAGNOSTICS
#include <stdio.h>
#endif

namespace Ziggurat {
namespace ZigguratInternal {
#if 0
} // mess with editors' indenting
} // mess with editors' indenting
#endif

using std::set;
using std::map;
using std::vector;
using std::pair;
using std::max;

/*
 * The Triangulation class is parametrised by a type Graph, and
 * expects that type to present the following API:

class Graph {
    class Number {
        // Number must be an ordered arithmetic type: it must support
        // the + - * operators and comparisons, and also permit
        // initialisation to, assignment from, and comparison with
        // integers. Equality tests will be expected to be exactly
        // accurate, in that two arithmetic expressions whose ideal
        // mathematical results ought to be equal will be expected to
        // actually compare equal.
    };

    class Vertex {
        Number x() const;
        Number y() const;
    };

    // Both these functions are given an empty std::vector and
    // expected to populate it with (respectively) the full list of
    // vertices of the graph and the list of vertices connected to v
    // by an edge. It doesn't matter whether enum_neighbours returns v
    // as a neighbour of itself (either consistency or
    // intermittently); the self-edge will be ignored if it does.
    void enum_vertices(std::vector<const Vertex *> &out) const;
    void enum_neighbours(const Vertex *v, std::vector<const Vertex *> &out) const;
};

 * You perform a triangulation of a graph by means of instantiating a
 * Triangulation class and passing the graph as a constructor
 * parameter; after that, you can query the resulting object to get
 * its collection of edges and faces and also list the edges in order
 * round each vertex.
 */
template<class Graph>
class Triangulation {
    typedef typename Graph::Vertex Vertex;
    typedef typename Graph::Number Number;

    struct Point {
        Number x, y;
        Point(Number ax, Number ay) : x(ax), y(ay) {}
        Point(const Vertex *v) : x(v->x()), y(v->y()) {}
        Point(const Point &p) : x(p.x), y(p.y) {}
        Point operator=(const Point &p) { x = p.x; y = p.y; }
        Point operator-(const Point &a) const {
            return Point(x-a.x, y-a.y);
        }
        Point operator+(const Point &a) const {
            return Point(x+a.x, y+a.y);
        }
        Number dot(const Point &a) const {
            return x * a.x + y * a.y;
        }
        Point rot90() const {
            return Point(-y, x);
        }
        bool operator<(const Point &b) const {
            if (y != b.y)
                return y < b.y;
            else
                return x < b.x;
        }
        bool operator==(const Point &b) const {
            return x == b.x && y == b.y;
        }
        int cyclic_class(Number *n, Number *d) {
            if (y > 0) {
                *n = x;
                *d = y;
                return 1;
            } else if (y < 0) {
                *n = -x;
                *d = -y;
                return 3;
            } else {
                *n = *d = 1;
                assert(x != 0);
                if (x < 0)
                    return 0;
                else
                    return 2;
            }
        }
    };

    static bool vertexcmp(const Vertex *a, const Vertex *b) {
        return Point(a) < Point(b);
    }

    struct Edge {
        const Vertex *a, *b;
        Edge(const Vertex *aa, const Vertex *bb) : a(aa), b(bb) {}
        static bool vless(const Vertex *a, const Vertex *b) {
            if (a == NULL || b == NULL)
                return (a != NULL) < (b != NULL);
            return Point(a) < Point(b);
        }
        bool operator<(const Edge &that) const {
            if (this->a == that.a)
                return vless(this->b, that.b);
            else
                return vless(this->a, that.a);
        }
        bool operator==(const Edge &that) const {
            return this->a == that.a && this->b == that.b;
        }
    };

    typedef set<Edge> edgeset;
    edgeset our_edges;

    struct ListNode {
        ListNode *left;
        const Vertex *v;
        ListNode *right;
        ListNode(ListNode *al, const Vertex *av, ListNode *ar)
            : left(al), v(av), right(ar) {
#ifdef TRIANGULATE_DIAGNOSTICS
            fprintf(stderr, "  new ListNode %p = [%p, ", this, left);
            if (av)
                fprintf(stderr, "(%g,%g)", (double)v->x(), (double)v->y());
            fprintf(stderr, ", %p]\n", right);
#endif
        }
        ListNode(const ListNode *n)
            : left(n->left), v(n->v), right(n->right) {
#ifdef TRIANGULATE_DIAGNOSTICS
            fprintf(stderr, "  new ListNode %p cloned %p\n", this, n);
#endif
        }
#ifdef TRIANGULATE_DIAGNOSTICS
        ~ListNode() {
            fprintf(stderr, "  delete ListNode %p\n", this);
        }
#endif
        static ListNode *dup(ListNode *n) {
            if (!n)
                return NULL;
            else
                return ListNode(n->left, n->v, n->right);
        }
    };

    struct Line {
        const Vertex *base, *top;
        Line() : base(NULL), top(NULL) { }
        Line(const Vertex *abase, const Vertex *atop) : base(abase), top(atop) { }

        inline Number dx() const { return top->x() - base->x(); }
        inline Number dy() const { return top->y() - base->y(); }

        // This function assumes both lines are non-null
        bool operator<(const Line &b) const {
            // We test whether one line segment is to the left of
            // another, assuming the precondition that there is some
            // y-coordinate overlapped by both of them.
            //
            // The obvious approach is to pick a y-coordinate and find
            // the two line segments' intersections with that
            // coordinate. But this fails if the line segments meet at
            // that y-coordinate, because their base or top points
            // coincide in some way. So first detect those cases and
            // do something else:
            if (base == b.base) {
                // Just look at the slopes of the two lines.
                return dx() * b.dy() < b.dx() * dy();
            }
            if (top == b.top) {
                // Ditto, but in the other direction.
                return dx() * b.dy() > b.dx() * dy();
            }
            if (base == b.top)
                return false;          // they're entirely before us
            if (top == b.base)
                return true;           // we're entirely before them

            // This must be an appropriate y-coordinate.
            Number ytest = max(base->y(), b.base->y());

            // If we had reliable division in our number type, this
            // would simply be a case of working out the x-coordinate
            // at which each of the two lines intersects that y
            // position, and comparing those. That's still basically
            // what we're going to do, but in the absence of division
            // we have to compute each x-coordinate as a numerator and
            // denominator, and instead of writing an/ad < bn/bd we
            // must write an*bd < bn*ad.
            //
            // (Since we always expect top to be above base, we can be
            // confident that the denominators will be conveniently
            // positive.)
            Number ad = dy();
            if (ad == 0) ad = 1; // cope with horizontal vectors
            Number an = base->x() * ad + (ytest - base->y()) * dx();

            Number bd = b.dy();
            if (bd == 0) bd = 1;
            Number bn = b.base->x() * bd + (ytest - b.base->y()) * b.dx();

            return an * bd < bn * ad;
        }
    };

    struct Interval {
        ListNode *highest;
        Line left, right;

        Interval(ListNode *ah, const Line &aleft, const Line &aright)
            : highest(ah), left(aleft), right(aright) {}

#ifdef TRIANGULATE_DIAGNOSTICS
        void dump() const {
            fprintf(stderr, "    left: ");
            if (left.base == NULL)
                fprintf(stderr, "null");
            else
                fprintf(stderr, "(%g,%g) -> (%g,%g)",
                        (double)left.base->x(), (double)left.base->y(),
                        (double)left.top->x(), (double)left.top->y());
            fprintf(stderr, "; highest: ");
            if (highest == NULL)
                fprintf(stderr, "null");
            else
                fprintf(stderr, "(%g,%g)",
                        (double)highest->v->x(), (double)highest->v->y());
            fprintf(stderr, "; right: ");
            if (right.base == NULL)
                fprintf(stderr, "null\n");
            else
                fprintf(stderr, "(%g,%g) -> (%g,%g)\n",
                        (double)right.base->x(), (double)right.base->y(),
                        (double)right.top->x(), (double)right.top->y());
        }
#endif

        bool operator<(const Interval &b) const {
            // We compare only the right edges of the interval, so
            // that we can also find the interval containing a given
            // point by constructing a fake Interval from that point.
            if (right.base && !b.right.base)
                return true;           // they're the rightmost and we're not
            if (!right.base && b.right.base)
                return false;          // we're the rightmost and they're not
            if (!right.base)
                return false; // both we and they are the rightmost, so equal

            // Now we know neither right nor b.right contains null
            // pointers, so we can use operator<() above.
            return right < b.right;
        }
    };

    void spread_to(const Vertex *v, const Interval &interval,
                   ListNode **leftp, ListNode **rightp) {
        ListNode *left, *right;

#ifdef TRIANGULATE_DIAGNOSTICS
            fprintf(stderr, "  Spreading (%g,%g) over interval:\n",
                    (double)v->x(), (double)v->y());
            interval.dump();
#endif

        if (!interval.highest) {
            // special case: nothing to do
#ifdef TRIANGULATE_DIAGNOSTICS
            fprintf(stderr, "  spread_to: nothing to do!\n");
#endif
            *leftp = *rightp = NULL;
            return;
        }

        connect(v, interval.highest->v);

        left = interval.highest;
        right = new ListNode(left);

        while (left->v != interval.left.base && left->left) {
            Point ab = Point(left->v) - Point(v);
            Point bc = Point(left->left->v) - Point(left->v);
            if (ab.rot90().dot(bc) >= 0) {
#ifdef TRIANGULATE_DIAGNOSTICS
                fprintf(stderr, "  spread_to: stopping on left before (%g,%g)\n",
                        (double)left->left->v->x(), (double)left->left->v->y());
#endif
                break;
            }
            ListNode *tmp = left->left;
            delete left;
            left = tmp;
            connect(v, left->v);
        }

        while (right->v != interval.right.base && right->right) {
            Point ab = Point(right->v) - Point(v);
            Point bc = Point(right->right->v) - Point(right->v);
            if (ab.rot90().dot(bc) <= 0) {
#ifdef TRIANGULATE_DIAGNOSTICS
                fprintf(stderr, "  spread_to: stopping on right before (%g,%g)\n",
                        (double)right->right->v->x(), (double)right->right->v->y());
#endif
                break;
            }
            ListNode *tmp = right->right;
            delete right;
            right = tmp;
            connect(v, right->v);
        }

#ifdef TRIANGULATE_DIAGNOSTICS
        fprintf(stderr, "  spread_to: finished with left=");
        if (left == NULL)
            fprintf(stderr, "null");
        else
            fprintf(stderr, "(%g,%g)", (double)left->v->x(), (double)left->v->y());
        fprintf(stderr, " right=");
        if (left == NULL)
            fprintf(stderr, "null\n");
        else
            fprintf(stderr, "(%g,%g)\n", (double)right->v->x(), (double)right->v->y());
#endif
        *leftp = left;
        *rightp = right;
    }

    void connect(const Vertex *a, const Vertex *b) {
#ifdef TRIANGULATE_DIAGNOSTICS
        fprintf(stderr, "-- connect: (%g,%g) - (%g,%g)\n",
                (double)a->x(), (double)a->y(), (double)b->x(), (double)b->y());
#endif
        our_edges.insert(Edge(a,b));
        our_edges.insert(Edge(b,a));
    }

    map<Point, vector<const Vertex *> > neighbourlists;
    struct cyclicvertexcmp {
        const Vertex *centre;
        cyclicvertexcmp(const Vertex *acentre) : centre(acentre) {}
        bool operator()(const Vertex *a, const Vertex *b) const {
            Point ap = Point(a) - Point(centre);
            Point bp = Point(b) - Point(centre);
            Number an, ad, bn, bd;
            int aclass, bclass;

            aclass = ap.cyclic_class(&an, &ad);
            bclass = bp.cyclic_class(&bn, &bd);
            if (aclass != bclass)
                return aclass < bclass;
            else
                return an * bd < bn * ad;
        }
    };

  public:
    Triangulation(const Graph &G) {
        /*
         * Start by sorting the vertices into order, primarily by
         * y-coordinate and then by x.
         */
        vector<const Vertex *> vertices;
        G.enum_vertices(vertices);
        sort(vertices.begin(), vertices.end(), vertexcmp);

        /*
         * Keep a list of intervals of the frontier, initially
         * populated with one interval containing nothing.
         */
        set<Interval> intervals;
        intervals.insert(Interval(NULL, Line(NULL, NULL), Line(NULL, NULL)));

        /*
         * Now iterate over those points.
         */
        for (const Vertex *v: vertices) {
#ifdef TRIANGULATE_DIAGNOSTICS
            fprintf(stderr, "Processing vertex (%g,%g)\n",
                    (double)v->x(), (double)v->y());
            fprintf(stderr, "  Current list of intervals:\n");
            for (typename set<Interval>::iterator p = intervals.begin();
                 p != intervals.end(); p++)
                p->dump();
#endif

            /*
             * Determine which interval(s) the new vertex v falls in.
             * It may fall in two, if it actually is the endpoint of
             * one of our upward edges.
             */
            typename set<Interval>::iterator start =
                intervals.lower_bound(Interval(NULL, Line(v,v), Line(v,v)));
            assert(start != intervals.end());
            typename set<Interval>::iterator end = start;
            Line left_edge = start->left;
            ListNode *left, *right;

            spread_to(v, *end, &left, &right);
            while (v == end->right.top) {
                assert(right->v == end->right.base);
                delete right;
                ++end;
                ListNode *newleft;
                spread_to(v, *end, &newleft, &right);
                assert(newleft->v == end->left.base);
                delete newleft;
            }
            Line right_edge = end->right;
            ++end;
            intervals.erase(start, end);

            /*
             * Now construct a series of new intervals to put back on
             * our list. There are n+1 of them, where n is the number
             * of upward edges from v. Each one contains a ListNode of
             * v as its highest point, and none of them contains any
             * other points except that the leftmost points to 'left'
             * and the rightmost to 'right'.
             */
            set<Line> newedges;
            vector<const Vertex *> neighbours_of_v;
            G.enum_neighbours(v, neighbours_of_v);
            for (const Vertex *v2: neighbours_of_v) {
                // Only consider upward edges
                if (Point(v) < Point(v2)) {
                    newedges.insert(Line(v, v2));
                    // Ensure we have all the original edges as well
                    // as new ones we invent.
                    connect(v, v2);
                }
            }

            Line left_curr, right_curr;
            left_curr = left_edge;
            for (const Line &line: newedges) {
                right_curr = line;
                Interval newint(new ListNode(left,v,NULL), left_curr, right_curr);
#ifdef TRIANGULATE_DIAGNOSTICS
                fprintf(stderr, "  Adding new interval:\n");
                newint.dump();
#endif
                auto ret = intervals.insert(newint);
                assert(ret.second);
                left_curr = right_curr;
                left = NULL;
            }
            right_curr = right_edge;
            Interval newint(new ListNode(left,v,right), left_curr, right_curr);
#ifdef TRIANGULATE_DIAGNOSTICS
            fprintf(stderr, "  Adding new interval:\n");
            newint.dump();
#endif
            auto ret = intervals.insert(newint);
            assert(ret.second);
        }

        /*
         * Cleanup: free the linked lists in all remaining intervals.
         */
        for (const Interval &interval: intervals) {
            ListNode *left = interval.highest;
            if (left) {
                ListNode *right = left->right;
                while (left) {
                    ListNode *next = left->left;
                    delete left;
                    left = next;
                }
                while (right) {
                    ListNode *next = right->right;
                    delete right;
                    right = next;
                }
            }
        }

        /*
         * Now generate the abstract planar embedding of the graph, by
         * which I mean a cyclically sorted list of the neighbours of
         * each vertex.
         */
        for (const Vertex *v: vertices) {
            vector<const Vertex *> neighbours;

            auto eit = our_edges.lower_bound(Edge(v, NULL));
            while (eit != our_edges.end() && eit->a == v) {
                neighbours.push_back(eit->b);
                ++eit;
            }

            sort(neighbours.begin(), neighbours.end(), cyclicvertexcmp(v));
            neighbourlists[Point(v)] = neighbours;

#ifdef TRIANGULATE_DIAGNOSTICS
            fprintf(stderr, "  Ordered neighbours of point (%g,%g):\n",
                    (double)v->x(), (double)v->y());
            for (const Vertex *nv: neighbours)
                fprintf(stderr, "    (%g,%g)\n",
                        (double)nv->x(), (double)nv->y());
#endif
        }

        /*
         * And finally, go through those neighbour lists and build up
         * the list of actual triangles.
         */
        for (const Vertex *v: vertices) {
            vector<const Vertex *> &neighbours = neighbourlists[Point(v)];
            const Vertex *prev = neighbours[neighbours.size() - 1];
            for (const Vertex *curr: neighbours) {
                if (Point(v) < curr && Point(v) < prev &&
                    adjacent(prev, curr) &&
                    (Point(v)-Point(prev)).rot90().dot(Point(curr)-Point(v))>0)
                {
#ifdef TRIANGULATE_DIAGNOSTICS
                    fprintf(stderr,
                            "  Creating triangle: (%g,%g) (%g,%g) (%g,%g)\n",
                            (double)v->x(), (double)v->y(),
                            (double)prev->x(), (double)prev->y(),
                            (double)curr->x(), (double)curr->y());
#endif
                    triangles.push_back(Triangle(v, prev, curr));
                }
                prev = curr;
            }
        }
    }

    bool adjacent(const Vertex *a, const Vertex *b) const {
        return (our_edges.find(Edge(a,b)) != our_edges.end());
    }

    vector<const Vertex *> neighbours(const Vertex *v) const {
        return neighbourlists.find(Point(v))->second;
    }

    struct Triangle {
        const Vertex *v[3];
        Triangle(const Vertex *a, const Vertex *b, const Vertex *c) {
            v[0] = a;
            v[1] = b;
            v[2] = c;
        }
    };
    vector<Triangle> triangles;
};

#if 0
{ // mess with editors' indenting
{ // mess with editors' indenting
#endif
} // close namespace ZigguratInternal
using ZigguratInternal::Triangulation;
} // close namespace Ziggurat

#ifdef TRIANGULATE_TEST

#include <stdio.h>
#include <string.h>
#include <math.h>
#define TAU (8*atan(1.0))

using std::min;
using std::max;
using std::vector;

/*
g++ -std=c++11 -Wall -Werror -W -g -O0 -DTRIANGULATE_TEST -o triangulate -x c++ triangulate.hh -lm
*/

using Ziggurat::Triangulation;

#define MAXVERTS 200

int pages = 0;

struct Graph {
    typedef int Number;

    struct Vertex {
        Number xc, yc;
        Number x() const { return xc; }
        Number y() const { return yc; }
    };

    Vertex verts[MAXVERTS];
    int nverts;
    bool adjacency[MAXVERTS][MAXVERTS];

    void enum_vertices(vector<const Vertex *> &out) const {
        for (int i = 0; i < nverts; i++)
            out.push_back(&verts[i]);
    }

    void enum_neighbours(const Vertex *v, vector<const Vertex *> &out) const {
        int vindex = v - (const Vertex *)verts;
        for (int i = 0; i < nverts; i++)
            if (adjacency[vindex][i])
                out.push_back(&verts[i]);
    }

    Graph() {
        nverts = 0;
        for (int i = 0; i < MAXVERTS; i++)
            for (int j = 0; j < MAXVERTS; j++)
                adjacency[i][j] = false;
    }

    Vertex *add_vertex(Number x, Number y) {
        assert(nverts < MAXVERTS);
        verts[nverts].xc = x;
        verts[nverts].yc = y;
        return &verts[nverts++];
    }

    void add_edge(Vertex *v, Vertex *w) {
        int vindex = v - verts, windex = w - verts;
        adjacency[vindex][windex] = adjacency[windex][vindex] = true;
    }

    void runtest() {
        Triangulation<Graph> T(*this);

        ++pages;
        printf("%%%%Page: %d %d\n", pages, pages);

        int xmin, xmax, ymin, ymax;
        xmin = xmax = verts[0].xc;
        ymin = ymax = verts[0].yc;
        for (int i = 1; i < nverts; i++) {
            xmin = min(xmin, verts[i].xc);
            xmax = max(xmax, verts[i].xc);
            ymin = min(ymin, verts[i].yc);
            ymax = max(ymax, verts[i].yc);
        }
        printf("%d %d %d %d graphbbox\n", xmin, ymin, xmax, ymax);

        for (Triangulation<Graph>::Triangle &tr: T.triangles) {
            printf("%d %d %d %d %d %d triangle\n",
                   tr.v[0]->x(), tr.v[0]->y(),
                   tr.v[1]->x(), tr.v[1]->y(),
                   tr.v[2]->x(), tr.v[2]->y());
        }

        for (int i = 0; i < nverts; i++)
            for (int j = i+1; j < nverts; j++)
                if (adjacency[i][j])
                    printf("%d %d %d %d originaledge\n",
                           verts[i].xc, verts[i].yc,
                           verts[j].xc, verts[j].yc);
                else if (T.adjacent(&verts[i], &verts[j]))
                    printf("%d %d %d %d newedge\n",
                           verts[i].xc, verts[i].yc,
                           verts[j].xc, verts[j].yc);
        for (int i = 0; i < nverts; i++)
            printf("%d %d vertex\n", verts[i].xc, verts[i].yc);
        printf("showpage\n");
    }
};
typedef Graph::Vertex Vertex;

#define lenof(a) (sizeof((a))/sizeof(*(a)))

int main(int, char **)
{
    puts("%!PS-Adobe-1.0\n"
         "%%Pages: atend\n"
         "%%BoundingBox: 25 25 570 815\n"
         "%%EndComments\n"
         "%%BeginProlog\n"
         "/graphbbox {\n"
         "    /y1 exch def /x1 exch def /y0 exch def /x0 exch def\n"
         "    /xscale x1 x0 sub 495 exch div def\n"
         "    /yscale y1 y0 sub 740 exch div def\n"
         "    /scale xscale yscale min def\n"
         "    /xbase x1 x0 add scale mul 595 exch sub 2 div def\n"
         "    /ybase y1 y0 add scale mul 840 exch sub 2 div def\n"
         "} def\n"
         "/transform {\n"
         "    scale mul ybase add exch\n"
         "    scale mul xbase add exch\n"
         "} def\n"
         "/edge {\n"
         "    newpath transform moveto transform lineto 1 setlinewidth stroke\n"
         "} def\n"
         "/vertex {\n"
         "    newpath transform 3 0 360 arc 0 setgray fill\n"
         "} def\n"
         "/triangle {\n"
         "    newpath transform moveto transform lineto transform lineto\n"
         "    closepath gsave clip clippath 0.5 setgray fill\n"
         "    clippath 1 setgray 5 setlinewidth stroke grestore\n"
         "} def\n"
         "/newedge { 1 0 0 setrgbcolor edge } def\n"
         "/originaledge { 0 setgray edge } def\n"
         "%%EndProlog\n"
         "%%BeginSetup\n"
         "%%EndSetup");

    {
        Graph G;
        G.add_vertex(10, 10);
        G.add_vertex(20, 10);
        G.add_vertex(10, 20);
        G.add_vertex(20, 20);
        G.runtest();
    }

    {
        Graph G;
        Vertex *a[7], *b[17];
        for (unsigned i = 0; i < lenof(a); i++)
            a[i] = G.add_vertex((int)(100.5 + 10 * cos(TAU * i / lenof(a))),
                                (int)(100.5 + 10 * sin(TAU * i / lenof(a))));
        for (unsigned i = 0; i < lenof(b); i++)
            b[i] = G.add_vertex((int)(100.5 + 50 * cos(TAU * i / lenof(b))),
                                (int)(100.5 + 50 * sin(TAU * i / lenof(b))));
        for (unsigned i = 0; i < lenof(a); i++)
            G.add_edge(a[i], a[(i+1) % lenof(a)]);
        for (unsigned i = 0; i < lenof(b); i++)
            G.add_edge(b[i], b[(i+1) % lenof(b)]);
        G.runtest();
    }

    {
        Graph G;
        Vertex *a[5];
        for (unsigned i = 0; i < lenof(a); i++)
            a[i] = G.add_vertex(i * 2, 0);
        for (unsigned i = 0; i < lenof(a)-1; i++)
            G.add_vertex(i * 2 + 1, 1);
        Vertex *v = G.add_vertex(lenof(a)-1, 2*lenof(a));
        for (unsigned i = 0; i < lenof(a); i++)
            G.add_edge(a[i], v);
        for (unsigned i = 0; i < lenof(a); i++)
            G.add_edge(v, G.add_vertex(i * 2, 4*lenof(a)));
        for (unsigned i = 0; i < lenof(a)-1; i++)
            G.add_vertex(i * 2 + 1, 4*lenof(a)-1);

        G.runtest();
    }

    {
        Graph G;
        Vertex *bottom;
        bottom = G.add_vertex(0,0);
        for (unsigned i = 1; i <= 3; i++)
            G.add_vertex(+i*1, i*10);
        G.add_edge(bottom, G.add_vertex(-20, 100));
        G.add_edge(bottom, G.add_vertex(+20, 100));
        G.runtest();
    }

    {
        Graph G;
        Vertex *bottom;
        bottom = G.add_vertex(0,0);
        for (unsigned i = 1; i <= 3; i++)
            G.add_vertex(-i*1, i*10);
        G.add_edge(bottom, G.add_vertex(-20, 100));
        G.add_edge(bottom, G.add_vertex(+20, 100));
        G.runtest();
    }

    {
        Graph G;
        Vertex *bottom;
        bottom = G.add_vertex(0,0);
        G.add_edge(bottom, G.add_vertex(-20, 100));
        G.add_edge(bottom, G.add_vertex(+20, 100));
        G.add_vertex(-1, 10);
        G.add_vertex(+1, 10);
        G.add_vertex(0, 20);
        G.runtest();
    }

    {
        Graph G;
        Vertex *bottom;
        bottom = G.add_vertex(0,0);
        G.add_edge(bottom, G.add_vertex(-20, 100));
        G.add_edge(bottom, G.add_vertex(+20, 100));
        G.add_vertex(-1, 10);
        G.add_vertex(+1, 10);
        G.add_vertex(0, 20);
        G.add_vertex(0, 50);
        G.runtest();
    }

    {
        Graph G;
        Vertex *bottom;
        bottom = G.add_vertex(0,0);
        G.add_edge(bottom, G.add_vertex(-20, 100));
        G.add_edge(bottom, G.add_vertex(+20, 100));
        G.add_vertex(-1, 10);
        G.add_vertex(+1, 10);
        G.add_vertex(-1, 20);
        G.add_vertex(+1, 20);
        G.add_vertex(0, 30);
        G.add_vertex(0, 50);
        G.runtest();
    }

    {
        Graph G;
        const int n = 10;
        Vertex *grid[n][n];
        for (int y = 0; y < n; y++)
            for (int x = 0; x < n; x++)
                grid[x][y] = G.add_vertex(x, y);
        for (int y = 0; y+1 < n; y++)
            for (int x = 0; x < n; x++)
                G.add_edge(grid[x][y], grid[x][y+1]);
        for (int y = 0; y < n; y++)
            for (int x = 0; x+1 < n; x++)
                G.add_edge(grid[x][y], grid[x+1][y]);
        G.runtest();
    }

    {
        Graph G;
        Vertex *v[20];
        for (int i = 0; i < 5; i++)
            v[i] = G.add_vertex((int)(100.5 + 10 * cos(TAU * i / 5)),
                                (int)(100.5 + 10 * sin(TAU * i / 5)));
        for (int i = 0; i < 10; i++)
            v[i+5] = G.add_vertex((int)(100.5 + 20 * cos(TAU * i / 10)),
                                  (int)(100.5 + 20 * sin(TAU * i / 10)));
        for (int i = 0; i < 5; i++)
            v[i+15] = G.add_vertex((int)(100.5 + 35 * cos(TAU * (2*i+1) / 10)),
                                   (int)(100.5 + 35 * sin(TAU * (2*i+1) / 10)));
        G.add_edge(v[0], v[1]);
        G.add_edge(v[2], v[9]);
        G.add_edge(v[3], v[11]);
        G.add_edge(v[4], v[13]);
        G.add_edge(v[5], v[6]);
        G.add_edge(v[7], v[8]);
        G.add_edge(v[10], v[17]);
        G.add_edge(v[12], v[18]);
        G.add_edge(v[14], v[19]);
        G.add_edge(v[15], v[16]);
        G.add_edge(v[0], v[5]);
        G.add_edge(v[1], v[2]);
        G.add_edge(v[3], v[4]);
        G.add_edge(v[6], v[7]);
        G.add_edge(v[8], v[16]);
        G.add_edge(v[9], v[10]);
        G.add_edge(v[11], v[12]);
        G.add_edge(v[13], v[14]);
        G.add_edge(v[15], v[19]);
        G.add_edge(v[17], v[18]);
        G.add_edge(v[0], v[4]);
        G.add_edge(v[1], v[7]);
        G.add_edge(v[2], v[3]);
        G.add_edge(v[5], v[14]);
        G.add_edge(v[6], v[15]);
        G.add_edge(v[8], v[9]);
        G.add_edge(v[10], v[11]);
        G.add_edge(v[12], v[13]);
        G.add_edge(v[16], v[17]);
        G.add_edge(v[18], v[19]);
        G.runtest();
    }

    {
        Graph G;
        Vertex *v[9][5];
        for (int y = 0; y < 5; y++)
            for (int x = 0; x < 9; x++)
                v[x][y] = NULL;
        for (int y = 0; y < 5; y++) {
            int start = abs(y-2);
            for (int x = start; x < 9-start; x += 2)
                v[x][y] = G.add_vertex(11*x, 19*y);
            for (int x = start; x+2 < 9-start; x += 2)
                G.add_edge(v[x][y], v[x+2][y]);
        }
        for (int y = 0; y < 2; y++) {
            int start = abs(y-2);
            for (int x = start; x < 9-start; x += 2) {
                G.add_edge(v[x][y], v[x-1][y+1]);
                G.add_edge(v[x][y], v[x+1][y+1]);
                G.add_edge(v[x][4-y], v[x-1][3-y]);
                G.add_edge(v[x][4-y], v[x+1][3-y]);
            }
        }
        G.runtest();
    }

    printf("%%%%Trailer\n"
           "%%%%Pages: %d\n"
           "%%%%EOF\n", pages);

    return 0;
}

#endif

#endif /* TRIANGULATE_HH */
