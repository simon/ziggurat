/*
 * Command-line tool to compute Voronoi diagrams using voronoi.hh.
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <limits.h>
#include <assert.h>
#include <math.h>
#include <errno.h>

#include <string>
#include <vector>
#include <algorithm>

#include <sys/stat.h>
#include <sys/types.h>
#include <err.h>

#include "merge.hh"
#include "numparse.hh"
#include "voronoi.hh"
#include "munge.hh"

using std::string;
using std::vector;
using std::min;
using std::max;

using Ziggurat::bigrat;
using Ziggurat::Voronoi;
using Ziggurat::numparse;
using namespace Ziggurat::Munge;

struct IdentityMatrix {
    Point operator*(const Point &p) const { return p; }
};

class IntersectionInsideTest {
  public:
    bool inside(const OurOutlineMerge &om, int ti) const {
        const Aux &aux = om.triangles[ti].aux;
        return aux[0] && aux[1];
    }
};

class NeverLoopReduce {
  public:
    bool reduce_loop(Loop &loop) const {
        return true;
    }
};

/*
 * Given the semi-infinite line segment starting at (ox,oy) and
 * heading in direction (dx,dy), determine which edge or vertex of the
 * rectangle [-s,+s]^2 it intersects first, and exactly where.
 *
 * Coordinates of the intersection are written into x,y. The return
 * value is in the range 0,...,7, where even numbers are edges and odd
 * numbers corners, and we go anticlockwise starting from the RH edge
 * at zero. (So 0,...,7 mean R,TR,T,TL,L,BL,B,BR respectively.)
 */
int get_border_intersection(bigrat ox, bigrat oy, bigrat dx, bigrat dy,
                            bigrat s, bigrat &x, bigrat &y)
{
    bool got_one = false;
    bigrat min_t, t;

    assert(-s <= ox);
    assert(ox <= s);
    assert(-s <= oy);
    assert(oy <= s);

    if (dx > 0) {
        t = (s - ox) / dx;
        if (!got_one || min_t > t)
            min_t = t, got_one = true;
    }
    if (dx < 0) {
        t = (-s - ox) / dx;
        if (!got_one || min_t > t)
            min_t = t, got_one = true;
    }
    if (dy > 0) {
        t = (s - oy) / dy;
        if (!got_one || min_t > t)
            min_t = t, got_one = true;
    }
    if (dy < 0) {
        t = (-s - oy) / dy;
        if (!got_one || min_t > t)
            min_t = t, got_one = true;
    }

    x = ox + min_t * dx;
    y = oy + min_t * dy;

    int xedge = (x == s ? 2 : x == -s ? 0 : 1);
    int yedge = (y == s ? 2 : y == -s ? 0 : 1);
    static const int indices[] = { 5, 6, 7, 4, -1, 0, 3, 2, 1 };
    int ret = indices[yedge*3 + xedge];
    assert(ret != -1);

    return ret;
}

bigrat bigabs(const bigrat &x) { return x < 0 ? -x : x; }

int main(int argc, char **argv)
{
    string outdir = "", outprefix = "", outsuffix = ".outline";
    vector<string> files;
    bool got_bbox = false;
    const char *boundary_outline = NULL;
    bigrat xlo, ylo, xhi, yhi;

    bool doing_opts = true;
    while (--argc > 0) {
        char *p = *++argv;
        if (doing_opts && p[0] == '-' && p[1]) {
            if (p[1] == '-') {
                if (!strcmp(p, "--")) {
                    doing_opts = false;
                } else if (!strcmp(p, "--help")) {
                    printf("usage:   voronoi <options> [<pointsfile>...]\n"
                           "options: -o OUTDIR       directory to put output outline files in\n"
                           "         -p OUTPREFIX    prefix to put on output outline file names\n"
                           "         -e EXTENSION    extension for output file names (default '.outline')\n"
                           "         -O, --outline BOUNDARY  outline file used to bound output outlines\n"
                           "         -B, --bounding-box X0,Y0,X1,Y1  rectangle to bound output outlines\n");
                    return 0;
                } else {
                    errx(1, "unrecognised option '%s'", p);
                }
            } else {
                char c;
                while (*p && (c = *++p) != '\0') {
                    switch (c) {
                      case 'o':
                      case 'p':
                      case 'e':
                      case 'O':
                      case 'B':
                        /*
                         * Options requiring an argument.
                         */
                        char *arg = p+1;
                        if (*arg)
                            p += strlen(p); // eat remainder of argv word
                        else if (--argc > 0)
                            arg = *++argv;
                        else
                            errx(1, "expected an argument to option '-%c'", c);

                        switch (c) {
                          case 'o':
                            outdir = string(arg) + "/";
                            break;
                          case 'p':
                            outprefix = arg;
                            break;
                          case 'e':
                            outsuffix = arg;
                            break;
                          case 'O':
                            boundary_outline = arg;
                            break;
                          case 'B':
                            {
                                vector<bigrat> bbox;
                                string origarg = arg;
                                for (char *p = arg; *p ;) {
                                    char c;
                                    char *q = p;
                                    bigrat x;

                                    while (*p && *p != ',') p++;
                                    c = *p;
                                    *p = '\0';

                                    if (!numparse(q, &x))
                                        errx(1, "unable to parse '%s' as a coordinate\n", q);
                                    bbox.push_back(x);

                                    if (c) p++;
                                }
                                if (bbox.size() != 4)
                                    errx(1, "unable to parse '%s' as the four coordinates of a bounding box",
                                         origarg.c_str());
                                xlo = min(bbox[0], bbox[2]);
                                xhi = max(bbox[0], bbox[2]);
                                ylo = min(bbox[1], bbox[3]);
                                yhi = max(bbox[1], bbox[3]);
                                got_bbox = true;
                            }
                            break;
                        }
                        break;
                    }
                }
            }
        } else {
            files.push_back(p);
        }
    }

    if (files.size() == 0)
        files.push_back("-");

    typedef Voronoi<bigrat, int> OurVoronoi;
    OurVoronoi voronoi;

    vector<string> pointnames;
    vector<bigrat> pointx, pointy;
    bigrat absmax = 0;

    for (int i = 0; i < files.size(); i++) {
        const char *fname = files[i].c_str();
        FILE *fp = files[i] == "-" ? stdin : fopen(fname, "r");
        if (!fp)
            err(1, "'%s': open", fname);

        string line;
        int lineno = 0;
        while ((lineno++, line = fgetstring(fp)) != "") {
            chomp(line);

            string xs, ys, name;

            int pi = pointnames.size();

            size_t firstspace = line.find(' ');
            if (firstspace == string::npos)
                errx(1, "%s:%d: unable to parse point specification '%s'",
                     fname, lineno, line.c_str());
            xs = line.substr(0, firstspace);

            size_t secondspace = line.find(' ', firstspace+1);
            if (secondspace == string::npos) {
                char buf[80];
                ys = line.substr(firstspace+1);
                snprintf(buf, sizeof(buf), "%d", pi);
                name = buf;
            } else {
                ys = line.substr(firstspace+1, secondspace - firstspace - 1);
                name = line.substr(secondspace+1);
            }

            bigrat x, y;
            if (!numparse(xs.c_str(), &x)) {
                errx(1, "%s:%d: unable to parse x-coordinate '%s'",
                     fname, lineno, xs.c_str());
            }
            if (!numparse(ys.c_str(), &y)) {
                errx(1, "%s:%d: unable to parse y-coordinate '%s'",
                     fname, lineno, ys.c_str());
            }

            absmax = max(absmax, bigabs(x));
            absmax = max(absmax, bigabs(y));

            voronoi.add_point(x, y, pi);
            pointnames.push_back(name);
            pointx.push_back(x);
            pointy.push_back(y);
        }

        if (fp != stdin)
            fclose(fp);
    }

    Outline boundary;

    if (boundary_outline) {
        read_outline(boundary, boundary_outline,
                     IdentityMatrix(), NeverLoopReduce());

        for (int j = 0; boundary.size() > j; j++) {
            const Loop &loop = boundary[j];
            for (int k = 0; loop.size() > k; k++) {
                const Point &p = loop[k];
                absmax = max(absmax, bigabs(p.x));
                absmax = max(absmax, bigabs(p.y));
            }
        }
    } else if (got_bbox) {
        boundary.push_back(Loop());
        Loop &loop = boundary[boundary.size()-1];
        loop.push_back(Point(xlo, ylo));        
        loop.push_back(Point(xlo, yhi));        
        loop.push_back(Point(xhi, yhi));        
        loop.push_back(Point(xhi, ylo));        
        absmax = max(absmax, bigabs(xlo));
        absmax = max(absmax, bigabs(xhi));
        absmax = max(absmax, bigabs(ylo));
        absmax = max(absmax, bigabs(yhi));
    } else {
        errx(1, "must specify one of a boundary outline and a bounding box");
    }

    voronoi.compute();

    if (outdir != "" && mkdir(outdir.c_str(), 0777) < 0 && errno != EEXIST)
        err(1, "%s: mkdir", outdir.c_str());

    for (int pi = 0; pi < (int)pointnames.size(); pi++) {
        OurOutlineMerge om;

        add_to_om(om, boundary, 0);

        Outline cell;
        cell.push_back(Loop());
        Loop &loop = cell[0];

        int first = voronoi.first_vertex(pi), ui = first;
        int u_border_component = -1, v_border_component = -1;
        do {
            int qi = voronoi.next_face(pi, ui);
            int vi = voronoi.next_vertex(pi, qi);

            bigrat x, y;

            if (ui == 0) {
                bigrat ox = (pointx[pi] + pointx[qi]) / 2U;
                bigrat oy = (pointy[pi] + pointy[qi]) / 2U;
                bigrat dx = pointy[qi] - pointy[pi];
                bigrat dy = pointx[pi] - pointx[qi];

                u_border_component = get_border_intersection
                    (ox, oy, dx, dy, absmax, x, y);
                loop.push_back(Point(x, y));
            }

            if (vi != 0) {
                voronoi.vertex_xy(vi, x, y);
                loop.push_back(Point(x, y));
            } else {
                bigrat ox = (pointx[pi] + pointx[qi]) / 2U;
                bigrat oy = (pointy[pi] + pointy[qi]) / 2U;
                bigrat dx = pointy[pi] - pointy[qi];
                bigrat dy = pointx[qi] - pointx[pi];

                v_border_component = get_border_intersection
                    (ox, oy, dx, dy, absmax, x, y);
                loop.push_back(Point(x, y));
            }

            ui = vi;
        } while (ui != first);

        if (u_border_component != -1) {
            assert(v_border_component != -1);

            for (int i = u_border_component + 7;
                 (i & 7) != v_border_component; i--) {
                if (i & 1) {
                    int i7 = i & 7;
                    Point p((i7 == 1 || i7 == 7) ? absmax : -absmax,
                            (i7 == 1 || i7 == 3) ? absmax : -absmax);
                    loop.push_back(p);
                }
            }
        }

        add_to_om(om, cell, 1);

        om.compute();

        Outline outline;
        om_result(outline, om, IntersectionInsideTest());

        string outname = outdir + outprefix + pointnames[pi] + outsuffix;
        write_outline(outname, outline);
    }

    return 0;
}
