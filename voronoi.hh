#include <assert.h>
#include <stdio.h>
#include <string.h>

#include <algorithm>
#include <set>
#include <map>
#include <vector>

#if defined TEST_VORONOI || defined TEST_VORONOI_DEBUG
#define TEST_VORONOI_DUMP // need these for diagnostics *and* unit tests
#endif
#if defined TEST_VORONOI
class Test;
#define TEST_VORONOI_INVARIANTS
#endif

namespace Ziggurat {
namespace ZigguratInternal {
#if 0
} // mess with editors' indenting
} // mess with editors' indenting
#endif

using std::set;
using std::map;
using std::vector;
using std::pair;
using std::min;

template<class Number, class Id> class Voronoi {

    // ------------------------------------------------------------------
    // Utility functions.

    static inline int ncmp(const Number &a, const Number &b) {
        return (a > b ? +1 : a < b ? -1 : 0);
    }
    static inline int nsign(const Number &a) {
        return ncmp(a, 0);
    }
    static inline int samesign(const Number &a, const Number &b) {
        // Loose sign test: the condition is that no two numbers have
        // strictly opposite signs. Equivalently, 0 is considered to
        // count as having both signs at once, and the question is
        // whether a,b have 'any' sign in common.
        return (a >= 0 ? b >= 0 : b <= 0);
    }

    // ------------------------------------------------------------------
    // Quadratic irrationals. Well, really, a quadratic extension of
    // whatever field the Number type represents - although I expect
    // I'll loosely imply that it's a rational on at least one more
    // occasion.

    struct QI {
        // A number of the form q + r*sqrt(rr).
        // Constraint: rr > 0.
        Number q, r, rr;

        QI() : q(0), r(0), rr(1) {}
        QI(const Number &aq) : q(aq), r(0), rr(1) {}
        QI(const Number &aq, const Number &ar, const Number &arr)
            : q(aq), r(ar), rr(arr) {}

        const Number &rational() const {
            assert(r == 0);
            return q;
        }

        int sign() const {
            return signtest2(q, 1, r, rr);
        }

        bool operator< (const QI &that) const { return cmp(that) <  0; }
        bool operator<=(const QI &that) const { return cmp(that) <= 0; }
        bool operator> (const QI &that) const { return cmp(that) >  0; }
        bool operator>=(const QI &that) const { return cmp(that) >= 0; }
        bool operator==(const QI &that) const { return cmp(that) == 0; }
        bool operator!=(const QI &that) const { return cmp(that) != 0; }

        inline int cmp(const QI &that) const {
            return signtest3(q - that.q, 1, r, rr, -that.r, that.rr);
        }

        // Evaluate a quadratic c0 + c1 x + c2 x^2, with x = this.
        // Return the result as a new QI with the same rr.
        QI quadratic(Number c0, Number c1, Number c2) const {
            return QI(c2 * (q*q + r*r*rr) + c1*q + c0, 2*c2*q*r + c1*r, rr);
        }

      private:

        static int signtest3(Number q, Number qr,
                             Number r, Number rr,
                             Number s, Number sr) {
            // Return the sign of q rt qr + r rt rr + s rt sr.

            // If q,r,s have consistent signs, trivial case.
            if (q == 0 && r == 0 && s == 0)
                return 0;
            if (q >= 0 && r >= 0 && s >= 0)
                return +1;
            if (q <= 0 && r <= 0 && s <= 0)
                return -1;

            // Otherwise, one of q,r,s has opposite sign to the others.
            // Rearrange so that q,r have same sign and s the opposite.
            if (!samesign(q, r)) {
                if (samesign(q, s)) {
                    Number tmp;
                    tmp = r; r = s; s = tmp;
                    tmp = rr; rr = sr; sr = tmp;
                } else { // in that case, samesign(r,s)
                    Number tmp;
                    tmp = q; q = s; s = tmp;
                    tmp = qr; qr = sr; sr = tmp;
                }
            }

            // Now we can reduce to testing the sign of
            //
            //    (q rt qr + r rt rr + s rt sr) (q rt qr + r rt rr - s rt sr)
            //  = (q rt qr + r rt rr)^2 - s^2 sr
            //  = q^2 qr + r^2 rr - s^2 sr - 2 q r rt(qr rr)
            //
            // because the second factor (q rt qr + r rt rr - s rt sr),
            // relating the thing we really wanted to the thing we're
            // reducing to, has an easily determined sign, namely the
            // common sign of q and r.
            return nsign(q+r-s) * signtest2(q*q*qr + r*r*rr - s*s*sr,
                                            1, 2*q*r, qr*rr);
        }

        static int signtest2(Number q, Number qr,
                             Number r, Number rr) {
            // Return the sign of q rt qr + r rt rr.

            // If q,r have consistent signs, trivial case.
            if (q == 0 && r == 0)
                return 0;
            if (q >= 0 && r >= 0)
                return +1;
            if (q <= 0 && r <= 0)
                return -1;

            // Otherwise we can reduce to testing the sign of
            //
            //    (q rt qr + r rt rr) (q rt qr - r rt rr)
            //  = q^2 qr - r^2 rr
            //
            // because the second factor (q rt qr - r rt rr), relating
            // the thing we really wanted to the thing we're reducing to,
            // has an easily determined sign, namely the sign of q.
            return nsign(q-r) * nsign(q*q*qr - r*r*rr);
        }

#ifdef TEST_VORONOI_DUMP
      public:
        char *stringify() const {
            char *qstr = q.stringify();
            if (r) {
                char *rstr = r.stringify();
                char *rrstr = rr.stringify();
                char *ret = new char[strlen(qstr) + strlen(rstr) +
                                     strlen(rrstr) + 40];
                sprintf(ret, "%s + %s sqrt(%s)", qstr, rstr, rrstr);
                delete[] rstr;
                delete[] rrstr;
                delete[] qstr;
                return ret;
            } else {
                return qstr;
            }
        }

        void dump() const {
            char *str = stringify();
            printf("%s", str);
            delete[] str;
        }
#endif
    };
    static inline int ncmp(const QI &a, const QI &b) { return a.cmp(b); }

    // ------------------------------------------------------------------
    // Point types, sorted by y and then x.

    template<class Num> struct Point {
        Num x, y;
        Point() : x(0), y(0) {}
        Point(const Num &ax, const Num &ay) : x(ax), y(ay) {}
        template<class OtherNum> Point(const Point<OtherNum> &p) :
            x(p.x), y(p.y) {}

        bool operator< (const Point &that) const { return cmp(that) <  0; }
        bool operator<=(const Point &that) const { return cmp(that) <= 0; }
        bool operator> (const Point &that) const { return cmp(that) >  0; }
        bool operator>=(const Point &that) const { return cmp(that) >= 0; }
        bool operator==(const Point &that) const { return cmp(that) == 0; }
        bool operator!=(const Point &that) const { return cmp(that) != 0; }

        inline int cmp(const Point &that) const {
            if (y != that.y)
                return ncmp(y, that.y);
            else
                return ncmp(x, that.x);
        }

#ifdef TEST_VORONOI_DUMP
        void dump() const {
            char *xstr = x.stringify();
            char *ystr = y.stringify();
            printf("(%s,%s)", xstr, ystr);
            delete[] xstr;
            delete[] ystr;
        }
#endif
    };
    typedef Point<Number> RPoint;
    typedef Point<QI> QPoint;

    struct RPointOpt {
        // A type which can either represent an RPoint or an absence
        // of one. Absent points are used to represent 'the point at
        // infinity' in the context of endpoints of the left- and
        // right-most x-axis intervals during the algorithm's
        // progress, and also in the output representation when a
        // Voronoi edge extends without bound in some direction. They
        // also come up during the main algorithm, in which there may
        // or may not be a new point being added at any given
        // iteration.
        bool absent;
        RPoint p;

        RPointOpt() : absent(true) {}
        RPointOpt(const RPoint &ap) : absent(false), p(ap) {}
        RPointOpt(const RPointOpt &rpo) : absent(rpo.absent), p(rpo.p) {}
        const RPoint &operator*() const { return p; }
        operator RPoint() const {
            assert(!absent);
            return p;
        }
        bool operator==(const RPointOpt &that) const {
            return ((absent && that.absent) ||
                    (!absent && !that.absent && p == that.p));
        }
        bool operator!=(const RPointOpt &that) const {
            return !(*this == that);
        }
        bool operator<(const RPointOpt &that) const {
            return ((absent && !that.absent) ||
                    (!absent && !that.absent && p < that.p));
        }

#ifdef TEST_VORONOI_DUMP
        void dump() const {
            if (absent)
                printf("(none)");
            else
                p.dump();
        }
#endif
    };

    // ------------------------------------------------------------------
    // Further utility functions.

    static RPointOpt circumcentre(const RPoint &p0, const RPoint &p1,
                                  const RPoint &p2) {
        const Number &x0 = p0.x, &x1 = p1.x, &x2 = p2.x;
        const Number &y0 = p0.y, &y1 = p1.y, &y2 = p2.y;
        const Number d0 = x0*x0 + y0*y0;
        const Number d1 = x1*x1 + y1*y1;
        const Number d2 = x2*x2 + y2*y2;
        const Number denom = 2*(x0*y1 + x1*y2 + x2*y0 - x0*y2 - x2*y1 - x1*y0);
        const Number xnum = y1*d0 + y0*d2 + y2*d1 - y0*d1 - y1*d2 - y2*d0;
        const Number ynum = x0*d1 + x2*d0 + x1*d2 - x1*d0 - x2*d1 - x0*d2;
        if (denom == 0)
            return RPointOpt();
        else
            return RPoint(xnum / denom, ynum / denom);
    }

    // ------------------------------------------------------------------
    // An endpoint separating two intervals during the progress of the
    // sweep algorithm.

    struct Endpoint {
        RPointOpt p, q;

        Endpoint(const RPointOpt &ap, const RPointOpt &aq,
                 const QI *a_sweep_y)
            : p(ap), q(aq), current_sweep_y(a_sweep_y), epsilon(0) {
            assert(!p.absent || !q.absent);
        }

        Endpoint(const RPointOpt &ap, const RPointOpt &aq,
                 const QI *a_sweep_y, Number a_initial_x, int a_epsilon)
            : p(ap), q(aq), current_sweep_y(a_sweep_y),
              initial_x(a_initial_x), initial_sweep_y(*a_sweep_y),
              epsilon(a_epsilon) {
            assert(!p.absent || !q.absent);
        }

        bool operator< (const Endpoint &that) const { return cmp(that) <  0; }
        bool operator<=(const Endpoint &that) const { return cmp(that) <= 0; }
        bool operator> (const Endpoint &that) const { return cmp(that) >  0; }
        bool operator>=(const Endpoint &that) const { return cmp(that) >= 0; }
        bool operator==(const Endpoint &that) const { return cmp(that) == 0; }
        bool operator!=(const Endpoint &that) const { return cmp(that) != 0; }

        bool operator==(const Number &x) const {
            return cmp_num(x, *current_sweep_y) == 0;
        }

#ifdef TEST_VORONOI_DUMP
    void dump() const {
        p.dump();
        printf("-");
        q.dump();
    }
#endif

      private:
        const QI *current_sweep_y;

        Number initial_x;
        QI initial_sweep_y;
        int epsilon;

        // Compare against a plain Number. Returns sign(this - x)
        // given the current sweep y.
        int cmp_num(const Number &x, const QI &sweep_y) const {
            // Special cases.
            if (p.absent) {
                return -1;
            } else if (q.absent) {
                return +1;
            } else if (p.p.y == q.p.y) {
                // Special case: if p,q are horizontally separated, then
                // we can tell immediately that the x-coordinate of any
                // endpoint separating them is half way between.
                return nsign(p.p.x + q.p.x - 2*x);
            }

            // The parabola of equidistance between the fixed point
            // (X,Y) and the sweep line at y-coordinate S is given by
            //
            //               (y-S)^2 = (y-Y)^2 + (x-X)^2
            //  =>   y^2 - 2yS + S^2 = y^2 - 2yY + Y^2 + (x-X)^2
            //  =>       2 (Y - S) y = Y^2 + (x-X)^2 - S^2
            //
            //                         Y^2 + (x-X)^2 - S^2
            //  =>                 y = -------------------
            //                              2 (Y - S)
            //
            // If we have two of these parabolas, one for X,Y and one
            // for X',Y' (with S in common) and want to find where
            // they're equal, we must set their y-coordinates the same,
            // which gives us
            //
            // Y^2 + (x-X)^2 - S^2   Y'^2 + (x-X')^2 - S^2
            // ------------------- = -------------------
            //     2 (Y - S)              2 (Y' - S)
            //
            // which we can simplify into a quadratic in S:
            //
            // =>     S^2 (Y-Y')
            //      + S   (Y'^2 - Y^2 + (x-X')^2 - (x-X)^2)
            //      +     (Y'Y^2 + Y'(x-X)^2 - YY'^2 - Y(x-X')^2) = 0
            //
            // and this gives us a function of S and x which will tell
            // us which side of the boundary point x is on, when the
            // sweep line hits S.

            if (q.p.y > p.p.y) {
                if (q.p.x < x) {
                    return -1;
                }
            } else {
                if (p.p.x > x) {
                    return +1;
                }
            }

            // Common subexpressions.
            Number xmpx = (x-p.p.x), xmqx = (x-q.p.x);
            Number xmpx2 = xmpx * xmpx, xmqx2 = xmqx * xmqx;
            Number py2 = p.p.y * p.p.y, qy2 = q.p.y * q.p.y;

            // Coefficients of the quadratic derived above.
            Number c0 = q.p.y*py2 - p.p.y*qy2 + q.p.y*xmpx2 - p.p.y*xmqx2;
            Number c1 = qy2 - py2 + xmqx2 - xmpx2;
            Number c2 = p.p.y - q.p.y;

            return sweep_y.quadratic(c0, c1, c2).sign();
        }

        // Compare against another Endpoint. Returns sign(this - that).
        int cmp(const Endpoint &that) const {
            // Special cases, similar to above.
            if (p.absent || that.p.absent) {
                return p.absent && that.p.absent ? 0 : p.absent ? -1 : +1;
            } else if (q.absent || that.q.absent) {
                return q.absent && that.q.absent ? 0 : q.absent ? +1 : -1;
            } else if (p.p.y == q.p.y) {
                return -that.cmp_num((p.p.x + q.p.x) / Number(2),
                                     *current_sweep_y);
            } else if (that.p.p.y == that.q.p.y) {
                return cmp_num((that.p.p.x + that.q.p.x) / Number(2),
                               *current_sweep_y);
            } else if (p == that.q && that.p == q) {
                // If these are two endpoints of the same line, then
                // the higher point goes in between the two
                // occurrences of the lower point, i.e. the one whose
                // q is higher than p goes on the left.
                //
                // (It's not possible to have p.y == q.y in this
                // situation, because two horizontally separated
                // points wouldn't have spawned a double endpoint in
                // the first place.)
                if (p.p.y < q.p.y)
                    return -1;         // and that's us
                else
                    return +1;         // oh, no, it isn't
            }

            // Directly comparing two of the above quadratics against
            // each other would get painful. Fortunately, we don't
            // have to: each Endpoint is introduced to the world at a
            // particular sweep_y position, and when it is, it has a
            // rational x-coordinate. So we can mentally rewind
            // sweep_y to the higher of the two endpoints'
            // initial_sweep_y values (at which point the older
            // endpoint was surely already in existence, so will be
            // ordered the right way with respect to the newer one)
            // and evaluate just one quadratic against the other one's
            // rational coordinate.
            if (initial_sweep_y < that.initial_sweep_y) {
                return cmp_num(that.initial_x, that.initial_sweep_y);
            } else if (initial_sweep_y > that.initial_sweep_y) {
                return -that.cmp_num(initial_x, initial_sweep_y);
            } else if (initial_x != that.initial_x) {
                return ncmp(initial_x, that.initial_x);
            } else if (p == that.p && q == that.q) {
                return 0;              // these are actually the same endpoint
            } else {
                // Final tiebreaker: when two endpoints were
                // introduced at identical coordinates, we will have
                // known which one would turn out to be on the left,
                // and given them different epsilon values.
                return epsilon - that.epsilon;
            }
        }
    };

    // ------------------------------------------------------------------
    // Details of an upcoming intersection point, where two Endpoints
    // moving towards each other will collide.

    struct Intersection {
        QPoint sweep_pt;
        RPoint meeting_pt;

        Intersection(const QPoint &a_sweep, const RPoint &a_meeting)
            : sweep_pt(a_sweep), meeting_pt(a_meeting) {}

        bool operator< (const Intersection &that) const {return cmp(that)< 0;}
        bool operator<=(const Intersection &that) const {return cmp(that)<=0;}
        bool operator> (const Intersection &that) const {return cmp(that)> 0;}
        bool operator>=(const Intersection &that) const {return cmp(that)>=0;}
        bool operator==(const Intersection &that) const {return cmp(that)==0;}
        bool operator!=(const Intersection &that) const {return cmp(that)!=0;}

#ifdef TEST_VORONOI_DUMP
        void dump() const {
            sweep_pt.dump();
            printf("->");
            meeting_pt.dump();
        }
#endif

      private:
        inline int cmp(const Intersection &that) const {
            return sweep_pt.cmp(that.sweep_pt);
        }
    };

    // ------------------------------------------------------------------
    // State variables for the main algorithm.

    // The input points, sorted by RPoint's ordering (i.e. in the
    // right order to iterate over during the algorithm), and also
    // mapped to their client identifiers.
    map<RPoint, Id> points;

    // Current sweep location. sweep.y is the sweep line in general
    // (and is directly referred to by all the Endpoints we
    // construct), and sweep.x indicates the point on that line we're
    // currently processing.
    QPoint sweep;

    // The main algorithm state is a list of Endpoints dividing the
    // x-axis into intervals. The leftmost one has p absent; the
    // rightmost has q absent; no other endpoints have any absent
    // points at all.
    set<Endpoint> xaxis;

    // Queue of upcoming intersection points. Each one has a reference
    // count, indicating how many adjacent endpoint pairs refer to it,
    // so that we can delete them again if all references disappear.
    map<Intersection,int> intersections;

    // Reverse map from triples of points to intersections, so we can
    // find them to decrement their reference count when removing an
    // endpoint.
    typedef pair<RPoint, RPoint> RPoint2;
    typedef pair<RPoint2, RPoint> RPoint3;
    map<RPoint3, const Intersection *> intersection_finder;

    // Have we run compute() at all?
    bool computed;

    // Assign a numeric index to each Voronoi vertex, for output
    // purposes. 0 is always the point at infinity.
    map<RPointOpt, int> vertex_ids;
    vector<RPoint> vertices;      // indexed off by one
    int next_vertex_id;

    // Map used by the next_vertex() output method.
    //
    // output maps a pair of input points P,Q to the endpoint of its
    // boundary segment, if the boundary segment has an endpoint in
    // the direction you reach by walking half way from P to Q and
    // then turning left.
    //
    // If the boundary segment extends without bound in that
    // direction, then output[P,Q] is the point at infinity. If P,Q do
    // not have a Voronoi boundary anyway, then output[P,Q] does not
    // exist.
    map<pair<Id, Id>, int> next_vertex_map;

    // Map used by the next_face() output method, with complementary
    // semantics to next_vertex_map, i.e. for a fixed point id p and a
    // starting other point q, retrieving v=next_vertex_map[p,q] and
    // then q=next_face_map[p,v] will step round to the next edge of
    // p's Voronoi cell, and so on.
    map<pair<Id, int>, Id> next_face_map;

    void add_voronoi_edge(const RPoint &p, const RPoint &q) {
        const Id &pid = points[p], &qid = points[q];
        next_vertex_map[pair<Id, Id>(pid, qid)] = 0;
        next_vertex_map[pair<Id, Id>(qid, pid)] = 0;
    }

    void terminate_voronoi_edge(const RPoint &p, const RPoint &q,
                                const RPointOpt &vertex) {
        const Id &pid = points[p], &qid = points[q];
        int vid;

        auto id_it = vertex_ids.find(vertex);
        if (id_it == vertex_ids.end()) {
            vid = next_vertex_id++;
            vertex_ids[vertex] = vid;
            vertices.push_back(vertex);
        } else {
            vid = id_it->second;
        }

        if (vid != 0 && next_vertex_map[pair<Id, Id>(qid, pid)] == vid) {
            // This edge turns out to have the same (real) vertex at
            // both ends, i.e. it's of zero length. Remove it again
            // from the output.
            next_vertex_map.erase(pair<Id, Id>(qid, pid));
            next_vertex_map.erase(pair<Id, Id>(pid, qid));
        } else {
            next_vertex_map[pair<Id, Id>(pid, qid)] = vid;
        }
    }

    void build_next_face_map() {
        for (auto &kv: next_vertex_map) {
            // If next_vertex_map[p,q] = v, then next_face_map[q,v] = p.
            const Id &p = kv.first.first, &q = kv.first.second;
            int v = kv.second;
            next_face_map[pair<Id, int>(q, v)] = p;
        }
    }

  public:

    Voronoi() : computed(false), next_vertex_id(1) {
        vertex_ids[RPointOpt()] = 0;
    }

    void add_point(Number x, Number y, Id id) {
        assert(!computed);

        points[RPoint(x,y)] = id;
    }

    void compute() {
        assert(!computed);

        // Initial sanity check: there should be at least one point.
        // If not, there's no Voronoi graph.
        if (points.empty())
            return;

        // Initialise an iterator to step gradually through our points
        // list.
        auto nextpoint = points.begin();

        // Preinitialise xaxis with all the points sharing the first
        // y-coordinate we come to.
        //
        // This is partly to avoid having to handle the special case
        // of an 'endpoint' (-inf,+inf) with no possible comparison
        // value at all, and partly because if there's more than one
        // point at that first y-coordinate then the endpoints between
        // them are a special case - each is one end of a boundary ray
        // whose other end we _already know_ to extend vertically
        // downwards to infinity.
        //
        // (Infinite boundary rays in any direction other than
        // vertically downwards are not finalised until the very end
        // of the algorithm - for any other direction, it's still
        // possible until the last minute that a point sufficiently
        // far off to one side might show up and interfere with them.)
        {
            Number ymin = nextpoint->first.y;
            RPointOpt prev_p;          // starts off at -inf

            do {
                const RPoint &p = nextpoint->first;
                xaxis.insert(Endpoint(prev_p, p, &sweep.y));

                if (!prev_p.absent)
                    add_voronoi_edge(prev_p, p);

                prev_p = p;
                ++nextpoint;
            } while (nextpoint != points.end() && nextpoint->first.y == ymin);

            xaxis.insert(Endpoint(prev_p, RPointOpt(), &sweep.y));
        }

#ifdef TEST_VORONOI_DEBUG
        printf("initialised\n");
#endif

        // Main loop.
        while (nextpoint != points.end() || !intersections.empty()) {

#ifdef TEST_VORONOI_INVARIANTS
            // Verify the invariant that adjacent elements in xaxis
            // must have the left one's q equal to the right one's p,
            // and must have the absent 'infinity' point only at the
            // very ends.
            //
            // This blatantly breaks the O(N log N) running time of
            // the algorithm, so of course we only do it when
            // compiling the standalone unit test.
            {
                RPointOpt expected_p;
                bool first = true;
                for (const Endpoint &end: xaxis) {
                    if (!first)
                        assert(!expected_p.absent);
                    assert(end.p == expected_p);
                    expected_p = end.q;
                    first = false;
                }
                assert(expected_p.absent);
            }

            // Similarly, check the invariant that every element of
            // intersections must have a refcount equal to the number
            // of point triples that cite it in intersection_finder.
            {
                for (auto &kv: intersections) {
                    const Intersection *inter = &kv.first;
                    int expected_count = kv.second;
                    int actual_count = 0;

                    for (auto &kv2: intersection_finder) {
                        if (kv2.second == inter)
                            actual_count++;
                    }
                    assert(expected_count == actual_count);
                }

                // And, conversely, verify that everything in
                // intersection_finder appears at least once in
                // intersections.
                for (auto kv: intersection_finder) {
                    assert(intersections.find(*kv.second) !=
                           intersections.end());
                }
            }
#endif

            auto next_int = intersections.begin();

#ifdef TEST_VORONOI_DEBUG
            printf("nextpoint = ");
            if (nextpoint == points.end()) {
                printf("(none)\n");
            } else {
                nextpoint->first.dump();
                printf("\n");
            }
            dump_intersections();
            printf("\n");
#endif

            // Find the next interesting location.
            //
            // (I feel it _ought_ to be possible to write this more
            // neatly - I want a std::min that takes two potentially
            // absent arguments...)
            if (nextpoint != points.end() && next_int != intersections.end()) {
                sweep = min(QPoint(nextpoint->first),
                            next_int->first.sweep_pt);
            } else if (nextpoint != points.end()) {
                sweep = nextpoint->first;
            } else {
                sweep = next_int->first.sweep_pt;
            }

#ifdef TEST_VORONOI_DEBUG
            printf("new sweep = ");
            sweep.dump();
            printf(", ");
            dump_xaxis();
            printf("\n");
#endif

            // Find out whether we're adding a new input point to the
            // diagram, and if so, advance nextpoint.
            RPointOpt new_point;
            if (nextpoint != points.end() &&
                QPoint(nextpoint->first) == sweep) {
                new_point = nextpoint->first;
                ++nextpoint;

#ifdef TEST_VORONOI_DEBUG
                printf("  new point ");
                new_point.dump();
                printf("\n");
#endif
            } else {
#ifdef TEST_VORONOI_DEBUG
                printf("  no new point\n");
#endif
            }

            // Find the location on the x-axis corresponding to this
            // sweep position.
            typename set<Endpoint>::iterator xaxis_it;
            {
                // The C++ STL doesn't support heterogeneous
                // comparison. Ideally, it would just let me call
                // xaxis.lower_bound(foo) for some foo not of type
                // Endpoint, as long as I provided a method in
                // Endpoint to compare against whatever type foo
                // actually was.
                //
                // Failing that, we have to construct an actual
                // Endpoint to compare against. Fortunately, that's
                // not too hard, because if I just set both of its
                // points to be the same, then it will fall into the
                // special case of equal y-coordinates and always
                // evaluate to the same rational x.
                RPoint dummypoint(sweep.x.rational(), 0);
                Endpoint dummy(dummypoint, dummypoint, &sweep.y);
                xaxis_it = xaxis.lower_bound(dummy);
            }

            // Find out whether we're adding a Voronoi vertex to the
            // diagram. This will certainly not happen unless at least
            // one endpoint in xaxis is directly below the sweep
            // location; but it also needs either a second coincident
            // endpoint in xaxis, or else a new point having just
            // materialised here as well.
            RPointOpt voronoi_vertex;
            if (*xaxis_it == sweep.x.rational()) {
                auto next_xaxis_it = xaxis_it;
                ++next_xaxis_it;
                if (*next_xaxis_it == sweep.x.rational()) {
                    // If two existing endpoints meet here, then we
                    // must have anticipated this vertex and entered
                    // it in the 'intersections' set, so we don't need
                    // to recompute its location.
                    assert(next_int->first.sweep_pt == sweep);
                    voronoi_vertex = next_int->first.meeting_pt;
#ifdef TEST_VORONOI_DEBUG
                    printf("  expected Voronoi vertex ");
                    voronoi_vertex.dump();
                    printf("\n");
#endif
                } else if (!new_point.absent) {
                    // The late-breaking case, in which we know the vertex
                    // is somewhere directly below 'sweep', but haven't
                    // already calculated its y-coordinate. The easiest
                    // approach is to compute the circumcentre of the new
                    // point and the two points whose intervals have just
                    // met under it. (Or rather, any two of the points
                    // meeting under it - there could be more than two
                    // Endpoints coinciding, but that's OK, any two
                    // unequal points will do here.)
                    voronoi_vertex = circumcentre(xaxis_it->p, xaxis_it->q,
                                                  new_point.p);
#ifdef TEST_VORONOI_DEBUG
                    printf("  late-breaking Voronoi vertex ");
                    voronoi_vertex.dump();
                    printf("\n");
#endif
                }
            }

#ifdef TEST_VORONOI_DEBUG
            if (voronoi_vertex.absent)
                printf("  no Voronoi vertex\n");
#endif

            assert(!new_point.absent || !voronoi_vertex.absent);

            // All Endpoints in the current x-axis which are at this
            // exact x-position must now terminate at the new Voronoi
            // vertex.
            //
            // We keep count of how many there were, which we'll need
            // for some follow-up bookkeeping.
            int nterminated = 0;
            for (auto term_it = xaxis_it;
                 term_it != xaxis.end() && *term_it == sweep.x.rational();
                 ++term_it) {
                const Endpoint &end = *term_it;

                terminate_voronoi_edge(end.p, end.q, voronoi_vertex);
#ifdef TEST_VORONOI_DEBUG
                printf("  terminating edge ");
                end.p.dump();
                printf("-");
                end.q.dump();
                printf("\n");
#endif

                nterminated++;
            }

            // Now, for all the endpoints we just terminated,
            // decrement the reference counts on the intersections
            // between them and their neighbours in xaxis.
            {
                auto deref_it = xaxis_it;
                --deref_it;
                for (int k = 0; k < nterminated+1; k++) {
                    const RPointOpt &pl = deref_it->p;
                    ++deref_it;
                    const RPointOpt &pm = deref_it->p;
                    const RPointOpt &pr = deref_it->q;
#ifdef TEST_VORONOI_DEBUG
                    printf("  checking for intersection to dereference: ");
                    pl.dump();
                    printf(",");
                    pm.dump();
                    printf(",");
                    pr.dump();
                    printf("\n");
#endif
                    if (!pl.absent && !pm.absent && !pr.absent) {
                        auto if_it = intersection_finder.find
                            (RPoint3(RPoint2(pl, pm), pr));

                        if (if_it != intersection_finder.end()) {
                            const Intersection &inter = *if_it->second;
                            int new_refcount = --intersections[inter];
#ifdef TEST_VORONOI_DEBUG
                            printf("    refcount on ");
                            inter.dump();
                            printf(" -> %d\n", new_refcount);
#endif
                            if (new_refcount == 0)
                                intersections.erase(inter);
                            intersection_finder.erase(if_it);
                        }
                    }
                }
            }

            // Finally, delete the terminated endpoints from xaxis.
            while (nterminated-- > 0) {
#ifdef TEST_VORONOI_DEBUG
                const Endpoint &end = *xaxis_it;
                printf("  erasing endpoint ");
                end.p.dump();
                printf("-");
                end.q.dump();
                printf("\n");
#endif
                xaxis.erase(xaxis_it++);
            }

#ifdef TEST_VORONOI_DEBUG
            printf("  ");
            dump_xaxis();
            printf("\n");
            printf("  ");
            dump_intersections();
            printf("\n");
#endif

            // Start tracking the neighbourhood of the stuff we're
            // inserting into xaxis, so that we can make Intersection
            // entries for them in a moment.
            vector<Endpoint> local_ends;
            {
                auto tmp_it = xaxis_it;
                --tmp_it;
                local_ends.push_back(*(tmp_it));
            }

            // Now create new Endpoints. We'll always be creating
            // either one or two of them, depending on whether a new
            // point is turning up.
            {
                RPointOpt points[3];
                int n = 0;
                {
                    auto tmp_it = xaxis_it;
                    --tmp_it;
                    points[n++] = tmp_it->q;
                }
                if (!new_point.absent)
                    points[n++] = new_point;
                points[n++] = xaxis_it->p;

                assert(!(n == 2 && points[0] == points[1]));

                for (int i = 0; i+1 < n; i++) {
                    RPointOpt &p = points[i], &q = points[i+1];

                    Endpoint end(p, q, &sweep.y, sweep.x.rational(), i);

#ifdef TEST_VORONOI_DEBUG
                    printf("  new endpoint ");
                    end.dump();
#endif

                    if (!p.absent && !q.absent) {
                        // Add this edge to the output diagram, and in
                        // one direction, terminate it at the current
                        // Voronoi vertex, if any. The other direction
                        // has no terminus as yet.
                        add_voronoi_edge(p, q);
#ifdef TEST_VORONOI_DEBUG
                        printf(" and edge");
                        if (!voronoi_vertex.absent)
                            printf(" with terminus");
#endif
                        terminate_voronoi_edge(q, p, voronoi_vertex);
                    }
#ifdef TEST_VORONOI_DEBUG
                    printf("\n");
#endif

                    local_ends.push_back(end);
                    xaxis.insert(end);
                }
            }

            local_ends.push_back(*xaxis_it);

#ifdef TEST_VORONOI_DEBUG
            printf("  ");
            dump_xaxis();
            printf("\n");
#endif

            // Now we must potentially add an upcoming intersection on
            // each side of the endpoints we've just inserted.
            for (int i = 0; i+1 < (int)local_ends.size(); ++i) {
                Endpoint &e0 = local_ends[i], &e1 = local_ends[i+1];

#ifdef TEST_VORONOI_DEBUG
                printf("  intersection check: ");
                e0.dump();
                printf(" vs ");
                e1.dump();
                printf("\n");
#endif

                assert(e0.q == e1.p);  // basic sanity check
                RPointOpt &pl = e0.p, &pm = e0.q, &pr = e1.q;

                // There's a bewildering array of reasons why these
                // two endpoints might not have an intersection in
                // their future. Rule them out one by one.

                if (pl.absent || pr.absent) {
#ifdef TEST_VORONOI_DEBUG
                    printf("    no, one endpoint is infinity\n");
#endif
                    continue;
                }

                if (pl == pr) {
#ifdef TEST_VORONOI_DEBUG
                    printf("    no, opposite ends of the same edge\n");
#endif
                    continue;
                }

                RPointOpt vertex = circumcentre(pl, pm, pr);
                if (vertex.absent) {
#ifdef TEST_VORONOI_DEBUG
                    printf("    no, edges are parallel\n");
#endif
                    continue;
                }
#ifdef TEST_VORONOI_DEBUG
                printf("    circumcentre = ");
                vertex.dump();
                printf("\n");
#endif

                if ((pl.p.y >= pr.p.y && pl.p.x > vertex.p.x) ||
                    (pl.p.y <= pr.p.y && pr.p.x < vertex.p.x)) {
                    // This condition deals with the possibility that
                    // we might have just computed the meeting point
                    // of the two _other_ endpoints of the same
                    // Voronoi edge. (Recall that the area of
                    // influence of any two input points can meet in
                    // up to two separate places.) We rule this out by
                    // making sure we're on the correct side of the
                    // higher one of the two outlying points.
#ifdef TEST_VORONOI_DEBUG
                    printf("    no, this is where the other ends meet\n");
#endif
                    continue;
                }

                // We now know the point where these two endpoints
                // will meet, or have met. Now we need to work out
                // where the sweep line position will be when they
                // meet, which will be higher than the actual vertex -
                // specifically, we must add to vertex.y the distance
                // to one of the points pl,pm,pr. (Any one of them, of
                // course, because by construction all three distances
                // are equal.)
                //
                // For this, we must construct a quadratic irrational,
                // because the distance will in general involve a
                // square root.
                Number dx = vertex.p.x - pm.p.x, dy = vertex.p.y - pm.p.y;
                QPoint int_sweep(vertex.p.x, QI(vertex.p.y, 1, dx*dx + dy*dy));
#ifdef TEST_VORONOI_DEBUG
                printf("    sweep position would be ");
                int_sweep.dump();
                printf("\n");
#endif

                if (int_sweep <= sweep) {
#ifdef TEST_VORONOI_DEBUG
                    printf("    no, this intersection has already happened\n");
#endif
                    continue;
                }

                // Finally, we've finished ruling things out. Insert
                // this intersection.
                Intersection inter(int_sweep, vertex);
#ifdef TEST_VORONOI_DEBUG
                printf("    yes! new intersection = ");
                inter.dump();
                printf("\n");
#endif
                if (intersections.find(inter) == intersections.end())
                    intersections[inter] = 1;
                else
                    intersections[inter] += 1;
                intersection_finder[RPoint3(RPoint2(pl, pm), pr)] =
                    &intersections.find(inter)->first;
            }
        }

        // Main algorithm now done, so we can clean up our internal
        // storage.
        xaxis.clear();
        intersections.clear();
        intersection_finder.clear();

        // Postprocessing for a usable output format.
        build_next_face_map();

        computed = true;
    }

    // Methods for extracting the output.

  public:

    Number vertex_x(int n) {
        assert(n > 0);
        assert(n-1 < vertices.size());
        return vertices[n-1].x;
    }

    Number vertex_y(int n) {
        assert(n > 0);
        assert(n-1 < vertices.size());
        return vertices[n-1].y;
    }

    void vertex_xy(int n, Number &x, Number &y) {
        assert(n > 0);
        assert(n-1 < vertices.size());
        x = vertices[n-1].x;
        y = vertices[n-1].y;
    }

    inline int nvertices() {
        return next_vertex_id;
    }

    int first_vertex(Id p) {
        // Return the lowest-numbered vertex on the boundary of p's
        // Voronoi cell. In particular, it will return 0 (the infinity
        // vertex) if 0 is on the boundary.

        auto it = next_face_map.lower_bound(pair<Id, int>(p, 0));
        assert(it != next_face_map.end());
        return it->first.second;
    }

    int next_vertex(Id p, Id q) {
        // As you stand at p, gaze at the edge separating you from q,
        // and turn left, what is the next vertex you come to?
        //
        // Returns the index of that vertex, or < 0 if p,q do not
        // share a Voronoi edge at all.

        auto it = next_vertex_map.find(pair<Id, Id>(p, q));
        return it == next_vertex_map.end() ? -1 : it->second;
    }

    Id next_face(Id p, int v) {
        // As you stand at p, gaze at the Voronoi vertex v, and turn
        // left, what is the next face you come to?
        //
        // Returns the id of that face. Precondition (because we don't
        // have an invalid Id value we can return if so): v must be
        // a vertex on the boundary of p's Voronoi cell.

        auto it = next_face_map.find(pair<Id, int>(p, v));
        assert(it != next_face_map.end());
        return it->second;
    }

#ifdef TEST_VORONOI_DEBUG
    void dump_xaxis() const {
        printf("xaxis = [");
        for (const Endpoint &x: xaxis) {
            printf(" ");
            x.dump();
        }
        printf(" ]");
    }

    void dump_intersections() const {
        printf("intersections = [");
        for (auto &kv: intersections) {
            printf(" ");
            kv.first.dump();
            printf("x%d", kv.second);
        }
        printf(" ]");
    }
#endif

#ifdef TEST_VORONOI
    friend class ::Test;
#endif

};

#if 0
{ // mess with editors' indenting
{ // mess with editors' indenting
#endif
} // close namespace ZigguratInternal
using ZigguratInternal::Voronoi;
} // close namespace Ziggurat

#ifdef TEST_VORONOI

/*

To build and run the self-test program:
g++ -std=c++11 -DTEST_VORONOI -o voronoitest -x c++ voronoi.hh -lgmp && ./voronoitest

To build the self-test with diagnostics:
g++ -std=c++11 -DTEST_VORONOI -DTEST_VORONOI_DEBUG -o voronoitest -x c++ voronoi.hh -lgmp
and then probably give a specific test name as a command-line argument.

*/

#include <string>
#include "bigrat.hh"

using std::string;
using std::vector;
using std::set;
using std::pair;

using Ziggurat::bigrat;
using Ziggurat::Voronoi;

class Test {
    typedef Voronoi<bigrat, string> TestVoronoi;

    void check_edge_one_way(TestVoronoi &v, const string &p,
                            const string &q,
                            TestVoronoi::RPointOpt expected) {
        int id = v.next_vertex(p, q);
        TestVoronoi::RPointOpt actual;
        if (id != 0)
            actual = TestVoronoi::RPoint(v.vertex_x(id), v.vertex_y(id));
        if (expected != actual) {
            printf("FAIL: %s-%s: expected ", p.c_str(), q.c_str());
            expected.dump();
            printf(", got ");
            actual.dump();
            printf("\n");
            failed = true;
        }

        // Actually reach into v.next_vertex_map and delete stuff from
        // it, which is the easiest way for the unit test to ensure at
        // the end that there aren't any entries lurking in here that
        // _weren't_ covered by the expected-output specification.
        v.next_vertex_map.erase(pair<string, string>(p, q));
    }

    void check_edge(TestVoronoi &v, const string &p, const string &q,
                    TestVoronoi::RPointOpt pq, TestVoronoi::RPointOpt qp) {
        check_edge_one_way(v, p, q, pq);
        check_edge_one_way(v, q, p, qp);
    }

    void check_done(TestVoronoi &v) {
        if (!(v.next_vertex_map.empty())) {
            printf("FAIL: spurious edge %s-%s\n",
                   v.next_vertex_map.begin()->first.first.c_str(),
                   v.next_vertex_map.begin()->first.second.c_str());
            failed = true;
        }
    }

#define POINT(v, name, x, y)                    \
    TestVoronoi::RPoint name(x, y);             \
    v.add_point(x, y, #name)

#define CHECK(v, p, q, pq, qp) check_edge(v, #p, #q, pq, qp)

#define None TestVoronoi::RPointOpt()

#define circumcentre TestVoronoi::circumcentre

  public:

    bool alltests;
    set<string> testnames;
    bool failed;

    bool start_test(const char *name) {
        if (alltests || testnames.find(name) != testnames.end()) {
            testnames.erase(name);
            printf("Running test: %s\n", name);
            return true;
        } else {
            return false;
        }
    }

    void check_testnames() {
        for (const string &testname: testnames) {
            printf("Unrecognised test name: %s\n", testname.c_str());
            failed = true;
        }
    }

    void grid_test(bigrat dx, bigrat dy, int n) {
        TestVoronoi v;
        int xi, yi;
        vector<string> names;

        for (yi = 0; yi < n; yi++) {
            for (xi = 0; xi < n; xi++) {
                char namebuf[80];
                sprintf(namebuf, "<%d,%d>", xi, yi);
                names.push_back(namebuf);
                bigrat x = xi * dx - yi * dy, y = xi * dy + yi * dx;
                v.add_point(x, y, namebuf);
            }
        }

        v.compute();

        for (yi = 0; yi < n-1; yi++) {
            for (xi = 0; xi < n; xi++) {
                TestVoronoi::RPointOpt pq, qp;
                if (xi > 0)
                    pq = TestVoronoi::RPoint
                        ((xi-bigrat(1,2))*dx - (yi+bigrat(1,2))*dy,
                         (xi-bigrat(1,2))*dy + (yi+bigrat(1,2))*dx);
                if (xi+1 < n)
                    qp = TestVoronoi::RPoint
                        ((xi+bigrat(1,2))*dx - (yi+bigrat(1,2))*dy,
                         (xi+bigrat(1,2))*dy + (yi+bigrat(1,2))*dx);
                check_edge(v, names[yi*n+xi], names[(yi+1)*n+xi], pq, qp);
            }
        }
        for (yi = 0; yi < n; yi++) {
            for (xi = 0; xi < n-1; xi++) {
                TestVoronoi::RPointOpt pq, qp;
                if (yi > 0)
                    qp = TestVoronoi::RPoint
                        ((xi+bigrat(1,2))*dx - (yi-bigrat(1,2))*dy,
                         (xi+bigrat(1,2))*dy + (yi-bigrat(1,2))*dx);
                if (yi+1 < n)
                    pq = TestVoronoi::RPoint
                        ((xi+bigrat(1,2))*dx - (yi+bigrat(1,2))*dy,
                         (xi+bigrat(1,2))*dy + (yi+bigrat(1,2))*dx);
                check_edge(v, names[yi*n+xi], names[yi*n+(xi+1)], pq, qp);
            }
        }

        check_done(v);
    }

    void circle_test(bool include_cardinal_points) {
        struct triple { int y, x, hyp; } triples[] = {
            {7,24,25},
            {5,12,13},
            {8,15,17},
            {3,4,5},
            {20,21,29},
        };
        int ntriples = sizeof(triples) / sizeof(*triples);

        TestVoronoi v;
        vector<string> names;

        for (int quadrant = 0; quadrant < 4; quadrant++) {
            int dx = quadrant == 0 ? 1 : quadrant == 2 ? -1 : 0;
            int dy = quadrant == 1 ? 1 : quadrant == 3 ? -1 : 0;
            char namebuf[80];

            if (include_cardinal_points) {
                sprintf(namebuf, "Q%dC", quadrant);
                names.push_back(namebuf);
                v.add_point(dx, dy, namebuf);
            }

            for (int i = 0; i < ntriples; i++) {
                struct triple &t = triples[i];
                sprintf(namebuf, "Q%d+%d", quadrant, i);
                names.push_back(namebuf);
                v.add_point(bigrat(dx * t.x - dy * t.y, t.hyp),
                            bigrat(dx * t.y + dy * t.x, t.hyp), namebuf);
            }

            for (int i = ntriples; i-- > 0 ;) {
                struct triple &t = triples[i];
                sprintf(namebuf, "Q%d-%d", quadrant, i);
                names.push_back(namebuf);
                v.add_point(bigrat(dx * t.y - dy * t.x, t.hyp),
                            bigrat(dx * t.x + dy * t.y, t.hyp), namebuf);
            }
        }

        v.compute();

        for (int i = 0; i < names.size(); ++i) {
            int j = (i+1) % names.size();
            check_edge(v, names[i], names[j],
                       TestVoronoi::RPoint(0,0), TestVoronoi::RPointOpt());
        }

        check_done(v);
    }

    void random_test(int seed, int npoints) {
        srand(seed);

        TestVoronoi v;

        for (int i = 0; i < npoints; i++) {
            char namebuf[80];
            sprintf(namebuf, "P%d", i);
            int x = rand();
            int y = rand();
            v.add_point(x, y, namebuf);
        }

        v.compute();
    }

    void tests() {
        failed = false;

        if (start_test("generic3")) {
            // Initial test case.
            TestVoronoi v;
            POINT(v, A, 1, 1);
            POINT(v, B, 10, 2);
            POINT(v, C, 5, 5);
            v.compute();
            CHECK(v, B, A, None, circumcentre(A,B,C));
            CHECK(v, C, B, None, circumcentre(A,B,C));
            CHECK(v, A, C, None, circumcentre(A,B,C));
            check_done(v);
        }
        if (start_test("generic4")) {
            // With four points, we introduce a two-ended boundary
            // segment.
            TestVoronoi v;
            POINT(v, A, 1, 3);
            POINT(v, B, 9, 4);
            POINT(v, C, 6, 0);
            POINT(v, D, 5, 5);
            v.compute();
            CHECK(v, C, A, None, circumcentre(A,C,D));
            CHECK(v, B, C, None, circumcentre(B,C,D));
            CHECK(v, D, B, None, circumcentre(B,C,D));
            CHECK(v, A, D, None, circumcentre(A,C,D));
            CHECK(v, D, C, circumcentre(B,C,D), circumcentre(A,C,D));
            check_done(v);
        }
        if (start_test("fourway")) {
            // Or, alternatively, we might find we've just introduced
            // a 4-way meeting point.
            TestVoronoi v;
            POINT(v, A, 1, 2);
            POINT(v, B, 9, 8);
            POINT(v, C, 8, 1);
            POINT(v, D, 0, 5);
            v.compute();
            CHECK(v, C, A, None, circumcentre(A,B,C));
            CHECK(v, B, C, None, circumcentre(A,B,C));
            CHECK(v, D, B, None, circumcentre(A,B,D));
            CHECK(v, A, D, None, circumcentre(A,B,D));
            check_done(v);
        }
        if (start_test("pointaboveendpoint")) {
            // Bring in a new point at the same x-coordinate that is
            // the current boundary between two regions.
            TestVoronoi v;
            POINT(v, A, 1, 1);
            POINT(v, B, 10, 4);
            POINT(v, C, 5, 9);
            v.compute();
            CHECK(v, B, A, None, circumcentre(A,B,C));
            CHECK(v, C, B, None, circumcentre(A,B,C));
            CHECK(v, A, C, None, circumcentre(A,B,C));
            check_done(v);
        }
        if (start_test("verticalrayup")) {
            // Insert two points at the same y-coordinate, giving rise
            // to a vertical boundary segment - in this case, a
            // vertical ray up to infinity.
            TestVoronoi v;
            POINT(v, A, 1, 7);
            POINT(v, B, 5, 1);
            POINT(v, C, 9, 7);
            v.compute();
            CHECK(v, B, A, None, circumcentre(A,B,C));
            CHECK(v, C, B, None, circumcentre(A,B,C));
            CHECK(v, A, C, None, circumcentre(A,B,C));
            check_done(v);
        }
        if (start_test("verticalraydown")) {
            // Insert two points at the same y-coordinate _first_, so
            // that a vertical boundary segment extends downwards to
            // infinity.
            TestVoronoi v;
            POINT(v, A, 5, 7);
            POINT(v, B, 1, 1);
            POINT(v, C, 9, 1);
            v.compute();
            CHECK(v, B, A, None, circumcentre(A,B,C));
            CHECK(v, C, B, None, circumcentre(A,B,C));
            CHECK(v, A, C, None, circumcentre(A,B,C));
            check_done(v);
        }
        if (start_test("multipurposesweep")) {
            // Bring in a new point at the same sweep-line
            // y-coordinate that is also the point where an x-interval
            // shrinks to zero.
            TestVoronoi v;
            POINT(v, A, 1, 2);
            POINT(v, B, 9, 8);
            POINT(v, C, 8, 1);
            POINT(v, D, 4, 10);
            v.compute();
            CHECK(v, C, A, None, circumcentre(A,B,C));
            CHECK(v, B, C, None, circumcentre(A,B,C));
            CHECK(v, D, B, None, circumcentre(A,B,D));
            CHECK(v, A, D, None, circumcentre(A,B,D));
            CHECK(v, B, A, circumcentre(A,B,C), circumcentre(A,B,D));
            check_done(v);
        }
        if (start_test("fourwayverticalup")) {
            // Same as multipurposesweep, but add the wrinkle that the
            // new point is *also* directly above the Voronoi vertex
            // being introduced, so we get a four-way vertex and
            // exercise both special cases at once.
            TestVoronoi v;
            POINT(v, A, 1, 2);
            POINT(v, B, 9, 8);
            POINT(v, C, 8, 1);
            POINT(v, D, 5, 10);
            v.compute();
            CHECK(v, C, A, None, circumcentre(A,B,C));
            CHECK(v, B, C, None, circumcentre(A,B,C));
            CHECK(v, D, B, None, circumcentre(A,B,D));
            CHECK(v, A, D, None, circumcentre(A,B,D));
            check_done(v);
        }
        if (start_test("horizedge")) {
            // Generic 4-way (no high-degree vertex) with a horizontal
            // edge in the middle.
            TestVoronoi v;
            POINT(v, A, 1, 3);
            POINT(v, B, 9, 3);
            POINT(v, C, 5, 0);
            POINT(v, D, 5, 5);
            v.compute();
            CHECK(v, C, A, None, circumcentre(A,C,D));
            CHECK(v, B, C, None, circumcentre(B,C,D));
            CHECK(v, D, B, None, circumcentre(B,C,D));
            CHECK(v, A, D, None, circumcentre(A,C,D));
            CHECK(v, D, C, circumcentre(B,C,D), circumcentre(A,C,D));
            check_done(v);
        }
        if (start_test("vertedge")) {
            // And, for completeness, one with a vertical edge too.
            // We've got infinite vertical rays above, but no finite
            // one.
            TestVoronoi v;
            POINT(v, A, 5, 0);
            POINT(v, B, 5, 9);
            POINT(v, C, 7, 5);
            POINT(v, D, 3, 5);
            v.compute();
            CHECK(v, C, A, None, circumcentre(A,C,D));
            CHECK(v, B, C, None, circumcentre(B,C,D));
            CHECK(v, D, B, None, circumcentre(B,C,D));
            CHECK(v, A, D, None, circumcentre(A,C,D));
            CHECK(v, D, C, circumcentre(B,C,D), circumcentre(A,C,D));
            check_done(v);
        }
        if (start_test("horizedgeleft")) {
            // Infinite horizontal edge, pointing left.
            TestVoronoi v;
            POINT(v, A, 5, 0);
            POINT(v, B, 9, 3);
            POINT(v, C, 5, 5);
            v.compute();
            CHECK(v, B, A, None, circumcentre(A,B,C));
            CHECK(v, C, B, None, circumcentre(A,B,C));
            CHECK(v, A, C, None, circumcentre(A,B,C));
            check_done(v);
        }
        if (start_test("horizedgeright")) {
            // Infinite horizontal edge, pointing right.
            TestVoronoi v;
            POINT(v, A, 1, 3);
            POINT(v, B, 5, 0);
            POINT(v, C, 5, 5);
            v.compute();
            CHECK(v, B, A, None, circumcentre(A,B,C));
            CHECK(v, C, B, None, circumcentre(A,B,C));
            CHECK(v, A, C, None, circumcentre(A,B,C));
            check_done(v);
        }
        if (start_test("orthogridmanual")) {
            // Another straight-up square with a single meeting point
            // in the middle, but this one is aligned to the grid.
            TestVoronoi v;
            POINT(v, A, 0, 0);
            POINT(v, B, 1, 1);
            POINT(v, C, 1, 0);
            POINT(v, D, 0, 1);
            v.compute();
            CHECK(v, C, A, None, circumcentre(A,B,C));
            CHECK(v, B, C, None, circumcentre(A,B,C));
            CHECK(v, D, B, None, circumcentre(A,B,D));
            CHECK(v, A, D, None, circumcentre(A,B,D));
            check_done(v);
        }
        if (start_test("twoatonce")) {
            // Test more than one point appearing in what was
            // previously the same x-axis interval.
            TestVoronoi v;
            POINT(v, A, 5, 0);
            POINT(v, B, 0, 1);
            POINT(v, C, 10, 1);
            POINT(v, D, 4, 2);
            POINT(v, E, 6, 2);
            v.compute();
            CHECK(v, C, A, None, circumcentre(A,C,E));
            CHECK(v, E, C, None, circumcentre(A,C,E));
            CHECK(v, D, E, None, circumcentre(A,D,E));
            CHECK(v, B, D, None, circumcentre(A,B,D));
            CHECK(v, A, B, None, circumcentre(A,B,D));
            CHECK(v, D, A, circumcentre(A,D,E), circumcentre(A,B,D));
            CHECK(v, E, A, circumcentre(A,C,E), circumcentre(A,D,E));
            check_done(v);
        }
        if (start_test("twopoints")) {
            // Daft case: only two points.
            TestVoronoi v;
            POINT(v, A, 0, 1);
            POINT(v, B, 1, 0);
            v.compute();
            CHECK(v, A, B, None, None);
            check_done(v);
        }
        if (start_test("twopointshoriz")) {
            // Special case of that: horizontally separated.
            TestVoronoi v;
            POINT(v, A, 0, 1);
            POINT(v, B, 1, 1);
            v.compute();
            CHECK(v, A, B, None, None);
            check_done(v);
        }
        if (start_test("twopointsvert")) {
            // And vertically separated.
            TestVoronoi v;
            POINT(v, A, 1, 0);
            POINT(v, B, 1, 1);
            v.compute();
            CHECK(v, A, B, None, None);
            check_done(v);
        }
        if (start_test("threecollinear")) {
            // Daft case: three collinear points.
            TestVoronoi v;
            POINT(v, A, 0, 0);
            POINT(v, B, 1, 1);
            POINT(v, C, 2, 2);
            v.compute();
            CHECK(v, A, B, None, None);
            CHECK(v, B, C, None, None);
            check_done(v);
        }
        if (start_test("threecollinearhoriz")) {
            // Again, same but horizontally separated.
            TestVoronoi v;
            POINT(v, A, 0, 1);
            POINT(v, B, 1, 1);
            POINT(v, C, 2, 1);
            v.compute();
            CHECK(v, A, B, None, None);
            CHECK(v, B, C, None, None);
            check_done(v);
        }
        if (start_test("threecollinearvert")) {
            // And vertically separated.
            TestVoronoi v;
            POINT(v, A, 1, 0);
            POINT(v, B, 1, 1);
            POINT(v, C, 1, 2);
            v.compute();
            CHECK(v, A, B, None, None);
            CHECK(v, B, C, None, None);
            check_done(v);
        }
        if (start_test("onepoint")) {
            // Second daftest case: just one point.
            TestVoronoi v;
            POINT(v, A, 1, 1);
            v.compute();
            check_done(v);
        }
        if (start_test("nopoints")) {
            // Very daftest case: no points at all..
            TestVoronoi v;
            v.compute();
            check_done(v);
        }
        // Some tests of larger grids, in various orientations.
        if (start_test("orthogrid"))
            grid_test(1, 0, 5);
        if (start_test("skewgrid"))
            grid_test(4, 1, 5);
        if (start_test("diaggrid"))
            grid_test(1, 1, 5);
        // And a large set of points on an exact circle, with a
        // many-way vertex at the centre. We try this test both with
        // and without the four cardinal points (so that on the one
        // hand we get to test having points at those extrema, and on
        // the other, we test vertical and horizontal output edges).
        if (start_test("circlewith"))
            circle_test(true);
        if (start_test("circlewithout"))
            circle_test(false);
        for (int i = 0; i < 50; i++) {
            char testbuf[80];
            sprintf(testbuf, "random_%d", i);
            if (start_test(testbuf))
                random_test(i, 200);
        }
    }
};

int main(int argc, char **argv)
{
    Test t;

    if (argc > 1) {
        t.alltests = false;
        for (int i = 1; i < argc; i++)
            t.testnames.insert(argv[i]);
    } else {
        t.alltests = true;
    }

    t.tests();

    t.check_testnames();
    return t.failed ? 1 : 0;
}

#endif /* TEST_VORONOI */
